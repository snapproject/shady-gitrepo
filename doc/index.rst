.. Shady documentation master file, created by
   sphinx-quickstart on Wed Jan  3 16:37:25 2018.
   This file should at least contain the root `toctree` directive.

.. image:: shades.png
	:align: right
	:scale: 5%

.. include:: auto/Welcome.rst
.. include:: auto/Contents.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
