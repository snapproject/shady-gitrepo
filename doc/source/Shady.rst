Shady Package
=============

.. contents:: :local:

The `World` Class
-----------------

.. autoclass:: Shady.World
	:members:
	:show-inheritance:
	:inherited-members:

The `Stimulus` Class
--------------------

.. autoclass:: Shady.Stimulus
	:members:
	:show-inheritance:
	:inherited-members:

Global Functions and Constants
------------------------------

.. autofunction:: Shady.AddCustomSignalFunction

.. autofunction:: Shady.AddCustomModulationFunction

.. autofunction:: Shady.AddCustomWindowingFunction

.. autofunction:: Shady.AddCustomColorTransformation

.. autofunction:: Shady.BackEnd

.. autofunction:: Shady.Screens

.. autoclass:: Shady.SIGFUNC

.. autoclass:: Shady.MODFUNC

.. autoclass:: Shady.WINFUNC

.. autoclass:: Shady.COLORTRANS

.. autoclass:: Shady.DRAWMODE
