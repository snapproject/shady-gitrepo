Shady API Reference
===================

.. toctree::
   :maxdepth: 4

   Shady
   Shady.Dynamics
   Shady.Linearization
   Shady.Contrast
   Shady.Utilities
   Shady.Text
   Shady.Video
   