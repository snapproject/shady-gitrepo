reStructuredText Cheat-Sheet
============================

Lists
-----

	Three different kinds:
	
	.. code:: rst

		- bullet point
		  (you can also use * or +)
	
		#. enumerated item
		   (you can also use explicit numbers)

		Term:
		    Definition



Hyperlinks
----------

-	...to an item in the autodoc:

	Put something in backticks, `\`like_this\`` and it will be cast into the "default role".
	In sphinx, your `conf.py` file can specify what the default role is::

		default_role = 'py:obj'

	We use `'py:obj'` which means the string will be interpreted as a Python object, and
	hence (1) rendered as code (e.g. inside <pre></pre> tags in the html version) and
	(2) hyperlinked to an autodoc-generated target of that name, if one exists. If the
	`default_role` were set differently, you would have to prefix `:py:obj:` before the
	backticks to get this behaviour.


-	...to another file, let's call it `TargetFile.rst`::

		:doc:`TargetFile`
		:doc:`alternative text <TargetFile>`
		:doc:`alternative text <../path/to/TargetFile>`

		Note, by contrast, that if you were to want to
		
		.. include:: TargetFile.rst
		
		then you would need the file extension, whereas in a link target you do not.

-	...to an explicitly defined target label (within or across files)::

		.. _TargetLabel:
	
		This is where we want to land. Every dot, space, underscore and colon has to
		be just right. Note the *single* trailing colon rather than the double of
		most directives.
		
		Now we can jump to our :ref:`TargetLabel` like this, or if we prefer we
		can specify some :ref:`alternative text <TargetLabel>`

-	...to a URL::

		If you want http://this.com to appear in full in the rendered text, just
		type it.  It will get hyperlinked.
	
		You can also do it more explicitly, use backticks and a trailing underscore:
		that opens up the possibility of using  `alt text <http://this.com>`_ 


-	...to a section or subsection:
	
	Within the same document, a heading (like `Hyperlinks`_ above) is a valid link
	target for the simple trailing-underscore syntax above::
	
		`Internal Section Heading`_
	
	Between documents, assuming we're using sphinx, you need to use `:ref:`
	and then either define a custom label (as above) or turn on the
	`sphinx.ext.autosectionlabel` extension in `conf.py` and use the section heading.
	We have done this, and also turned on the `autosectionlabel_prefix_document`
	option, so the cross-reference to the document has to appear there too::
	
		:ref:`subdir_if_applicable/TargetFile:External Section Heading`


Google docstring format
-----------------------

Our sphinx `conf.py` specifies the following options::

	extensions = [
		# ...
		'sphinx.ext.napoleon',
	]	
	napoleon_google_docstring = True

The reason for this is that otherwise, the docstring formatting conventions required for
the autodoc extension makes docstrings very cluttered with markup elements---this is bad
because we also want docstrings to be legible in plain text, from the Python console.
Google docstrings have this format::

	def Screens( pretty_print=False ):
		"""
		Get details of any attached screens using the `Screens()` method
		of whichever windowing backend is enabled.
	
		Args:
			pretty_print (bool): determines the type of the return value
	
		Returns:
			If `pretty_print` is `True`, returns a human-readable string.
			If `pretty_print` is `False`, returns a `dict`.
		"""
		# ...
		
If you can't fit the description of an argument onto one line, it gets more finicky, but
the best outcome seems to be to ensure you line-break and indent directly after the colon,
making the arguments into a "definition list".  In particular, if the argument has a
limited set of possible values, each of which needs to be documented, a nested definition
list seems to be the best way to go, as in the following example (note the blank lines
before and after the sublist)::

	def BackEnd( windowing=None, acceleration=None ):
		"""
		Globally specify the back-end windowing and rendering systems that future
		`World` instances should use.

		Args:
			windowing:
				specifies the windowing system. Possible values are as follows:
		
				`'default'`:
					  use the ShaDyLib dynamic library if available, else fall back
					  on pyglet.
				`'shadylib'`, `'accel'`, or `'glfw'`:
					  use the ShaDyLib dynamic library (windowing is handled via the
					  GLFW library from http://glfw.org ).
				`'pyglet'`:
					  use pyglet (a third-party package that you will need
					  to install separately if you want to use this option)
				`'pygame'`:
					  use pygame (a third-party package that you will need
					  to install separately if you want to use this option)
					  
	    acceleration:
	        specifies the rendering implementation, i.e. whether to use the ShaDyLib
	        dynamic library (and if so, whether to use the "development" copy of
	        ShaDyLib in cases where you have the entire Shady repository including
	        the C++ sources for ShaDyLib) or whether to fall back on Python code
	        for rendering (not recommended). Possible values are:

	        `None`:
	              leave things as they were (default).
	        `False`:
	              disable ShaDyLib and fall back on the `pyglet` or `PyOpenGL`
	              code in the `PyEngine` submodule (this option is not
	              recommended for time-critical presentation).
	        `True`:
	              if ShaDyLib is already imported, leave things as they are;
	              if not, import either version of ShaDyLib or die trying.
	              Prefer the development version, if available. Print the
	              outcome.
	        `'bundled'`:
	              silently import the bundled version of ShaDyLib from the
	              Shady.accel sub-package, or die trying.
	        `'devel'`:
	              silently import the development version of ShaDyLib from
	              `../../accel-src/release/`, or die trying.
	        `'auto'`:
	              try to import ShaDyLib. Prefer the development version,
	              if available. Don't die in the attempt. Whatever happens,
	              print the outcome.
	
	Returns:
	    If both input arguments are `None`, the name of the current windowing backend
	    is returned. Otherwise, returns `None`.
	"""
	# ...





