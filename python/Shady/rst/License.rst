License
=======

This file is part of the Shady project, a Python framework for
real-time manipulation of psychophysical stimuli for vision science.

Copyright (C) 2017-18  Jeremy Hill, Scott Mooney

Shady is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program (file `COPYING` in the main package
directory). If not, see http://www.gnu.org/licenses/ .

We know the GNU Public License is restrictive. It may not be
suitable for your purposes. If you need something different,
contact the authors.
