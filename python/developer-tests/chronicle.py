#!/usr/bin/env python
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
import sys, os, time
import Shady

from Shady.Rendering import Chronicler

class StimulusChronicler( Chronicler ):
	def Process( self, key, value ):
		if key.startswith( '/stimuli' ):
			print( '%s %s' % ( key, value ) )

		
if __name__ == '__main__':
	cmdline = Shady.WorldConstructorCommandLine( width=200, height=300, top=50, frame=True )
	cmdline.Finalize()

	Shady.Stimulus.AddCustomUniform( spookiness=2 )
	w = Shady.World( **cmdline.opts )
	c = StimulusChronicler( w )
	s = w.Stimulus( Shady.PackagePath( 'examples/media/alien1.gif' ) )
	
	@w.EventHandler( slot=-1 )
	def ToggleAnimation( event ):
		if event >> "kp[ ]":
			# press space to pause or unpause animation (carrierTranslation property should report updates when unpaused)
			if s.GetDynamic( 'frame' ) is None: s.frame = s.frame + Shady.Integral( 16 )
			else: s.frame = s.frame
			if s.GetDynamic( 'spookiness' ) is None: s.spookiness = lambda t: t
			else: s.spookiness = s.spookiness
		if event >> "kp[v]":
			# press v to toggle visibility: updates should be reported when visible, not when not
			# press shift-v to set s.visible = -1:  report all updates once but do not draw
			if 'shift' in event.modifiers: s.visible = -1;
			else: s.visible = not s.visible
		if event >> "kp[l]":
			s.Leave()
		if event >> "kp[e]":
			s.Enter()	
	Shady.AutoFinish( w )
