# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
import Shady, numpy

cmdline = Shady.WorldConstructorCommandLine( fullScreenMode=0 )
cmdline.Help().Finalize()

w = Shady.World( **cmdline.opts )

n = 200

s = w.Stimulus( numpy.linspace( 0.0, 1.0, n, endpoint=True ).T, width=n )
s.lut = [
	[ 255,   0,   0 ],
	[ 255, 127,   0 ],
	[ 255, 255,   0 ],
	[   0, 255,   0 ],
	[   0, 127, 255 ],
	[  63,   0, 255 ],
	[ 127,   0, 255 ],
]

Shady.AutoFinish( w )
