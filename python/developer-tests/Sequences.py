#!/usr/bin/env python
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$

import inspect
import numpy
import Shady; from Shady import *

def NiceRepr( x ):
	return ( '%g' % x ) if isinstance( x, ( int, float ) ) else repr( x )
	
def go( *seq ):
	print( '===========================================' )
	if len( seq ) == 1 and ( inspect.isgenerator( seq[ 0 ] ) or isinstance( seq[ 0 ], dict ) ):
		seq = seq[ 0 ]
	f = Sequence( seq )
	try:
		for t in numpy.arange( 0, 3, 0.1 ):
			print( NiceRepr( f( t ) ) )
	except StopIteration as stop:
		print( 'stopped: %r' % stop.args[ -1 ] )

		
go( 0, 1, 2, 3, 4 )


go( { 0:0, 0.5:10, 1.0:100, 1.5:1000 } )

go(
	Transition( 0, 1 ),
	Transition( 1, 0 ),  # should repeat the 1.0
)

go( {
	0.0: Transition( 0, 1 ),
	0.5: 2.0,
	1.0: Transition( 1, 0 ),
	2.5: 3.0,
} )

go(
	Transition( 0, 1 ),
	STITCH,   # should *not* repeat the 1.0
	Transition( 1, 0 ),
	CallOnce( lambda: print( 'done!' ) ), 
	12,
)

# see also TimeOut() and WaitUntil()
