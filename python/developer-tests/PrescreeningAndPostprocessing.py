# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$

import Shady

cmdline = Shady.WorldConstructorCommandLine(fullScreenMode=0)
cmdline.Help().Finalize()


Shady.World.AddCustomUniform( apertureCenter=[0,0], apertureSize=[0,0] )

if 0:
	# to make prescreening and postprocessing functions Stimulus-specific,
	# pre-empt the (otherwise automatic World-level) creation of the following
	# custom uniforms:
	Shady.Stimulus.AddCustomUniform( prescreeningFunction=0, postprocessingFunction=0 )
	# below, you'll have to set c.prescreeningFunction instead of w.prescreeningFunction

mask = Shady.Rendering.AddCustomPrescreeningFunction( r"""
void EdgeMask(void)
{
	if( length( ( vPixelPositionInWindow.xy - uApertureCenter ) / uApertureSize ) > 0.5 ) discard;
}
""" )


invert = Shady.Rendering.AddCustomPostprocessingFunction( r"""
vec4 Invert( vec4 c )
{
	return vec4( 1.0 - c.rgb, c.a );
}
""" )


w = Shady.World( **cmdline.opts )
c = w.MakeCanvas( color=[ 1, 0, 0 ] )
w.Set( apertureSize=min( w.size ), apertureCenter=w.size / 2, prescreeningFunction=mask )

Shady.AutoFinish(w)
