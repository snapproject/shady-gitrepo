# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$

import sys
import Shady
import Shady.Text

if __name__ == '__main__':

	cmdline = Shady.WorldConstructorCommandLine( width=500, top=50, fullScreenMode=False )
	cmdline.Help().Finalize()
	
	eh = lambda w, e: sys.stdout.write( '%10s %10.6f %10.6f\n' % ( e.abbrev, e.t, e.t + w.t0 ) )
	w = Shady.World( **cmdline.opts )
	w.SetEventHandler( eh, slot=-1 )
	s = w.Stimulus( text=lambda t: int( t ) )
	
	w.Run()
	w = Shady.World( **cmdline.opts )
	w.SetEventHandler( eh, slot=-1 )
	s = w.Stimulus( text=lambda t: int( t ) )
	w.Run()
