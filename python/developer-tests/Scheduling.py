# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
import os
import time
import Shady

import Shady.Text
from Shady.Dependencies import ImageGrab

cmdline = Shady.WorldConstructorCommandLine( size=700, top=100, frame=True, fullScreenMode=False ).Help().Finalize()
w = Shady.World( **cmdline.opts )
s = w.Stimulus()
images = []
for i in range( 10 ):
	s.text = i
	w.Wait(); #time.sleep( 0.050 )
	images.append( s.Capture() )
b = w.Stimulus( images, x=20 )

@w.AnimationCallback
def animate( world, t ):
	f = int( world.framesCompleted % 10 )
	b.frame = f  # the visible change is caused by the shader's handling of carrier translation
	s.text = f   # ...whereas this causes a new texture to be transferred on each frame

@w.EventHandler( slot=-1 )
def TriggerGrab( self, event ):
	if event.type == 'key_press' and event.key in [ 'enter', 'return' ]:
		Grab()
def Grab():
	# this lets us examine whether the two changes are in synch at any randomly chosen moment
	ImageGrab.grab( [ 0, 0, w.width, w.height ] ).show()
	# you can also just take a slow-mo video with your phone if you think the image capture
	# might artifactually allow the Stimuli to "catch up" with each other

Shady.AutoFinish( w )