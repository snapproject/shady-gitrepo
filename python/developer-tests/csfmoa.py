# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
import sys
import time
import math
import random
import itertools

import Shady; from Shady import World, VDP, DegreesToPixels, SINEWAVE_SIG, Clock, FindGamma, Transition, StateMachine

# measure the physical viewing distance and height of active part of the
# display hardware, and adjust the following measurements accordingly:
viewingDistanceInMm = 700
displayHeightInMm   = 335  # 169 for Surface Pro 3; 335 for Lenovo Horizon II

targetSizeInCycles = 3
borderThicknessInCycles = 1.66
framesPerSecond = 60.0

conditions = {
	'cyclesPerDegree'    : [ 0.25, 0.5, 1.0, 2.0, 3.0, 4.0, 8.0 ],
	'degreesPerSecond'   : [ 0.0, 0.0, -10.0, 10.0 ],
	'initialLogContrast' : [ 'low', 'high' ],
}
toDo = [
	dict( zip( conditions.keys(), combo ) )
	for combo in itertools.product( *conditions.values() )
]
toDo = [
	combo
	for combo in toDo
	if abs( combo[ 'cyclesPerDegree' ] * combo[ 'degreesPerSecond' ] ) <= framesPerSecond / 2.0
]

random.shuffle( toDo )
done = []

filename = time.strftime( 'csfmoa-%Y%m%d-%H%M%S.txt' )
with open( filename, 'wt' ) as fh: fh.write( 'filename = %r\nresults = []\n' % filename )

size = None
size = 1920, 1080 # simulates Lenovo Horizon II (TODO: remove)
if size and sys.platform.lower().startswith( 'darwin' ): size = [ x // 2 for x in size ] # hackaround for Retina screen (TODO: remove)

world = World(
	size,
	clearColor = 0,
	canvas = True,
	backgroundColor = 0.5,
	#gamma = [1.9407407407407407, 1.9407407407407407, 2.1216743827160491], # TODO - scott
	gamma = [2,2,2.12],  # TODO - jez
	#gamma = -1,  # negative gamma means: use the sRGB piecewise function instead of a true exponential
) 
Shady.Console.ThreadedShell( world.Close )
with open( filename, 'at' ) as fh: fh.write( 'backgroundColor = %r\n' % list( world.backgroundColor ) )

bg = world.Stimulus( Shady.CheckPattern( world, meanZero=False, checkSize=1 ), atmosphere=world, z=+1.0 )
world.stimuli.canvas.Set( z=+0.9, pp=0.95, size=min( world.size ) )

target = world.Stimulus(
	name = 'target',
	signalFunction = SINEWAVE_SIG,
	siga = 0.5,
	plateauProportion = 0.0,
	visible  = False,
	atmosphere = world,      #  This shorthand syntax creates a link between
	                         #  various linearization/dithering properties
	                         #  and the corresponding properties of `world`
)
progress = world.Stimulus(
	name = 'progress',
	color = [ 0, 0.5, 0 ],
	position = world.Place( +1, -1 ),
	anchor = [ +1, -1 ],
	width = 100,
	height = lambda t: world.height * float( len( done ) ) / ( len( done ) + len( toDo ) ),
)
vdp = VDP( world, viewingDistanceInMm=viewingDistanceInMm, heightInMm=displayHeightInMm )
pixelsPerDegree = DegreesToPixels( 1.0, vdp )

def NextCondition( world ):
	if toDo: d = toDo.pop( 0 )
	else: return world.Close()
	
	world.settings = d # hang the container here so that it's easy to drop the result into it

	target = world.stimuli[ 'target' ]
	pixelsPerCycle = DegreesToPixels( 1.0 / d[ 'cyclesPerDegree' ], vdp )
	if d[ 'initialLogContrast' ] == 'high': d[ 'initialLogContrast' ] = random.uniform( -0.5, -0.1 )
	if d[ 'initialLogContrast' ] == 'low':  d[ 'initialLogContrast' ] = random.uniform( -3.0, -2.5 )
	d[ 'gain' ] = random.uniform( 25.0, 50.0 )
	target.normalizedContrast = 10.0 ** d[ 'initialLogContrast' ]
	target.envelopeSize = pixelsPerCycle * targetSizeInCycles
	target.sigf = 1.0 / pixelsPerCycle
	target.cx = pixelsPerDegree * d[ 'degreesPerSecond' ] * Clock()
	target.visible = True
	
	if 1:
		canvas = world.stimuli[ 'canvas' ]
		desiredSize = pixelsPerCycle * ( targetSizeInCycles + 2.0 * borderThicknessInCycles )
		canvas.envelopeSize = Transition( min( canvas.envelopeSize ), desiredSize, 1.0, finish=lambda: target.Set( visible=True ) )
		canvas.plateauProportion = Transition( canvas.plateauProportion, 1.0 - 5 / desiredSize )
		canvas.backgroundAlpha = 0.0
		target.visible = False

def HandleEvent( world, event ):
	delta = 0
	if event.type in [ 'key_release' ]:
		if   event.key in [ 'q', 'escape' ]: world.Close()
		elif event.key in [ 'down' ]: delta = -1
		elif event.key in [ 'up'   ]: delta = +1
		elif event.key in [ 'enter' ]:
			d = world.settings
			d[ 'finalLogContrast' ] = math.log10( world.stimuli[ 'target' ].normalizedContrast )
			done.append( d )
			with open( filename, 'at' ) as fh: fh.write( 'results.append(%r)\n' % d )
			NextCondition( world )
	elif event.type in [ 'text' ]:
		if   event.text in [ '[' ]: delta = -1
		elif event.text in [ ']' ]: delta = +1
	elif event.dy:
		delta = +1 if event.dy > 0 else -1
	if delta:
		factor = 10.0 ** ( world.settings[ 'gain' ] * float( delta ) / world.height )
		target = world.stimuli[ 'target' ]
		target.normalizedContrast = min( 0.9, target.normalizedContrast * factor ) 
	
def Finish():
	with open( filename, 'rt' ) as fh: print( fh.read() )

world.Animate = StateMachine()
@world.Animate.AddState
class Linearize( StateMachine.State ):
	def onset( self ):
		FindGamma( world, finish=self.UseGamma )
	def UseGamma( self, gamma ):
		with open( filename, 'at' ) as fh: fh.write( 'gamma = %r\n' % list( gamma ) )
		world.gamma = gamma
		self.Change()
@world.Animate.AddState
class Measure( StateMachine.State ):
	def onset( self ):
		world.SetEventHandler( HandleEvent )
		world.OnClose( Finish )
		NextCondition( world )
		
		
Shady.AutoFinish( world )
