# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$

import numpy
import Shady

if __name__ == '__main__':

	cmdline = Shady.WorldConstructorCommandLine( fullScreenMode=False, canvas=True, debugTiming=True )
	cmdline.Help().Finalize()
	
	w = Shady.World( **cmdline.opts )
	Shady.FrameIntervalGauge( w )
	
	w.depthTestEnabled = False
	w.Culling( w.depthTestEnabled )
	
	Shady.Stimulus.SetDefault( visible=False )
	a = w.Stimulus( numpy.random.uniform( size=w.size[::-1] ), contrast=0.15 )
	b = w.Sine( size=w.size, contrast=0.01 )
	s = w.Stimulus( bg=lambda t: w.framesCompleted % 2, visible=True )

	@w.EventHandler( slot=-1 )
	def Blah( self, event ):
		if event >> "kp[a]": a.visible = not a.visible
		if event >> "kp[b]": b.visible = not b.visible
		if event >> "kp[c]": w.stimuli.canvas.visible = not w.stimuli.canvas.visible
		if event >> "kp[d]":
			w.depthTestEnabled = not w.depthTestEnabled
			w.Culling( w.depthTestEnabled )

	Shady.AutoFinish( w )
