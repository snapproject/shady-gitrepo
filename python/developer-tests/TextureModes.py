# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
"""
OpenGL deprecated the GL_LUMINANCE and GL_LUMINANCE_ALPHA modes of texture transfer
at some point, and they would fail *silently* on some systems: if you transferred a
1- or 2-channel texture, everything would *appear* to work, but the result would be
represented in 8-bit integers, not floating-point, with the concomitant loss of
dynamic range. This was only discovered when Scott noticed the lack of dithering:
integer textures don't get dithered, because they always hit the DAC numbers
that they aim for exactly.

This demo tests our workaround.  1-, 2-, 3- and 4-channel luminance ramps are
transferred, and then each one is put under a Loupe to ensure that it's getting
dithered.  Note that you should see stripes of more and less dithering, as the
target intensity gets further from and closer to integer DAC values.
"""

import Shady
import numpy

cmdline = Shady.WorldConstructorCommandLine()
luminanceRange = cmdline.Option( 'range', [ 0.0, 0.05 ], type=( tuple, list ), length=2, container=None )
width          = cmdline.Option( 'pixels', 200, type=int, min=2, container=None )
eightBit       = cmdline.Option( 'int',  False, type=bool, container=None )
cmdline.Help().Finalize()

minLum, maxLum = min( luminanceRange ), max( luminanceRange )
enhancedContrast = 1.0 / ( maxLum - minLum )
bg = ( enhancedContrast * minLum ) / ( enhancedContrast - 1.0 )

world = Shady.World( **cmdline.opts )
for nChannels in [ 1, 2, 3, 4 ]:
	texture = numpy.linspace( luminanceRange[ 0 ], luminanceRange[ -1 ], width, endpoint=True )[ None, :, None ].repeat( nChannels, axis=2 )
	if nChannels in [ 2, 4 ]: texture[ :, :, -1 ] = 1.0  # alpha content
	if eightBit: texture = numpy.round( texture * 255 ).astype( 'uint8' )
	stim = world.Stimulus( texture, height=50, bg=bg, position=world.Place( -0.5,  1.0 - nChannels * 0.4 ), atmosphere=world )
	loupe = Shady.Loupe( stim, scaling=4, anchor=Shady.LEFT, position=stim.Place( 1.1, 0 ), contrast=lambda t: enhancedContrast, update_period=0.1 )
Shady.AutoFinish( world )

