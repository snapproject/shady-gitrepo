# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
import Shady
import Shady.Text

cmdline = Shady.WorldConstructorCommandLine( fullScreenMode=0 )
cmdline.Help().Finalize()

DRAW_MODES = 'LINE_LOOP LINE_STRIP LINES POINTS POLYGON'.split()
def CycleDrawMode():
	mode = ''
	while not hasattr( Shady.DRAWMODE, mode ): mode = DRAW_MODES.pop( 0 )
	DRAW_MODES.append( mode )
	global drawMode; drawMode = mode
	return mode
CycleDrawMode()

w = Shady.World( **cmdline.opts )
s = Shady.PixelRuler( w.Stimulus( position=w.Place( -1 ) + 10, anchor=-1, size=50 ) )
b = w.Stimulus( color=[ 1, 0, 1 ], z=+0.1, position=lambda t: s.Place( 0 ), anchor=0, size=lambda t: s.size + 20 )
Shady.PixelRuler( b, alpha=0.5 )
d = w.Stimulus( position=s, anchor=s, size=s, drawMode=lambda t: getattr( Shady.DRAWMODE, drawMode ), smoothing=1, penThickness=1, color=( 0, 1, 0 ), alpha=0.9 )

l = Shady.Loupe( s, anchor=-1, position=s.Place( 0 ) + s.size // 2 + 20, scaling=int( 0.66 * w.height / s.height ), update_period=0.1 )
t = w.Stimulus(
	position = lambda t: l.Place( Shady.UPPER_LEFT ) + [ 0, 10 ],
	anchor = Shady.LOWER_LEFT,
	text = lambda t: ' (D)    drawMode = Shady.DRAWMODE.%s,\n (S)    smoothing = %d,\n (P)    penThickness = %g,\n(F/H)   points = [ %s ],' % ( drawMode, d.smoothing, d.penThickness, ', '.join( repr( x ) for x in d.pointsComplex ) ),
)

d.points = [ 9+9j, 19+9j, 19+19j, 9+19j ]

@w.EventHandler( slot=-1 )
def Blah( world, event ):
	if event.type == 'key_press' and event.key in [ 'm', 'd' ]:
		CycleDrawMode()
	if event.type == 'key_press' and event.key in [ 'p' ]:
		d.penThickness = d.penThickness % 5 + 1
	if event.type == 'key_press' and event.key in [ 's' ]:
		d.smoothing = ( d.smoothing + 1 ) % 3 # not d.smoothing
	if event.type == 'key_press' and event.key in [ 'f', 'w', 'h' ]:
		d.pointsComplex = numpy.floor( d.pointsComplex.real ) + 1j * numpy.floor( d.pointsComplex.imag )
		if event.key in [ 'h' ]: d.pointsComplex += 0.5 + 0.5j
	if event.type == 'key_press' and event.key in [ '1', '2', '3', '4' ]:
		d.nPoints = int( event.key )
		d.points = d.points

Shady.AutoFinish( w )
