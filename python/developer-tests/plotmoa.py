#!/usr/bin/env python
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
import os
import sys
import glob
import itertools
import collections
import matplotlib.pyplot as plt, numpy, numpy as np

import Shady

def PlotMOA( filename ):
	
	fileContents = {}
	with open( filename, 'rt' ) as fh: exec( fh.read(), fileContents, fileContents )
	results = fileContents[ 'results' ]
	
	grouped = {}
	speeds = sorted( { abs( d[ 'degreesPerSecond' ] ) for d in results } )
	allSFs = sorted( { abs( d[ 'cyclesPerDegree'  ] ) for d in results } )
	for speed in speeds:
		subset = [ d for d in results if abs( d[ 'degreesPerSecond' ] ) == speed ]
		spatialFrequencies = sorted( { d[ 'cyclesPerDegree' ] for d in subset } )
		grouped[ speed ] = group = collections.defaultdict( list )
		for spatialFrequency in spatialFrequencies:
			cs = [ -d[ 'finalLogContrast' ] for d in subset if d[ 'cyclesPerDegree' ] == spatialFrequency ]
			group[ 'cpd' ].append( spatialFrequency )
			group[ 'log10cpd' ].append( numpy.log10( spatialFrequency ) )
			group[ 'cs' ].append( cs )
			group[ 'mean' ].append( numpy.mean( cs ) )
			group[ 'median' ].append( numpy.median( cs ) )
			group[ 'std' ].append( numpy.std( cs ) )
			group[ 'ste' ].append( numpy.std( cs ) / len( cs ) ** 0.5 )
			group[ 'n' ].append( len( cs ) )
	
	markers = [ 's', 'o' ]
	avg = 'median'
	plt.cla()
	for speed, group in sorted( grouped.items() ):
		xticks = [ 0.125, 0.25, 0.5, 1, 2, 4, 8, 16 ]
		ylims = [ 0, 4 ]
		marker = markers.pop( 0 ); markers.append( marker )
		h, = plt.plot( group[ 'log10cpd' ], group[ avg ], marker=marker, label='%g deg/sec' % speed )
		for sf, cs in zip( group[ 'log10cpd' ], group[ 'cs' ] ):
			plt.plot( [ sf ] * len( cs ), cs, color=h.get_color(), marker=h.get_marker(), markersize=h.get_markersize()*0.8, linestyle='None', alpha=0.25 )
		
		plt.gca().set( xticks=numpy.log10( xticks ), xticklabels=[ '%g' % val for val in xticks ], xlim=numpy.log10( [ min( xticks ), max( xticks ) ] ), ylim=ylims )
		plt.gca().set( ylabel='contrast sensitivity:  %s($-\log_{10}$ contrast)' % avg, xlabel='spatial frequency\n(cycles per degree)' )
		plt.grid( True )
	title = os.path.splitext( os.path.basename( fileContents[ 'filename' ] ) )[ 0 ]
	if 'title' in fileContents: title += '\n' + fileContents[ 'title' ]
	plt.title( title )
	plt.legend( numpoints=1 )
	return grouped
	
if __name__ == '__main__':
	filenames = sys.argv[ 1: ]
	filenames = [ filename for x in filenames for filename in sorted( glob.glob( x ) ) ]
	for i, filename in enumerate( filenames ):
		#plt.figure()
		plt.subplot( 1, len( filenames ), i + 1 )
		PlotMOA( filename )
		Shady.Utilities.FinishFigure( wait=0 )
	Shady.Utilities.FinishFigure()
