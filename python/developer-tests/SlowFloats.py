"""
Must upgrade to Shady 1.9.0 or later (via `pip install --upgrade Shady --no-deps` or
`git fetch && git checkout release && git merge`).  Versions below 1.9.0 will not even
have the `RequireShadyVersion` function so will crash there (as well as being unable to
run and report the profiler properly in the rest of this script, due to bugs/missing
features in 1.8.8).

Test as follows::

    python -m Shady slowfloats.py --dtype=float64
    python -m Shady slowfloats.py --dtype=float32
    python -m Shady slowfloats.py --dtype=uint8

Examine the log file it produces in each case---look for the profiler report.
"""

import numpy
import Shady

Shady.RequireShadyVersion( '1.9.1' )
cmdline = Shady.WorldConstructorCommandLine( profile=True, logfile='slowfloats-{}-full.txt' )

dtype        = cmdline.Option( 'dtype',        default='float64', type=str, strings=[ 'float64', 'float32', 'uint8' ], doc="""
Data type of numpy stimulus array to generate.
""" )

shape        = cmdline.Option( 'shape',        default=[ 1080, 1080, 25 ], type=( tuple, list ), length=3, doc="""
Dimensions of stimulus array: [rows, columns, pages]
""" )

assbackwards = cmdline.Option( 'assbackwards', default=False, type=bool, doc="""
Generate stimuli with the "pages" dimension packed first rather than last
(e.g. 25 x 1080 x 1080  instead of  1080 x 1080 x 25).
""" )

deepcopy     = cmdline.Option( 'deepcopy',     default=False, type=bool, doc="""
No matter what has gone before (assbackwards generation, data-type conversion...)
this makes a deep contiguous-in-memory copy of each two-dimensional page as a final
preprocessing step.  This should take the pressure off of Shady's internal texture-prep
methods at loading time.  With assbackwards generation, this should be redundant (and
needlessly expensive).
""" )

w = Shady.World( **cmdline )

if assbackwards: shape = shape[ ::-1 ] # generate stimuli with "page" dimension packed first (25 x 1080 x 1080)
yy = numpy.random.uniform( size=shape )

prep = Shady.Rendering.ProfileContextManager( True )
with prep.running:
	if dtype == 'uint8': yy = numpy.round( yy * 255 )
	if yy.dtype != dtype: yy = yy.astype( dtype )	
	if assbackwards:
		pages = numpy.split( yy, yy.shape[ 0 ], axis=0 )
		pages = [ page.squeeze( 0 ) for page in pages ] # in the assbackwards case we have to explicitly remove the leading singleton dimension
	else:
		pages = numpy.split( yy, yy.shape[ 2 ], axis=2 )
	if deepcopy:
		pages = [ page.copy() for page in pages ] # no matter what has gone before, making a contiguous-in-memory copy of each array takes the pressure off Shady.Rendering.PrepareTexture at loading time

print( 'Prep:' ); prep.stats().print_stats()

s = w.Stimulus()
with w.profile:
	s.LoadPages( pages )

p = w.profile.stats()
p.print_stats( 15 ) # output will go into the --logfile, or to the console if --logfile is disabled


t0 = w.t
w.SetAnimationCallback( lambda self, t: t > t0 + 1 and self.Close() )	
Shady.AutoFinish( w )
