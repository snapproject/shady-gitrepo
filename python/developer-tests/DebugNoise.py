# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
"""
Tinker with the "magic" numbers used in the `random()` implementation
in `Random.glsl`. This fallback implementation is only used on macOS
when a `World` is created with `legacy=True` (which is still the
default as at mid-March 2019, though this may change).

This demo can be run with `--legacy=True` OR `--legacy=False` because
it actually deploys a custom re-creation of the `Random.glsl` code,
adapted to allow the magic numbers to be manipulated as uniforms.

Press m to switch between however Shady currently generates random
numbers natively, and the custom magic mode.  In either mode, the
arrow keys, space bar, and {number keys followed by 'G', a la vim}
let you navigate through the possible noise frames. In magic mode,
you can actually just assign new values to the Python variables
`mx`, `my`, `mz`, `mw` and `loopFrames` in the shell, and they will
be automatically picked up.   So far, no combination of magic
numbers is anywhere near as good as the `RandomGLSL330AndUp.glsl`
implementation that macOS unfortunately prevents you from using
concurrently with legacy OpenGL support.

Note:

- Some flaws are invisible when the noise is static.
- Some flaws are invisible when the screen pixels are physically
  too small.
  
"""
if __name__ == '__main__':
	import Shady, Shady.Text


	cmdline = Shady.WorldConstructorCommandLine( fullScreenMode=False );
	explore    = cmdline.Option( 'explore', False, type=bool, container=None )
	loopFrames = cmdline.Option( 'loopFrames', 900, type=( int, float ), min=1, container=None ) # 900 frames = 15 seconds' worth at 60Hz
	mx         = cmdline.Option( 'mx',    12.9898, type=float, container=None )
	my         = cmdline.Option( 'my',    78.2330, type=float, container=None )
	mz         = cmdline.Option( 'mz',     1.8950, type=float, container=None )
	mw         = cmdline.Option( 'mw', 54000.0000, type=float, container=None )
	# Original coefficients from internet folklore: 12.9898, 78.233, none,  43758.5453
	# Note that the `loopFrames` setting is also magic---it interacts heavily with `mz`
	# since the frame number will get divided by `.loopFrames + 2` (to keep it bounded
	# within a known range) before getting multiplied by `mz`.
	cmdline.Help().Finalize()

	Shady.Stimulus.AddCustomUniform( fakeFrame = 0 )
	Shady.Stimulus.AddCustomUniform( loopFrames = float( loopFrames ) )  
	Shady.Stimulus.AddCustomUniform( explore = int( explore ) )
	Shady.Stimulus.AddCustomUniform( magic = [ mx, my, mz, mw ] )
	Shady.AddCustomSignalFunction( """
	float Noise( vec2 unused )
	{
		// This part is adapted from the main Shady pipeline (used with both Random.glsl and RandomGLSL330AndUp.glsl)
		// but it differs in that it allows the `c.loopFrames` parameter to be tweaked
		
		float frame = float( uFakeFrame ); // originally: uFramesCompleted
		vec3 randomSeed;
		randomSeed.xy = ( 1.0 + vFragCoordinateInEnvelope.xy ) / ( 2.0 + uEnvelopeSize );    // in range (0,1)
		randomSeed.z = ( 1.0 + mod( float( frame ), uLoopFrames ) ) / ( 2.0 + uLoopFrames ); // in range (0,1)
		
		if( uExplore == 0 )
		{
			// This part just uses whichever random implementation Shady has currently enabled
			// (on the Mac, this will be dependent on whether you launch with --legacy=0 or --legacy=1)
		
			return random( randomSeed ) / 2.0;
		}
		else
		{
			vec3 co = randomSeed;
			
			// This part is adapted from the "fallback" Random.glsl implementation of random()
			// but it differs in that it allows the `c.magic` parameters to be tweaked.
		
			// Here's the central algorithm
			float r = fract( sin( dot( co, uMagic.xyz ) ) * uMagic.w );
		
			// jez added this section because very small values are over-represented, for some reason
			const float lowerLimit = 1e-6;  // originally 1e-5
			const float upperLimit = 1.0 - lowerLimit;
			if( r < lowerLimit || r > upperLimit )
			{
				r = fract( sin( dot( co.zyx, uMagic.xyz ) ) * uMagic.w ); // so give it another try with the seeds swapped
				r = max( lowerLimit, min( upperLimit, r ) ); // then if all else fails, clip the output
			}
		
			// scale to output range [-0.5, +0.5)
			return ( r - 0.5 );	
		}
	}
	""" )


	w = Shady.World( **cmdline.opts )
	w.ReportVersions()
	c = w.MakeCanvas( signalFunction=Shady.SIGFUNC.Noise, magic=lambda t: [ mx, my, mz, mw ], loopFrames=lambda t: loopFrames )
	def Message( t ):
		msg = 'frame number %5d\n\n' % c.fakeFrame
		if c.explore: msg += 'mx = %7.4f, my = %7.4f\nmz = %7.4f, mw = %g\nloopFrames = %g' % ( mx, my, mz, mw, loopFrames )
		else: msg += "using shader's own random()\n(GLSL %s)" % w.versions[ 'GLSL' ].split()[ 0 ]
		return msg
	t = w.Stimulus( color=[ 1, 0, 0 ], text=Message, text_blockbg=[ 0, 0, 0, 0.5 ] )

	c._direction = +1
	c._goto = ''
	@w.EventHandler( slot=-1 )
	def ControlPlayback( self, event ):
		if event >> 't[,] t[<] t[[] t[{] t[(] t[-] t[_] kp[left] ka[left]':
			if 'shift' in event.modifiers: c._direction = -1; Play()
			else: Pause( -1 )
		if event >> 't[.] t[>] t[]] t[}] t[)] t[=] t[+] kp[right] ka[right]':
			if 'shift' in event.modifiers: c._direction = +1; Play()
			else: Pause( +1 )
		if event >> 'kp[g]':
			if len( c._goto ):
				target = int( c._goto )
				if c.GetDynamic( 'fakeFrame' ): Play( target )
				else: c.fakeFrame = target
		if event >> 'kp[p] kp[ ]':
			if c.GetDynamic( 'fakeFrame' ): Pause()
			else: Play()
		if event >> 'kp[e] kp[x] kp[m]':
			c.explore = not c.explore
		
		if event.type == 'key_press':
			if event.key in '0123456789': c._goto += event.key
			else: c._goto = ''

	def Pause( offset=0 ):
		c.fakeFrame = c.fakeFrame + offset

	speed = 1.0
	def Play( fakeStartFrame=None ):
		if fakeStartFrame is None: fakeStartFrame = c.fakeFrame
		trueStartFrame = w.framesCompleted
		direction = c._direction
		c.fakeFrame = lambda t: round( speed * direction * ( w.framesCompleted - trueStartFrame ) + fakeStartFrame )

	Shady.AutoFinish( w, shell=True )
