# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
import os
import sys
import numpy

import Shady


def eh( self, event ):
	single = self.stimuli.single
	multi = self.stimuli.multi
	
	if event.type == 'mouse_motion' and 'left' in event.button:
		xp = 0.5 + event.x / float( self.width )
		yp = 0.5 + event.y / float( self.height )
		if 'shift' in event.modifiers:
			single.gamma = 1.0 + 2.0 * yp
			single.bluegamma *= 0.5 + xp
		elif 'ctrl' in event.modifiers:
			single.sigf = 1.0 / ( 2.0 + 98.0 * yp )
			single.xscale = ( 0.5 + 2.0 * xp )
			multi.modf = 1.0 / ( 100 + 900 *  xp )
		else:
			single.sigf = min( 0.5, 0.1 ** ( 2.0 * ( 1.0 - xp ) ) )
			single.contrast = min( 0.97, 0.1 ** ( 3.0 * yp ) )
			
			
	if event.type == 'key_release':
		if event.key in list( 'abcdefghijklmnopqrstuvwxyz' ):
			print( '\n%s: %s' % ( event.key.upper(), tuple( single.gamma ) ) )
		elif event.key in [ 'space', 'return', 'enter' ]:
			single.visible = not single.visible
			multi.visible = not single.visible
		elif event.key not in [ 'q', 'escape' ]:
			print( event )
		if event.key in [ 'q', 'escape' ]: self.Close()


w = Shady.World( canvas=True, anchor=0.0, backgroundColor=0.5, gamma=-1 )
sinusoid = lambda x: numpy.sin( 2.0 * numpy.pi * x )

single = w.Stimulus(
	name = 'single',
	width = 500.0,
	height = w.height,
	anchor = 0.0,
	ppx = 0.0,
	ppy = 0.0,
	sigfunc = 1,
	siga = 0.5,
	sigf = 1./25,
	sigo = 90,
	x = lambda t: sinusoid( t / 5.0 ) * 0.9 * w.width / 2.0,
	z = +0.2,
	contrast = 0.97,
	atmosphere = w,
)
multi = w.Stimulus(
	name = 'multi',
	width = w.width,
	height = w.height,
	anchor = 0.0,
	ppx = -1,
	ppy = 0.5,
	sigfunc = 1,
	modfunc = 1,
	siga = 0.5,
	sigf = 1./25,
	sigo = 90,
	moda = 1.0,
	modf = 1./500,
	modo = 0.0,
	cx = lambda t: t * 500,
	z = +0.1,
	visible = False,
	atmosphere = w,
	contrast = single,
	signalParameters = single,
)

w.SetEventHandler( eh )

