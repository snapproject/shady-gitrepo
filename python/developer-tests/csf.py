# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
import random

import Shady; from Shady import World, VDP, DegreesToPixels, SINEWAVE_SIG, Apply, Tukey, Clock, Sinusoid

# measure the physical viewing distance and height of active part of the
# display hardware, and adjust the following measurements accordingly:
viewingDistanceInMm = 700
displayHeightInMm   = 169  # 169 for Surface Pro 3; 335 for Lenovo Horizon II

nAFC = 4
targetSizeInCycles = 3
targetOffsetInCycles = 0
borderThicknessInCycles = 0.41
delayInSeconds = 1.0
riseTimeInSeconds = 0.5
plateauTimeInSeconds = 3.0

frequencies = [ 0.25, 0.5, 1.0, 2.0, 3.0, 4.0, 8.0 ]

world = World(
#	(1920,1080), # simulate Lenovo Horizon II (TODO: remove)
	clearColor = 0,
	canvas = True,
	backgroundColor = 0.5,
	gamma = -1,
)

vdp = VDP( world, viewingDistanceInMm=viewingDistanceInMm, heightInMm=displayHeightInMm )

targets = {}

for cyclesPerDegree in frequencies:
	pixelsPerCycle = DegreesToPixels( 1.0 / cyclesPerDegree, vdp )
	target = targets[ cyclesPerDegree ] = world.Stimulus(
		size = targetSizeInCycles * pixelsPerCycle,
		signalFunction = SINEWAVE_SIG,
		siga = 0.5,
		sigf = 1.0 / pixelsPerCycle,
		plateauProportion = 0.0,
		normalizedContrast = 0.0,
		visible  = False,
		atmosphere = world,      #  This shorthand syntax creates a link between
		                         #  various linearization/dithering properties
		                         #  and the corresponding properties of `world`
	)
	target.pixelsPerCycle = pixelsPerCycle # keep this for future reference

fixation = world.Stimulus(
	size = DegreesToPixels( 0.1, vdp ),
	foregroundColor = ( 0, 0, 1 ),
	backgroundAlpha = 0,
	plateauProportion = 1.0, # circular/ellipsoid, sharp-edged
)

def Present( cyclesPerDegree, normalizedContrast=0.9, degreesPerSecond=10.0 ):
	for target in targets.values():
		target.Set( normalizedContrast=0.0, visible=False, cx=0.0 )
	target = targets[ cyclesPerDegree ]
	targetLocation = random.randint( 0, nAFC - 1 )
	radialDistanceInPixels = target.pixelsPerCycle * ( targetOffsetInCycles + targetSizeInCycles / 2.0 )
	canvasSizeInPixels = target.pixelsPerCycle * 2.0  * ( targetOffsetInCycles + targetSizeInCycles + borderThicknessInCycles )
	world.stimuli.canvas.DefineEnvelope # TODO
	world.stimuli.canvas.Set( envelopeSize=( canvasSizeInPixels, canvasSizeInPixels ), pp=1, bgalpha=0 )
	fixPlateau = delayInSeconds + riseTimeInSeconds * 2 + plateauTimeInSeconds
	fixation.normalizedContrast = 1.0 - Apply( Tukey, Clock(), rise=riseTimeInSeconds, plateau=fixPlateau )
	target.Set(
		envelopeTranslation = [ Sinusoid( targetLocation / nAFC, phase ) * radialDistanceInPixels for phase in [ 90, 0 ] ],
		normalizedContrast = normalizedContrast * Apply( Tukey, Clock() - delayInSeconds, rise=riseTimeInSeconds, plateau=plateauTimeInSeconds ),
		visible = True,
		cx = Clock() * degreesPerSecond * cyclesPerDegree * target.pixelsPerCycle, 
	)
	return targetLocation

# TODO: unfinished