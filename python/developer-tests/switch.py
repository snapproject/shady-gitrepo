# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$

"""
Assume the "developer" version of the accelerator has been hacked to
use the "more-intuitive" Order A:

	- A1 Prepare parameters (Python callback)
	- A2 Send draw commands to GPU
	- A3 Display completed frame (swap buffers)

...by moving the line::

	if( mUpdateCallback ) mUpdateCallback( callTime, mUpdateCallbackArgument );
	
from near the end of ShaDyLib::Renderer::Draw(), up to the beginning---immediately
after `WaitForNextFrame()` and measurement of `callTime = Seconds()`.

Further assume that the "bundled" version uses the official Order B:

	- B1 Send draw commands to GPU
	- B2 Prepare parameters (Python callback)
	- B3 Display completed frame (swap buffers)


...which is theoretically more efficient as it allows CPU and GPU to work in
parallel with each other rather than waiting for each other to finish.

The efficiency gain can be demonstrated as follows.  On a large screen,
these two perform roughly the same::

	shady switch.py --acceleration=devel   --stimSize=700 --canvas=0
	shady switch.py --acceleration=bundled --stimSize=700 --canvas=0
	

Whereas if you add a large dithered canvas (significant GPU expense), the
`devel` version (order A) breaks down at a lower `stimSize` than the `bundled`
version (order B).  You might have to adjust the `--stimSize` to find the 
threshold for your particular system---this was done on a Late 2013 MacBook
with the lid closed, using a 2560 x 1440 @ 59Hz Thunderbolt display as its sole
display::

	shady switch.py --acceleration=devel   --stimSize=700 --canvas=1
	shady switch.py --acceleration=bundled --stimSize=700 --canvas=1
"""

import numpy
import Shady

cmdline = Shady.WorldConstructorCommandLine( fullScreenMode=0, debugTiming=1 )
stimSize = cmdline.Option( 'stimSize', default=100, type=int, min=1, container=None )
w = Shady.World( **cmdline )

a = numpy.random.uniform( size=[ stimSize, stimSize, 1 ] )
red   = numpy.concatenate( [ a, a * 0, a * 0 ], axis=2 )
green = numpy.concatenate( [ a * 0, a, a * 0 ], axis=2 )

s = w.Stimulus( red, x=-200, pp=-1, bgalpha=0 )
f = Shady.FrameIntervalGauge( w )



@w.AnimationCallback
def Switch( self, t ):
	if w.framesCompleted % 2:
		s.Set( x=-200, pp=-1 ).LoadTexture( red )
	else:
		s.Set( x=+200, pp=+1 ).LoadTexture( green )

Shady.AutoFinish( w )