# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
import os
import sys
import time

import numpy
import Shady

#Shady.SetProcessPriority(+3) # NB: will only actually reach +3 if we're running as admin
#Shady.SetThreadPriority(+3)  # NB: will only actually reach +3 if we're running as admin
class MyWorld( Shady.World ):
	def Prepare( self ):
		#import pyglet; clock = pyglet.clock.ClockDisplay(); clock.label.y = 100; self.AddForeignStimulus( clock, 'clock%02d' )
		#import pyglet; fps = pyglet.window.FPSDisplay( self.window ); fps.label.y = 150; self.AddForeignStimulus( fps, 'clock%02d' )
		pass

cmdline = Shady.WorldConstructorCommandLine( canvas=True, debugTiming=True, profile=False, logfile='stress-{}.log' )
shell     = cmdline.Option( 'shell',     False, container=None, type=bool )
tripout   = cmdline.Option( 'tripout',   False, container=None, type=bool )
gauge     = cmdline.Option( 'gauge',     False, container=None, type=bool )
record    = cmdline.Option( 'record',    False, container=None, type=( bool, int, float ), min=0 )
patchSize = cmdline.Option( 'patchsize', 450,   container=None, type=int, min=10 )
cmdline.Help()
cmdline.Finalize()

world = MyWorld( **cmdline.opts ).Set( anchor=[ -1, -1 ] )

patchSize = [ patchSize, patchSize ]
nPatches = [ world.size[ i ] // patchSize[ i ] for i in [ 0, 1 ] ]
#nPatches[ 0 ] -= 0; nPatches[ 1 ] = 2
period   = [ world.size[ i ]  / max( 1, nPatches[  i ] ) for i in [ 0, 1 ] ]

def MakeStimulus( offset ):
	return world.Stimulus(
		source = numpy.random.rand( *patchSize ), # NB: greyscale
		anchor = [ +1, -1 ],
		x = lambda t: int( ( offset[ 0 ] + ( t * 300 ) ) % ( world.width + period[ 0 ] ) ),
		y = int( offset[ 1 ]  + 0.5 * ( period[ 1 ] - patchSize[ 1 ] ) ),
		contrast = 0.9,
		atmosphere = world,
		debugTiming = False,
	)

for j in range( nPatches[ 0 ] + 1 ):
	for i in range( nPatches[ 1 ] ):
		MakeStimulus( [ period[ 0 ] * j, period[ 1 ] * i ] )

def Record( fps ):
	if not fps: return
	if fps == True: fps = 30.0
	world.fakeFrameRate = fps
	world.videoOutput = Shady.VideoRecording( 'stress', fps=world )
	world.OnClose( world.videoOutput.Close )
	world.SetAnimationCallback( lambda self, t: self.videoOutput.WriteFrame( self ) )
Record( record )

def TripOut():
	for x in world.stimuli.values():
		if not x.name.startswith( 'stim' ): continue
		x.Set(
			cr     = Shady.Integral( numpy.random.randn() * 60.0 ),
			cscale = Shady.Oscillator( numpy.random.randn() ) * 0.5 + 1.0,
		)

if tripout: TripOut()
if gauge: f = Shady.FrameIntervalGauge( world )
Shady.AutoFinish( world, shell=shell )
