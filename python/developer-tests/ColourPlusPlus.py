# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
"""

"""
if __name__ == '__main__':
	import sys
	import Shady
	cmdline = Shady.WorldConstructorCommandLine( )
	cmdline.Help().Finalize()
	
	Shady.AddCustomSignalFunction( """
float CheckSignal( vec2 coord )
{
	coord = mod( coord, 2.0 );
	float d = coord.x - coord.y;
	if( uSignalParameters[ 3 ] == 0.0 )
	{
		// un-rounded function (reveals potential imperfection: unless the shader code is just right, in double-wide mode, the pixel values increase steadily rather than being repeated in pairs)
		return d * 0.5 * uSignalParameters[ 0 ];
	}
	else
	{
		// black-and-white-checks (looks perfect, masks the interpolation problem above)
		if( d > 0.5 || d <= -0.5 ) return -uSignalParameters[ 0 ];
		else return uSignalParameters[ 0 ];
	}
}
	""" )
	w = Shady.World( **cmdline.opts )
	
	size = 100
	s = w.Stimulus( size=size, sigfunc=Shady.SIGFUNC.CheckSignal, siga=0.5, linearMagnification=0, pos=-size/2 )
	t = w.Stimulus( size=size, sigfunc=Shady.SIGFUNC.CheckSignal, siga=0.5, y=s.y + size + 1, x=s.x + 1, color=s )
	
	s2 = w.Stimulus( linearMagnification=0, color=s, x=s.x + size + 1, y=s.y ); s2.LoadTexture( Shady.CheckPattern( size, meanZero=False ) )
	t2 = w.Stimulus( linearMagnification=1, color=s, x=t.x + size + 1, y=t.y ); t2.LoadTexture( Shady.CheckPattern( size, meanZero=False ) )
	
	v = w.Stimulus( pos=0, size=size*2+20, visible=0 )
	p = w.Stimulus( 1.0, anchor=-1 )
	def Render( *values ):
		p.color = values
		p.color /= 65535.0
		@w.RunDeferred
		def rendered():
			a = p.Capture()
			sys.stdout.write( '\n' )
			for row in a:
				for pixel in row: sys.stdout.write( '     [%r, %r, %r]' % tuple( pixel[ :3 ] ) )
				sys.stdout.write( '\n' )
			sys.stdout.write( '\n' )
	
	
	w.verticalGrouping = 2	
	@w.EventHandler( slot=-1 )
	def e( self, event ):
		if event >> 'kp[m]':
			# 4 -> grouped pixels, but no recoding of pixel values
			# 3 -> grouped pixels, with odd-numbered columns tinted cyan
			# 2 -> grouped pixels, colourPlusPlus rendering
			# 1 -> normal resolution, monoPlusPlus rendering
			# 0 -> normal Shady rendering
			self.SetBitCombiningMode( ( self.bitCombiningMode - 1 ) % 5, verticalGrouping=self.verticalGrouping )
		if event >> 'kp[y]':
			self.verticalGrouping = 3 - self.verticalGrouping
			self.SetBitCombiningMode( self.bitCombiningMode, verticalGrouping=self.verticalGrouping )
		if event >> 'kp[p]':
			s.sigp = t.sigp = 1 - s.sigp   # used in the custom CheckSignal function to switch between types
		if event >> 'kp[r]':
			if t.cr:
				s.Set( scaling=1, rotation=0, cscaling=1, cr=0 )
				t.Set( scaling=1, rotation=0, cscaling=1, cr=0 )
			else:
				s.Set( scaling=50, rotation=10 )
				t.Set( cscaling=20, cr=10 )
		if event >> 'kp[s]':
			self.captured_raw = raw = s.Capture()
			self.captured_assembled = ( raw[ :, 0::2, : ] * 256.0 + raw[ :, 1::2, : ] ) #/ 65535.0
		if event >> 'kp[x]':
			w.Capture( saveas='blah.png' )
			import os, sys
			if sys.platform.lower().startswith( 'win' ): os.system( 'start blah.png' )
			else: os.system( 'open blah.png' )
	Shady.AutoFinish( w )
