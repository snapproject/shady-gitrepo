# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
"""
Page through a number of Worlds with the spacebar.
Tries all combos of: even and odd World sizes, even and odd Stimulus
sizes, and various World and Stimulus anchors.  For each combo,
make sure we have calculated its bounding box correctly, as indicated
by whether the Loupe captures the correct number of pixels without
any of the magenta surround.
"""

if __name__ == '__main__':
	
	import numpy
	import Shady
	import Shady.Text
	cmdline = Shady.WorldConstructorCommandLine()
	cmdline.Help().Finalize()
	
	m = lambda a: numpy.asmatrix( a.reshape( ( 4, 4 ) ) )
	
	worldSizes = [ 1000,1001 ]
	worldAnchors = [ -1, 0, 1, -0.11, +0.11 ]
	stimulusAnchors = [ -1, 0, 1, -0.123, +0.123 ]
	stimulusSizes = [ 100, 101 ]
	
	for worldSize in worldSizes:
		cmdline.opts.update( dict( width=worldSize, height=worldSize ) )
		w = Shady.World( **cmdline.opts )	
		s = Shady.PixelRuler( w.Stimulus( pos=lambda t: w.Place( +1, -1 ) + [ -250, +250 ] ) )
		bg = w.Stimulus( pos=lambda t:s.Place( 0 ), size=400, color=2, z=1 )
		l = Shady.Loupe( s, update_period=0.05, pos=lambda t:w.Place( -1, +1 ) + [ 10, -10 ], scaling=5, anchor=[ -1, +1 ] )
		t = w.Stimulus( pos=lambda t: l.Place( +1, 0 ) + [ 10, 0 ], anchor=( -1, 0 ), text=lambda t: 'w.size=%s\nw.anchor=%s\ns.size=%s\ns.anchor=%s' % ( w.size, w.anchor, s.size, s.anchor ) )
		w.advance = False
		@w.AnimationCallback
		def Animate( self ):
			for stimulusSize in stimulusSizes:
				s.size = stimulusSize
				for stimulusAnchor in stimulusAnchors:
					s.anchor = stimulusAnchor
					for worldAnchor in worldAnchors:
						w.anchor = worldAnchor
						print( ( w.anchor, s.width, s.anchor ) )
						while not self.advance: yield
						self.advance = False
			self.Close()
		@w.EventHandler( slot=-1 )
		def HandleEvent( self, event ):
			if event.abbrev == 'kp[ ]':
				self.advance = True
		w.Run()
			
			
	
	def NWP( w ):
		"""
		normalizedPositionInWindow =     P * T * R * S * A * vertex
		                           = N * W * T * R * S * A * vertex
		pixelPositionInWindow      =     W * T * R * S * A * vertex
		"""
		N = numpy.matrix( [
			[ 2.0 / w.width,  0,               0,             -1.0, ],
			[ 0,              2.0 / w.height,  0,             -1.0, ],
			[ 0,              0,               1.0,            0,   ],
			[ 0,              0,               0,              1.0, ],
		] )
		W = numpy.matrix( [
			[ 1.0,            0,               0,              numpy.ceil( 0.5 * w.width  * ( w.ax_n + 1 ) ) ],
			[ 0,              1.0,             0,              numpy.ceil( 0.5 * w.height * ( w.ay_n + 1 ) ) ],
			[ 0,              0,               1.0,            0,                                            ],
			[ 0,              0,               0,              1.0,                                          ],
		] )
		P = m( w.matrixWorldProjection )
		print( N*W - P )
		return N, W, P
		
	Shady.AutoFinish( w )
