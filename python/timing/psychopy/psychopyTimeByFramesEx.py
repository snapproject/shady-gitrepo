#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This is an extended version of the base timeByFrames demo, adding
the ability to set the psychopy experiment runtime process priority
and disable python garbage collection to see if either influences
the precision of your frame flips.
"""

from __future__ import division
from __future__ import print_function

import Shady
Shady.SetDPIAwareness( 2 )
cmdline = Shady.CommandLine()
nIntervals   = cmdline.Option( 'nIntervals',   500, type=int, position=0, min=1, doc="Target number of frame-to-frame intervals to run." )
nStimuli     = cmdline.Option( 'nStimuli',       1, type=int, position=1, min=1, max=15, doc="Number of Gabor patches." )
burnInFrames = cmdline.Option( 'burnInFrames',  10, type=int, min=0, doc="Disregard this many frames from the beginning of the World's run." )
disable_gc   = cmdline.Option( 'disable_gc', False, type=bool, doc="Whether to disable Python's garbage collector." )
process_priority = cmdline.Option( 'priority', 'normal', type=str, strings=[ 'normal', 'high', 'realtime' ], doc="Process priority." )
plot         = cmdline.Option( 'plot',       False, type=bool, doc="Whether to plot immediately on completion." )
cmdline.Help().Finalize()

from builtins import str
from builtins import range
import gc, numpy
from psychopy import visual, logging, core, event


visual.useFBO = True  # if available (try without for comparison)

if process_priority == 'normal':
	pass
elif process_priority == 'high':
	core.rush(True)
elif process_priority == 'realtime':
	# Only makes a diff compared to 'high' on Windows.
	core.rush( True, realtime=True )
else:
	print( 'Invalid process priority:', process_priority, "Process running at normal." )
	process_priority = 'normal'

if disable_gc:
	gc.disable()

import matplotlib
matplotlib.use('Qt4Agg')  # change this to control the plotting 'back end'
import pylab

win = visual.Window([1920, 1200], pos=[-1920,0], fullscr=False, allowGUI=False, waitBlanking=True)
progBar = visual.GratingStim(win, tex=None, mask=None,
	size=[0, 0.05], color='red', pos=[0, -0.9], autoLog=False)
stimuli = []
shift = 400
positions = [ [ 0, 0 ], [ -shift, 0 ], [ +shift, 0 ], [ 0, +shift ], [ 0, -shift ], [ -shift, -shift ], [ +shift, -shift ], [ +shift, +shift ], [ -shift, +shift ], [ -shift*2, 0 ], [ +shift*2, 0 ], [ -shift*2, +shift ], [ -shift*2, -shift ], [ +shift*2, +shift ], [ +shift*2, -shift ], ]
for position in positions[ :nStimuli ]:
	myStim = visual.GratingStim(win, tex='sin', mask='gauss', size=300, sf=0.05, units='pix', autoLog=False, pos=position)
	stimuli.append( myStim )

# logging.console.setLevel(logging.INFO)  # uncomment to print log every frame

nFlips = burnInFrames + nIntervals + 1

fliptimes = numpy.zeros( nFlips )
drawingDone = numpy.zeros( nFlips )
updatesDone = numpy.zeros( nFlips )
previousFlipTime = numpy.nan
#win.recordFrameIntervals = True
for frameN in range( nFlips ):
	progBar.draw()
	for myStim in stimuli: myStim.draw()
	if frameN: drawingDone[ frameN ] = logging.defaultClock.getTime() - previousFlipTime
	else: drawingDone[ frameN ] = None
	# this is the equivalent place to 'World_BeginFrameCallback'
	
	progBar.setSize( [ 2.0 * frameN / float( nFlips ), 0.05 ] )
	for myStim in stimuli:
		myStim.setPhase( 0.1, '+' )
		myStim.setOri( 1, '+' )
		
	if event.getKeys():
		print( 'stopped early' )
		break
		
	if frameN: updatesDone[ frameN ] = logging.defaultClock.getTime() - previousFlipTime
	else: updatesDone[ frameN ] = None
		
	#win.logOnFlip( msg='frame=%i' % frameN, level=logging.EXP )
	previousFlipTime = fliptimes[ frameN ] = win.flip()


### log results
import psychopy
outputs = dict(
	toolbox = 'PsychoPy %s (pure Python)' % psychopy.__version__,
	nStimuli = nStimuli,
	nIntervals = nIntervals,
	burnInFrames = burnInFrames,
	world_size = tuple( win.size ),
	process_priority = process_priority,
	disable_gc = disable_gc,
)
import time
logfile = '%s-psychopy-nStimuli=%d.log' % ( time.strftime( '%Y%m%d-%H%M%S' ), nStimuli )
print( 'Logging to %s' % logfile )
with open( logfile, 'wt' ) as fh:
	for key, value in outputs.items():
		try: value = value.tolist()
		except: pass
		fh.write( '%s = %r\n' % ( key, value ) )
	fh.write( """
timings = {
	'DrawTimeInMilliseconds' : %r,
	'World_BeginFrameCallback' : %r,
	'World_RunPending' : %r,
}
""" % (
	( 1000.0 * fliptimes ).tolist(),
	( 1000 * drawingDone ).tolist(),
	( 1000 * updatesDone ).tolist(),
) )
####

if disable_gc:
	gc.enable()
core.rush( False )

win.close()

if plot: from timingReport import Report; Report( logfile )

core.quit()

# The contents of this file are in the public domain.
