"""
hacks to PsychoPy timeByFramesEx:
	DPI.SetDPIAwareness(2)
	optional multiple stimuli
	one histogram only
"""
import numpy
import Shady  # NB: have to do  `python -m pip install -e \PATH\TO\shady-gitrepo`   where `python` invokes the PsychoPy standalone executable

cmdline = Shady.CommandLine()
nIntervals   = cmdline.Option( 'nIntervals',  500, type=int, position=0, min=1, doc="Target number of frame-to-frame intervals to run." )
nStimuli     = cmdline.Option( 'nStimuli',      1, type=int, position=1, min=1, max=15, doc="Number of Gabor patches." )
burnInFrames = cmdline.Option( 'burnInFrames', 10, type=int, min=0, doc="Disregard this many frames from the beginning of the World's run." )
screen       = cmdline.Option( 'screen',        0, type=int, min=0, doc="Screen ID number (0 for default screen)." )
pure         = cmdline.Option( 'pure',      False, type=bool, doc="Whether or not to ruin things by switching to pure-Python drawing engine (requires third-party package `pyglet`)." )
plot         = cmdline.Option( 'plot',      False, type=bool, doc="Whether to plot immediately on completion." )
cmdline.Help().Finalize()

if pure:  Shady.BackEnd( 'pyglet', acceleration=False )

frameRate = Shady.Screens()[ screen ][ 'hz' ]
process_priority = 'normal'; disable_gc = False  # NB: these are stubs. we log these settings but at the moment we don't use them

world = Shady.World( canvas=True, threaded=False, screen=screen, logfile='{}-shady%s-nStimuli=%d.log' % ( '-pure' if pure else '', nStimuli ) )
world.logger.Log( **cmdline.opts )


shift = 400
positions = [ [ 0, 0 ], [ -shift, 0 ], [ +shift, 0 ], [ 0, +shift ], [ 0, -shift ], [ -shift, -shift ], [ +shift, -shift ], [ +shift, +shift ], [ -shift, +shift ], [ -shift*2, 0 ], [ +shift*2, 0 ], [ -shift*2, +shift ], [ -shift*2, -shift ], [ +shift*2, +shift ], [ +shift*2, -shift ], ]
for position in positions[ :nStimuli ]:
	world.Stimulus(
		size              = 300,
		signalFunction    = Shady.SIGFUNC.SinewaveSignal,
		signalAmplitude   = 0.5,
		signalFrequency   = 0.05,
		signalPhase       = lambda t: t * frameRate * 36,  # because the original PsychoPy demo advances 0.1 of a cycle per frame
		
		carrierRotation   = lambda t: t * frameRate,       # because the original PsychoPy demo advances 1 degree per frame
		plateauProportion = 0,
		position          = position,
		atmosphere        = world,
	)

progressBar = world.Stimulus(
	color    = world.clearColor,
	position = world.Place( 0.0, -0.9 ),
	width    = lambda t: world.width * ( world.framesCompleted / float( nIntervals + burnInFrames ) ),
	height   = 0.025 * world.height,
)
@world.AnimationCallback
def SelfDestruct( self, t ):
	if self.framesCompleted >= nIntervals + 1 + burnInFrames:
		self.Close()

world.logger.Log(
	toolbox = 'Shady %s%s' % ( Shady.__version__, ' (pure Python)' if pure else ' (accelerated)' ),
	process_priority = process_priority,
	disable_gc = disable_gc,
)

world.debugTiming = True

@world.EventHandler( slot=-1 )
def Snapshot( self, event ):
	if event.type == 'key_press' and event.key == 's':
		self.Capture( saveas='cap.png' )

world.Run()

if plot: from timingReport import Report; Report( world.logger.filename )
