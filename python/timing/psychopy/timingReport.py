import os, re, sys, glob, copy, itertools
import numpy, matplotlib.pyplot as plt
import Shady

def Histogram( a, limits, binsPerMsec=4 ):
	bin_edges = numpy.linspace( min( limits ), max( limits ), int( binsPerMsec * ( max( limits ) - min( limits ) ) ) + 1, endpoint=True )
	return 0.5 * ( bin_edges[ 1: ] + bin_edges[ :-1 ] ), numpy.histogram( numpy.array( a ).flat, bins=bin_edges )[ 0 ]

def Patch( x, y=None, baseline=0, **kwargs ):
	if y is None: y, x = x, None
	y = numpy.array( y ).flatten()
	if x is None: x = numpy.arange( y.size )
	x = numpy.array( x ).flatten()
	plt.fill_between( x, y, y * 0 + baseline, **kwargs )
	
def Report( filenames, limits=None, separate=False, detail=False, collate=False, fps=60.0, hist=True, legend='right', transpose=False, patches=None, showVersion=False ):
	
	out = {}
	auto_limits = limits is None
	if limits is None: limits = ( 10, 40 )
	limits = list( limits )
	if detail: limits[ 0 ] = 0
	
	if isinstance( filenames, str ):
		filenames = filenames.split( ';' )
	filenames = [ filename for pattern in filenames for filename in sorted( glob.glob( pattern ) ) ]
	
	if patches is not None:
		if not isinstance( patches, ( tuple, list ) ): patches = [ patches ]
		def extract_patches( s ):
			m = re.match( r'.*nStimuli\=([0-9]+).*', os.path.basename( s ) )
			if m: return int( m.group( 1 ) )
		filenames = [ filename for filename in filenames if extract_patches( filename ) in patches ]
	
	if not filenames: raise ValueError( 'no matching files found' )
	
	all_info = {}
	for index, filename in enumerate( filenames ):
		
		info = all_info[ filename ] = Shady.Logging.Read( filename )
		intervalsMS = info[ 'intervalsMS' ] = list( numpy.diff( info[ 'timings' ][ 'DrawTimeInMilliseconds' ] ) )[ -info[ 'nIntervals' ]: ]
		for k, v in info[ 'timings' ].items(): info[ 'timings' ][ k ] = list( v )[ -info[ 'nIntervals' ]: ]
		info[ 'world_size' ] = tuple( info[ 'world_size' ] )		
		if auto_limits:
			while limits[ -1 ] < max( intervalsMS ): limits[ -1 ] += 15

	if collate:
		matchTerms = 'disable_gc world_size nStimuli toolbox process_priority'.split()
		groupKey = lambda filename: tuple( ( term, all_info[ filename ][ term ] ) for term in matchTerms ) + ( ( 'timingKeys', tuple( sorted( all_info[ filename ][ 'timings' ].keys() ) ) ), )
		sortKey  = lambda filename: groupKey( filename ) + ( ( 'filename', filename ), )
		collated = {}
		master_filenames = []
		for condition, group in itertools.groupby( sorted( filenames, key=sortKey ), groupKey ):
			for filename in group:
				info = all_info[ filename ]
				if condition in collated:
					collated[ condition ][ 'intervalsMS' ] += info[ 'intervalsMS' ]
					collated[ condition ][ 'cycleNumber' ] += [ collated[ condition ][ 'cycleNumber' ][ -1 ] + 1 ] * len( info[ 'intervalsMS' ] )
					for k, v in collated[ condition ][ 'timings' ].items():
						v += info[ 'timings' ][ k ]
				else:
					collated[ condition ] = copy.deepcopy( info )
					collated[ condition ][ 'cycleNumber' ] = [ 0 ] * len( collated[ condition ][ 'intervalsMS' ] )
					collated[ condition ][ 'filename' ] = filename
					collated[ condition ][ 'condition' ] = condition
					master_filenames.append( filename )
		all_info = { info[ 'filename' ]: info for info in collated.values() }
		filenames = master_filenames
		
	stimulusConditions = sorted( set( info[ 'nStimuli' ] for info in all_info.values() ) )
	nStimulusConditions = len( stimulusConditions )
	nTraces = len( filenames )
	nContexts = ( nTraces // nStimulusConditions ) if separate else 1
	if nStimulusConditions > 1: hist = False
	
	conditionForYLabel = min( stimulusConditions )

	if   legend == 'none':   conditionForLegend = -1
	elif legend == 'left':   conditionForLegend = min( stimulusConditions ); legendLocation = 'upper left'
	elif legend == 'right':  conditionForLegend = max( stimulusConditions ); legendLocation = 'upper right'
	elif legend == 'center': conditionForLegend = stimulusConditions[ len( stimulusConditions ) // 2 ];  legendLocation = 'upper center'
	
	plt.figure( figsize=[ 12, 8 ] )
	
	def MakeSubplot( layout, coords, span=None ):
		if transpose:
			layout = layout[ ::-1 ] 
			coords = coords[ ::-1 ] 
			span = { 'colspan' : span } if span else {}
		else:
			span = { 'rowspan' : span } if span else {}
		ax = plt.subplot2grid( layout, coords, **span )
		ax.coords = coords
		ax.left   = coords[ 1 ] == 0
		ax.right  = coords[ 1 ] == layout[ 1 ] - 1
		ax.top    = coords[ 0 ] == 0
		ax.bottom = coords[ 0 ] == layout[ 0 ] - 1
		return ax

	if hist:
		histAx = MakeSubplot( ( nTraces, 2 ), ( 0, 1 ), span=nTraces )
		axesList = []
		if separate:
			for index, filename in enumerate( filenames ):
				ax = MakeSubplot( ( nTraces, 2 ), ( index, 0 ) )
				axesList.append( ax )
				all_info[ filename ][ 'timeSeriesAxes' ] = ax
				all_info[ filename ][ 'axesList' ] = axesList
		else:
			ax = MakeSubplot( ( nTraces, 2 ), ( 0, 0 ), span=nTraces )
			axesList.append( ax )
			for index, filename in enumerate( filenames ):
				all_info[ filename ][ 'timeSeriesAxes' ] = ax
				all_info[ filename ][ 'axesList' ] = axesList
	else:
		axes = {}
		for iStimulusCondition, nStimuli in enumerate( stimulusConditions ):
			axesList = axes[ nStimuli ] = []
			for filename in filenames:
				info = all_info[ filename ]
				if info[ 'nStimuli' ] != nStimuli: continue
				info[ 'axesList' ] = axesList
				if separate or not axesList:
					info[ 'timeSeriesAxes' ] = ax = MakeSubplot( ( nContexts, nStimulusConditions ), ( len( axesList ), iStimulusCondition ) )
					axesList.append( ax )
				else:
					info[ 'timeSeriesAxes' ] = ax = axesList[ -1 ]
	
	markers = [ 'o', 'x', 's', '^' ]
	for index, filename in enumerate( filenames ):
		info = all_info[ filename ]
		intervalsMS = info[ 'intervalsMS' ]
		
		m = numpy.mean( intervalsMS )
		sd = numpy.std( intervalsMS )
		se = sd / numpy.sqrt( len( intervalsMS ) ) # could use this for CI of the mean
		
		msg = "Mean=%.1fms, s.d.=%.2f, 99%%\nCI(frame)=%.2f-%.2f"
		distString = msg % ( m, sd, m - 2.58 * sd, m + 2.58 * sd )
		nTotal = len( intervalsMS )
		target = 1000.0 / fps
		nDropped = sum( interval > ( 1.5 * target ) for interval in intervalsMS )
		label = info[ 'toolbox' ]
		if not showVersion: label = re.sub( r'\s+\d+\.\d+\.\d+\s+', ' ', label  )
		if info[ 'process_priority' ] != 'normal': label += ' --priority=%s' % info[ 'process_priority' ]
		if info[ 'disable_gc' ]: label += ' --disable_gc'
		

		# plot the frameintervals

		color = [1,0,0] if 'psychopy' in info[ 'toolbox' ].lower() else [0,0.7,0] if 'pure' in info[ 'toolbox' ].lower() else [0,0,1]
		color = numpy.array( color, dtype=float )	
		
		if hist:
			plt.sca( histAx )
			marker = markers.pop( 0 ); markers.append( marker )
			h, = plt.plot( *Histogram( intervalsMS, limits, 2 ), color=color, marker=marker, label=r'%s: %.2f ms $\pm$ %.2f SD' % ( info[ 'toolbox' ], m, sd ), zorder=sd )
		
			plt.xlim( limits )
			plt.xlabel( 'Frame-to-Frame Interval (ms)' )
			plt.ylabel( 'Number of Frames' )
			plt.grid( True )
			plt.legend( loc='upper right' )
		
		ax = info[ 'timeSeriesAxes' ]
		plt.sca( ax )
		plt.plot( intervalsMS, color=color, label=label, zorder=-sd )
		#plt.plot( numpy.diff( info[ 'cycleNumber' ] ) * target * 0.9, color=[1,0,1], zorder=-sd, alpha=0.5 )
		if ax.right: plt.plot( 1.02, target, marker='<', markersize=10, color='k', clip_on=False, transform=ax.get_yaxis_transform() )
		if detail:
			color = 0.5 + 0.5 * color; Patch( info[ 'timings' ][ 'World_RunPending' ], color=color, zorder=-sd )
			color = 0.5 + 0.5 * color; Patch( info[ 'timings' ][ 'World_BeginFrameCallback' ], color=color, zorder=-sd )
		plt.ylim( limits )
		plt.xlim( [ 0, nTotal ] )
		plt.grid( True )
		if ax.left: plt.ylabel( 'Frame-to-Frame Interval (ms)' )
		if ax.bottom: plt.xlabel( 'Frame Number' )
		if ax.top and not transpose: plt.title( '%d stimulus patch%s' % ( info[ 'nStimuli' ], '' if info[ 'nStimuli' ] == 1 else 'es'  ))
		msg = 'Dropped %d ' % nDropped
		ax.dropped = plt.text( 0.5, 0.7, msg, transform=ax.transAxes, ha='center', va='center' )
		ax.info = info # NB: circular reference
		if info[ 'nStimuli' ] == conditionForLegend: plt.legend( loc=legendLocation )
		out[ ax.coords ] = ax
	return out

def TouchUpAxes( d, size=None, wspace=0.08, hspace=0.05, **axprops ):
	title_size = axprops.pop( 'title', size )
	dropped_size = axprops.pop( 'dropped', size )
	dropped_y = axprops.pop( 'dropped_y', None )
	label_size = axprops.pop( 'axislabels', size )
	legend_size = axprops.pop( 'legend', size )
	tick_size = axprops.pop( 'ticklabels', size * 0.7 if size else None )
	if isinstance( d, dict ): d = list( d.values() )
	for ax in d:
		if title_size: ax.title.set_size( title_size )
		if dropped_size: ax.dropped.set_size( dropped_size )
		if label_size:
			ax.yaxis.label.set_size( label_size )
			ax.xaxis.label.set_size( label_size )
		if legend_size and ax.legend_:
			for t in ax.legend_.texts:
				t.set_size( legend_size )
		if axprops: ax.set( **axprops )
		maxxlim = max( ax.get_xlim() )
		if not ax.left: ax.set_yticklabels([])
		if not ax.bottom: ax.set_xticklabels([])
		elif not ax.right:
			#print( [ tl.get_text() for tl in ax.get_xticklabels() ] )
			ax.set_xticklabels( [ ( '%g'%x if float( x ) < maxxlim else '' ) for x in ax.get_xticks() ] )
		if tick_size:
			for t in list( ax.get_xticklabels() ) + list( ax.get_yticklabels() ):
				t.set_size( tick_size )
		if dropped_y is not None:
			ax.dropped.set_y( dropped_y )
	plt.subplots_adjust( wspace=wspace, hspace=hspace )
	plt.draw()

if __name__ == '__main__':
	cmdline = Shady.CommandLine()
	cmdline.Option( 'separate',     True, type=bool, doc="Whether to plot time series as separate subplots." )
	cmdline.Option( 'detail',       True, type=bool, doc="Whether to plot draw duration and update duration in addition to frame-to-frame interval." )
	cmdline.Option( 'collate',      True, type=bool, doc="Whether to collate timing information across similar files." )
	cmdline.Option( 'hist',         True, type=bool, doc="Whether to plot a histogram." )
	cmdline.Option( 'transpose',   False, type=bool, doc="Whether to spatially transpose the array of subplots." )
	cmdline.Option( 'limits',       None, type=( tuple, list, None ), length=2, doc="Time axis limits: [min,max] in milliseconds." )
	cmdline.Option( 'fps',          None, type=( int, float, None ), min=1.0, doc="Presumed frame rate in Hz." )
	cmdline.Option( 'legend',    'right', type=str, strings=[ 'left', 'center', 'right', 'none' ], doc="Where (i.e. on which column of plots) to put the legend." )
	cmdline.Option( 'patches',      None, type=( list, int, None ), minlength=1, doc="Optionally specifies exactly which number(s) of stimulus patches to filter for." )
	saveas = cmdline.Option( 'saveas',         '', type=str, doc="Output filename for figure.", container=None )
	cmdline.Help().Delegate()
	
	if cmdline.opts[ 'fps' ] is None:
		cmdline.opts[ 'fps' ] = 60.0
		print( '\n!!! ASSUMING --fps=60  !!!\n' )
	
	if 'IPython' in sys.modules: plt.ion()
	v = Report( sys.argv[ 1: ], **cmdline.opts )
	#TouchUpAxes( v, 20, axislabels=14, ticklabels=10 )
	TouchUpAxes( v, 15, axislabels=12, title=12, ticklabels=10, dropped_y=0.8 )
	Shady.Utilities.FinishFigure( maximize=True, wait=False )
	if saveas: plt.savefig( os.path.expanduser( saveas ) )
	Shady.Utilities.FinishFigure()
	