@for /L %%i in ( 1, 1, 25 ) do (
	python %~dp0\psychopyTimeByFramesEx.py   --nIntervals=100 --nStimuli=1
	python %~dp0\shadyTimeByFrames.py --pure --nIntervals=100 --nStimuli=1
	python %~dp0\shadyTimeByFrames.py        --nIntervals=100 --nStimuli=1
	python %~dp0\psychopyTimeByFramesEx.py   --nIntervals=100 --nStimuli=2
	python %~dp0\shadyTimeByFrames.py --pure --nIntervals=100 --nStimuli=2
	python %~dp0\shadyTimeByFrames.py        --nIntervals=100 --nStimuli=2
	python %~dp0\psychopyTimeByFramesEx.py   --nIntervals=100 --nStimuli=3
	python %~dp0\shadyTimeByFrames.py --pure --nIntervals=100 --nStimuli=3
	python %~dp0\shadyTimeByFrames.py        --nIntervals=100 --nStimuli=3
	python %~dp0\psychopyTimeByFramesEx.py   --nIntervals=100 --nStimuli=4
	python %~dp0\shadyTimeByFrames.py --pure --nIntervals=100 --nStimuli=4
	python %~dp0\shadyTimeByFrames.py        --nIntervals=100 --nStimuli=4
	python %~dp0\psychopyTimeByFramesEx.py   --nIntervals=100 --nStimuli=5
	python %~dp0\shadyTimeByFrames.py --pure --nIntervals=100 --nStimuli=5
	python %~dp0\shadyTimeByFrames.py        --nIntervals=100 --nStimuli=5
)
