@for /L %%i in ( 1, 1, 25 ) do (
	python "%~dp0\psychopyTimeByFramesEx.py"   --nIntervals=100 --nStimuli=3 --priority=normal
	python "%~dp0\psychopyTimeByFramesEx.py"   --nIntervals=100 --nStimuli=3 --priority=high
	python "%~dp0\psychopyTimeByFramesEx.py"   --nIntervals=100 --nStimuli=3 --priority=realtime
)
