import Shady
import numpy

cmdline = Shady.WorldConstructorCommandLine( threaded=False, debugTiming=True, canvas=True, logfile='{}.log' )
config = {}
size         = cmdline.Option( 'size',            40, type=( int, float ), min=0, container=config )
territory    = cmdline.Option( 'territory',       80, type=( int, float ), min=1, container=config )
pace         = cmdline.Option( 'pace',             6, type=( int, float ), min=0, container=config )
batchSize    = cmdline.Option( 'batchSize',        6, type=int, min=0, container=config )
maxStimuli   = cmdline.Option( 'maxStimuli',     108, type=int, min=0, container=config )
textured     = cmdline.Option( 'textured',     False, type=bool, container=config, doc="""
If True, a separate (independent) random texture is added to each patch. """ )
gabor        = cmdline.Option( 'gabor',        False, type=bool, container=config )
leave        = cmdline.Option( 'leave',         True, type=bool, container=config )
cancellation = cmdline.Option( 'cancellation', False, type=bool, container=config, doc="""\
Disables animation of patches. Sets gamma to 1.0 and patches' alpha to 0.5, so their
intensity is exactly averaged with whatever is in the background. As a background, this
option creates a textured canvas on which are drawn, statically, white silhouettes of the
patches. This canvas reverses its contrast on every frame. The patches, when they appear,
reverse their contrast in antiphase to the canvas (so they should average out, with the
canvas, to match the background gray). The canvas is drawn first on each frame. If all
patches are then drawn in time, they should cancel the canvas texture exactly.""" )
wait         = cmdline.Option( 'wait',           0.0, type=( int, float ), container=config, doc="""\
Wait for the specified number of seconds after the patches are finished, before removing them.""" )
tearing      = cmdline.Option( 'tearing',        0.0, type=( int, float ), container=config, doc="""\
Performs a tearing test for the specified number of seconds after the patches are finished.""" )
animate      = cmdline.Option( 'animate',       not cancellation, type=bool, container=config )
independent  = cmdline.Option( 'independent',   True, type=bool, container=None, doc="""\
If True, each patch is a separate Stimulus. If False, each patch is a polygon within the
same (single) Stimulus.""" ); config[ 'independent_stimuli' ] = independent  # backwardly-compatible name :-/
plot         = cmdline.Option( 'plot',          True, type=bool, container=None )
cmdline.Help().Finalize()

if cancellation:
	cmdline.opts[ 'canvas' ] = True
	gamma = 1.0
	targetAlpha = 0.5
	if textured: raise ValueError( 'cannot use --textured and --cancellation options together' )
	if animate:  raise ValueError( 'cannot use --animate and --cancellation options together' )
else:
	gamma = -1
	targetAlpha = 1.0

sm = Shady.StateMachine()

@sm.AddState
class Begin( Shady.StateMachine.State ):
	duration = pace
	next = 'Appear'

if not cancellation:
	pace /= ( 3.0 if animate else 2.0 )

@sm.AddState
class Appear( Shady.StateMachine.State ):
	duration = pace
	next = 'Appear' if cancellation else 'Enlarge'
	queue = []
	def onset( self ):
		if independent:
			if not self.queue: return self.Change( 'Finish' )
			for i in range( batchSize ):
				if self.queue:
					a = self.queue.pop( 0 )
					a.visible = True
					a.alpha = targetAlpha
					if leave: a.Enter()
					Enlarge.queue.append( a )
		else:
			field = w.stimuli.field
			if field.nPointsVisible >= maxStimuli: return self.Change( 'Finish' )
			newValue = min( field.nPointsVisible + batchSize, maxStimuli )
			Enlarge.queue.append( numpy.arange( field.nPointsVisible, newValue ) )
			field.nPointsVisible = newValue
			
@sm.AddState
class Enlarge( Shady.StateMachine.State ):
	duration = pace
	next = 'Animate' if animate else 'Appear'
	queue = []
	def onset( self ):
		if independent:
			for i in range( batchSize ):
				if self.queue:
					a = self.queue.pop( 0 )
					a.width *= 2 ** 0.5
					a.height *= 2 ** 0.5
					Animate.queue.append ( a )
		else:
			while self.queue:
				ind = self.queue.pop( 0 )
				field = w.stimuli.field
				field.radii.flat[ ind ] *= 2 ** 0.5
				Animate.queue.append ( ind )
		
@sm.AddState
class Animate( Shady.StateMachine.State ):
	duration = pace
	next = 'Appear'
	queue = []
	def onset( self ):
		if independent:
			for i in range( batchSize ):
				if self.queue:
					stim = self.queue.pop( 0 )
					#stim.noise = -0.1
					master = stim.world().stimuli.stim000
					if stim is master: stim.rotation = lambda t: t * 30
					else: stim.rotation = master
		else:
			while self.queue:
				ind = self.queue.pop( 0 )
				field = w.stimuli.field
				field.angular_speeds.flat[ ind ] += 30
			
@sm.AddState
class Finish( Shady.StateMachine.State ):
	duration = wait
	next = 'Tearing'
@sm.AddState
class Tearing( Shady.StateMachine.State ):
	duration = tearing
	next = 'Close'
	def onset( self ): Shady.TearingTest( w )
@sm.AddState
class Close( Shady.StateMachine.State ):
	def onset( self ): w.Close()
	
class TestWorld( Shady.World ):
	def Prepare( self ):
		self.patches = []
		self.patchTextureSlots = []
		self.Animate = sm
		self.logger.Log( config=config )
		self.logger.logSystemInfoOnClose = False
		if cancellation:
			canvasTexture = numpy.ones( [ self.height, self.width, 3 ], dtype=float ) * [ [ list( self.backgroundColor ) ] ]

		intensity = 254.5 / 255
		locations = []
		x = y = territory // 2
		global maxStimuli
		for i in range( maxStimuli ):
			locations.append( x + 1.0j * y )
			if cancellation:
				canvasTexture[ -y-size+size//2:-y+size//2, :, : ][ :, x-size//2:x+size-size//2, : ].flat = intensity
			x += territory
			if x + size // 2 > self.width: x = territory // 2; y += territory
			if y + size // 2 > self.height: break
		maxStimuli = len( locations )
		if independent:
			for i in range( maxStimuli ):
				if textured: texture = numpy.random.uniform( 0.1, 0.9, size=[ size, size, 3 ] )
				else: texture = None
				stim = self.Stimulus(
					texture,
					name = 'stim%03d' % i,
					x = locations[ i ].real,
					y = locations[ i ].imag,
					size = size,
					debugTiming = False,
					alpha = 0,
					atmosphere = self,
					
					signalFunction = Shady.SIGFUNC.SinewaveSignal if gabor else 0,
					signalAmplitude = 0.5 if gabor else 1.0,
					signalPhase = ( lambda t: t * 90  ) if gabor else 0,
					plateauProportion = 0 if gabor else -1,
				)
				if not textured: stim.color = intensity
				if leave: stim.Leave()
				Appear.queue.append( stim )
				self.patches.append( stim )
				self.patchTextureSlots.append( stim.textureSlotNumber )
				if i == 0: master = stim
				else: stim.contrast = master
		else:
			if textured: texture = numpy.random.uniform( 0.1, 0.9, size=[ self.height, self.width, 3 ] )
			else: texture = None
			field = self.Stimulus(
				texture,
				name = 'field',
				anchor = -1,
				position = self.Place( -1 ),
				drawMode = Shady.DRAWMODE.POLYGON,
				color = -1 if textured else intensity,
				alpha = targetAlpha,
				debugTiming = False,
				atmosphere = self,
			)
			field.locations = numpy.array( locations )[ :, None ]
			field.angular_speeds = field.locations.real * 0
			field.radii = field.locations.real * 0 + size / 2 ** 0.5
			field.shape = Shady.ComplexPolygonBase( 4 )
			field.nPointsVisible = 0
			@field.AnimationCallback
			def Animate( self, t ):
				self.points = self.locations + self.radii * self.shape * 1j ** ( 0.5 + self.angular_speeds * t / 90.0 )
				self.nPoints = self.nPointsVisible * self.shape.size
			master = field
		if cancellation:
			canvas = self.stimuli.canvas
			canvas.LoadTexture( canvasTexture )
			master.contrast = lambda t: -canvas.contrast
			canvas.contrast = lambda t: -canvas.contrast
		
w = TestWorld( anchor=-1, gamma=gamma, **cmdline.opts )
if textured:
	@w.EventHandler
	def TextureToggler( self, event ):
		if event >> "kp[t]":
			if 'shift' in event.modifiers:
				for stim, slot in zip( self.patches[ 1: ], self.patchTextureSlots[ 1: ] ):
					stim.MakePropertiesIndependent( textureSlotNumber=slot )
			else:
				self.patches[ 0 ].ShareProperties( 'textureSlotNumber', *self.patches[ 1: ] )
Shady.AutoFinish( w, plot=( lambda: Shady.PlotTimings( w ) ) if plot else None )
