import os, sys, glob

import Shady
plt = Shady.DependencyManagement.LoadPyplot()

cmdline = Shady.Utilities.CommandLine()
canvas        = cmdline.Option( 'canvas',       None, type=( None, bool ) )
independent   = cmdline.Option( 'independent',  None, type=( None, bool ) )
acceleration  = cmdline.Option( 'acceleration', 'pure|hybrid|accelerated|mystery', type=( None, str ) )
acceleration = [ x.lower().strip() for x in acceleration.replace( '+', '|' ).split( '|' ) ]
patterns = cmdline.Delegate()
logs = [ Shady.Logging.Read( filename ) for pattern in patterns for filename in sorted( glob.glob( pattern ) ) ]
for log in logs:
	if 'pyglet'     in log[ 'versions' ] and 'ShaDyLib' not in log[ 'versions' ]: log[ 'config' ][ 'acceleration' ] = 'pure'
	if 'pyglet' not in log[ 'versions' ] and 'ShaDyLib'     in log[ 'versions' ]: log[ 'config' ][ 'acceleration' ] = 'accelerated'
	if 'pyglet'     in log[ 'versions' ] and 'ShaDyLib'     in log[ 'versions' ]: log[ 'config' ][ 'acceleration' ] = 'hybrid'
	if 'pyglet' not in log[ 'versions' ] and 'ShaDyLib' not in log[ 'versions' ]: log[ 'config' ][ 'acceleration' ] = 'mystery'
logs.sort( key=lambda log: ( log[ 'config' ][ 'acceleration' ], log[ 'config' ][ 'independent_stimuli' ], log[ 'world_construction' ][ 'canvas' ], log[ 'starttime' ] ) )
if canvas in [ True, False ]: logs = [ log for log in logs if log[ 'world_construction' ][ 'canvas' ] == canvas ]
if independent in [ True, False ]: logs = [ log for log in logs if log[ 'config' ][ 'independent_stimuli' ] == independent ]
logs = [ log for log in logs if log[ 'config' ][ 'acceleration' ].lower() in acceleration ]
nPlots = len( logs )
fig, axes = plt.subplots( 2, ( nPlots + 1 ) // 2, sharex=True, sharey=True )
if axes.ndim == 1: axes = axes[ :, None ]

for ax, log in zip( axes.T.flat, logs ):
	Shady.PlotTimings( log[ 'timings' ], axes=ax, traces=[
		'DeltaDrawTimeInMilliseconds = Frame-to-frame interval',
		'TestWorld_RunPending = Inter-frame update code finished',
		'TestWorld_BeginFrameCallback = Rendering commands issued',
	], finish=False )
	plt.ylim( [ 0, 35 ] )
	title = 'Independent stimuli' if log[ 'config' ][ 'independent_stimuli' ] else 'Disjoint patches of same stimulus'
	accstrings = dict( pure='No acceleration', hybrid='Hybrid acceleration', accelerated='Accelerated', mystery='Unaccelerated, mystery windowing (???)' )
	title += '\n' + accstrings.get( log[ 'config' ][ 'acceleration' ])
	if not log[ 'world_construction' ][ 'canvas' ]: title += ' - no canvas'
	plt.title( title )
	if ax in axes[ :, 0 ]: ax.set_ylabel( 'milliseconds' )
	if ax in axes[ -1, : ]: ax.set_xlabel( 'seconds ($\simeq$ number of patches)' )
	if ax is axes.flat[ 0 ]:
		legend = plt.legend( loc='center', bbox_to_anchor=[ 1.1, -0.1 ] )
		try: legend.draggable( True )
		except: legend.set_draggable( True )
	else:
		try: plt.legend().remove()
		except Exception as err: print( err )

Shady.Utilities.FinishFigure( maximize=True, raise_=True, zoom=True )
