import Shady



gamma = [ 1.9,1.9,1.9 ] # interactive by eye on snap02 (Horizon II)
#gamma = [ 1.8,1.8,1.9 ] # interactive by eye on snap10 (Dell OptiPlex)
w = Shady.World( canvas=True, gamma=gamma )
flicker = w.Stimulus( size=w.size, anchor=Shady.LEFT,  position=w.Place( +0.8, 0.0 ), atmosphere=w, color=1 )
mask    = w.Stimulus( size=w.size, anchor=Shady.RIGHT, position=w.Place( -0.8, 0.0 ), atmosphere=w, color=0 )
w.midpoint = 0.5
w.amplitude = 0.5

def ClusterFlicker( world ):
	"""
	This is designed to test (in combination with a real-time USB light meter)
	the temporal bandwidth of the screen, because often a screen will take >1
	frame to reach full amplitude (see Ghodrati, Morris & Price 2015, Frontiers
	in Psychology).  This test performs a full contrast 1-frame-on-1-frame-off
	flicker for 60 frames, then 2-frames-on-2-frames-off for 60 frames, then 3,
	then 4, then 5, then 6 before looping back to 1.
	
	WARNING: on some LCD screens, running this for an extended period (a few minutes)
	has been observed to cause long-lasting location-specific flicker after-effects. 
	These after-effects persist even if the computer is turned off for several minutes
	and restarted.  The first time it happened (on a Dell OptiPlex touchscreen) the
	effect disappeared after the computer and screen were left off for a couple of days.
	The second time, the effect was induced in about 5 minutes and disappeared over the
	course of about an hour. As yet, we have no idea how long it really takes in general
	to induce or recover, how recovery time might vary as a function of induction time,
	WHETHER THE DAMAGE COULD BECOME PERMANENT under some circumstances, or whether the
	existence of the phenomonon at all indicates that the hardware is faulty. Similar
	reports, with (not especially well-informed) discussion, can be found at:

		https://steamcommunity.com/app/235460/discussions/0/540731691457198542/
		https://steamcommunity.com/app/235460/discussions/0/630802344115823048/
	"""
	f = world.framesCompleted
	nFramesPerSection = 60
	sections = [ 1, 2, 3, 4, 5, 6 ]
	f %= ( nFramesPerSection * len( sections ) )
	sectionIndex = f // nFramesPerSection
	f %= nFramesPerSection
	framesOn = sections[ sectionIndex ]
	f //= framesOn
	if sections[ sectionIndex ] > 4: f += 1
	result = 1 if ( f % 2 ) else -1
	return result

w.oscillators = [ Shady.Function( 0 ), Shady.Oscillator( 0.5 ), Shady.Oscillator( 1.0 ), Shady.Oscillator( 2.0 ), Shady.Function( lambda t: ClusterFlicker( w ) ) ]
@w.EventHandler( slot=-1 )
def Toggle( self, event=None ):
	if event and ( event.type != 'key_press' or event.key not in [ 'space', ' ' ] ): return
	f = self.oscillators.pop( 0 )
	self.oscillators.append( f )
	flicker.color = f * ( lambda t: self.amplitude ) + ( lambda t: self.midpoint )
	mask.color = w.bgcolor
	
Shady.AutoFinish( w )
