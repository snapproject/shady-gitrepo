//   env MACOSX_DEPLOYMENT_TARGET=10.9   g++ -std=c++11 scraps/timing.cpp -lc++
#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cstdio>
#include <chrono>  // C++11
#include <thread>  // C++11
using namespace std;


#ifdef _WIN32
#include "Windows.h"
double gCPUTicksPerSecond = 1e7;
ULONGLONG WinCPUTime()
{
	FILETIME creation, exit, kernel, user;
	GetProcessTimes( GetCurrentProcess(), &creation, &exit, &kernel, &user );
	ULARGE_INTEGER u, k;
	u.HighPart = user.dwHighDateTime;
	u.LowPart  = user.dwLowDateTime;
	k.HighPart = kernel.dwHighDateTime;
	k.LowPart  = kernel.dwLowDateTime;
	return u.QuadPart + k.QuadPart;
}
#else
double gCPUTicksPerSecond = CLOCKS_PER_SEC;
#endif

#define WALL_CLOCK  steady_clock   // high_resolution_clock is not necessarily monotonic
double gWallTicksPerSecond = std::chrono::WALL_CLOCK::period::den;
double gTicksPerSecond = min( gWallTicksPerSecond, gCPUTicksPerSecond );

double Datestamp( void )
{
	return std::chrono::duration_cast< std::chrono::milliseconds >( std::chrono::system_clock::now().time_since_epoch() ).count() / 1000.0;
}

double CPUTime( void )
{
#ifdef _WIN32
	static ULONGLONG t0 = WinCPUTime();
	ULONGLONG t = WinCPUTime();
	double elapsedSinceFirst = t - t0;  // 100ns intervals
#else
	static clock_t t0 = clock();
	clock_t t = clock(); // under MSVC this is a wall time (includes sleeps)
	double elapsedSinceFirst = t - t0;;
#endif
	elapsedSinceFirst /= gCPUTicksPerSecond;
	elapsedSinceFirst = ( int )( elapsedSinceFirst * gTicksPerSecond ) / gTicksPerSecond;
	return elapsedSinceFirst;
}

double WallTime( void )
{
	static std::chrono::WALL_CLOCK::time_point t0 = std::chrono::WALL_CLOCK::now();
	std::chrono::WALL_CLOCK::time_point t = std::chrono::WALL_CLOCK::now();
	double elapsedSinceFirst = std::chrono::duration_cast< std::chrono::microseconds >( t - t0 ).count() * 1e-6;
	elapsedSinceFirst = ( int )( elapsedSinceFirst * gTicksPerSecond ) / gTicksPerSecond;
	return elapsedSinceFirst;
}

typedef unsigned int number_t;
int main( int argc, const char * argv[] )
{
	double previousWallTime, currentWallTime;
	double previousCPUTime, currentCPUTime;
	number_t tests = 0, batchSize = 10000000;
	vector< number_t > primes;
	vector< pair< double, double > > times;
	
	FILE * out = stdout;
	if( argc > 1 ) out = fopen( argv[ 1 ], "wt" );
	fprintf( out, "cpp = d = {}\n" );
	fprintf( out, "d[ 'batch_size' ] = %d\n", batchSize );
	fprintf( out, "d[ 'ticks_per_sec' ] = (%g, %g)\n", gWallTicksPerSecond, gCPUTicksPerSecond );
	fprintf( out, "d[ 'times' ] = t = []; t = t.append\n" );
	//int t = Datestamp();
	//while( ( int )Datestamp() <= t ) std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );
	while( true )
	{
		primes.clear();
		times.clear();
		previousWallTime = WallTime();
		previousCPUTime = CPUTime();
		for( number_t candidate = 2; ; candidate++ )
		{
			bool isPrime = true;
			for( vector< number_t >::iterator prime = primes.begin(); prime != primes.end(); prime++ )
			{
				number_t divisor = *prime;
				if( divisor * divisor > candidate ) break;
				if( ( ++tests % batchSize ) == 0 )
				{
					currentWallTime = WallTime();
					currentCPUTime = CPUTime();
					double elapsedWallTime = currentWallTime - previousWallTime;
					double elapsedCPUTime  = currentCPUTime  - previousCPUTime;
					times.emplace_back( elapsedWallTime, elapsedCPUTime );
					//fprintf( out, "%d\n", primes.size() );
					double absTime = Datestamp();
					fprintf( out, "t(( %.3f, %.5f, %.5f ))", absTime, elapsedWallTime, elapsedCPUTime );
					fprintf( out, "     #   % 11.6f ms = %.2f %%\n", 1000.0 * ( elapsedWallTime - elapsedCPUTime ), 200.0 * ( elapsedWallTime - elapsedCPUTime ) / ( elapsedWallTime + elapsedCPUTime ) );
					fflush( out );
					previousWallTime = WallTime();
					previousCPUTime  = CPUTime();
					//std::this_thread::sleep_for( std::chrono::milliseconds( 300 ) );
				}
				number_t factor = candidate / divisor;
				if( factor * divisor == candidate ) { isPrime = false; break; }
			}
			if( isPrime )
			{
				primes.push_back( candidate );
				if( primes.size() > 500000 ) break;
			}
		}
	}
	return 0;
}
