# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
import sys
import time
import threading

import numpy
import psutil

class Recorder( object ):
	def __init__( self, nTicks=1000 ):
		self.arrays = {}
		self.processes = {}
		self.bufferSize = nTicks
		self.ticks = 0
		self.t = 0.0
		self.t0 = 0.0
	
	def GetArray( self, key ):
		array = self.arrays.get( key, None )
		if array is None: array = self.arrays[ key ] = numpy.zeros( [ self.bufferSize ], dtype='float64' ) + numpy.nan
		return array

	def Prime( self ):
		self.Tick( verbose=True ).ticks = 0
		return self
		
	def Tick( self, verbose=False ):
		self.t = self.Record( key='t', value=time.time() )
		if self.ticks == 0: self.t0 = self.t
		for p in psutil.process_iter():
			key = p.pid
			if key not in self.processes:
				try: self.processes[ key ] = p.as_dict()
				except: print( '.as_dict() failed on process %r' % p )
			try: self.Record( key=key, value=p.cpu_percent() )
			except psutil.ZombieProcess: pass
			if verbose: print( len( self.processes ) )
		self.ticks += 1
		return self
	
	def Record( self, key, value ):
		array = self.GetArray( key )
		array[ self.ticks % array.size ] = value
		return value
		
	def Freeze( self, sort_key='cpu_percent_max' ):
		t = self.arrays[ 't' ]
		keep = ~numpy.isnan( t )
		t = t[ keep ]
		reorder = numpy.argsort( t )
		t = t[ reorder ] - self.t0
		self.frozen_t = t
		self.frozen = [ ( self.processes[ k ], a[ keep ][ reorder ].copy() ) for k, a in self.arrays.items() if k != 't' and k in self.processes ]
		for info, vals in self.frozen:
			info[ 'cpu_percent_avg' ] = numpy.nanmean( vals )
			info[ 'cpu_percent_max' ] = numpy.nanmax( vals )
		self.frozen.sort( key=lambda x: -x[ 0 ][ sort_key ] )
		return self
		
	def WaitForCommand( self, prompt=None, targets=(), after=None ):
		cmd = ''
		while not cmd:
			if prompt:
				sys.stdout.write( prompt )
				try: sys.stdout.flush()
				except: pass
			cmd = sys.stdin.readline()
			#print( repr( cmd ) )
			if not cmd: return None
			cmd = cmd.strip().lower()
			if targets and cmd not in targets: cmd = ''
		if after: after()
		return cmd
		
	def WaitForGreenLight( self ):
		sys.stdout.write( 'ready\n' )
		try: sys.stdout.flush()
		except: pass
		if self.WaitForCommand( ': ' ) in 'exit quit q escape stop'.split(): return None
		return self
		
	def Go( self ):
		try:
			self.Prime()
			self.keep_going = True
			while self.keep_going:
				print( self.ticks )
				self.Tick()
				time.sleep( 0.010 )
		except KeyboardInterrupt:
			self.Stop()
		return self
	
	def Stop( self ):
		self.keep_going = False

	def Plot( self, sort_key='cpu_percent_max', cutoff=5.0, wait=None, **kwargs ):
		self.Freeze( sort_key=sort_key )
		import matplotlib, matplotlib.pyplot as plt
		plt.clf()
		for i, ( info, vals ) in enumerate( self.frozen ):
			cpu_percent = info[ sort_key ]
			if cpu_percent < cutoff: continue
			if info[ 'cmdline' ]:
				label = ' '.join( ( ( '"' + x + '"' ) if ' ' in x else x ) for x in info[ 'cmdline' ] )
			else:
				label = '(' + info[ 'name' ] + ')'
			label = '%02d %s' % ( cpu_percent, label )
			plt.plot( self.frozen_t, vals, label=label, **kwargs )
		legend = plt.legend( loc=( 1.0, 0.0 ) )
		try: legend.draggable( True )
		except: legend.set_draggable( True )
		plt.subplots_adjust( right=0.8 )
		from Shady.Utilities import FinishFigure; FinishFigure( maximize=True, raise_=True, wait=wait )
		return self
			
if __name__ == '__main__':
	p = Recorder().Prime().WaitForGreenLight()
	if p:
		threading.Thread( target=p.WaitForCommand, kwargs=dict( prompt='', targets='exit quit q stop escape'.split(), after=p.Stop ) ).start()
		p.Go().Plot( marker='x' )
