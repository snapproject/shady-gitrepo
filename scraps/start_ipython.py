import sys
import IPython

start_ipython = None
if not start_ipython:
	try: from IPython import start_ipython
	except ImportError: pass
if not start_ipython:
	try: from IPython.terminal.ipapp import launch_new_instance as start_ipython
	except ImportError: pass
if not start_ipython:
	try: from IPython.frontend.terminal.ipapp import launch_new_instance as start_ipython
	except ImportError: pass
if not start_ipython:
	try: from pkg_resources import load_entry_point
	except ImportError: pass
	else: start_ipython = load_entry_point( 'ipython', 'console_scripts', 'ipython' )
if not start_ipython:
	try: from pkg_resources import run_script
	except ImportError: pass
	else: start_ipython = lambda: 1 if run_script( 'ipython', 'ipython' ) else 0
if not start_ipython:
	raise ImportError( 'run out of options for launching IPython version %s under Python version %s' % ( IPython.__version__, sys.version ) )

# problem: only the authentic `start_python` takes args  `argv` and (more importantly) `user_ns`;
# `launch_new_instance` does not
