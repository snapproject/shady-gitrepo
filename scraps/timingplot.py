import os
import sys
import glob
import time

import numpy

def CollateTimingResults( *sources ):
	results = []
	for source in sources:
		if isinstance( source, dict ):
			results.append( source )
		elif isinstance( source, list ):
			results.extend( source )
		else:
			with open( source, 'rt' ) as fh: txt = fh.read()
			namespace = {}
			exec( txt, namespace, namespace )
			k, v = [ [ k, v ] for k,v in namespace.items() if k in [ 'cpp', 'py' ] ][ 0 ]
			v[ 'type' ] = k
			v[ 'filename' ] = source
			a = numpy.array( v.pop( 'times' ) )
			v[ 't' ] = a[ :, 0 ]
			v[ 'deltaWallTime' ] = a[ :, 1 ]
			v[ 'deltaCpuTime' ] = a[ :, 2 ]
			v[ 't0' ] = v[ 't' ].min()
			v[ 't' ] -= v[ 't0' ]
			results.append( v )
	
	overall_t0 = min( v[ 't0' ] for v in results )
	for v in results:
		v[ 't' ] -= overall_t0 - v[ 't0' ]
		v[ 't0' ] = overall_t0
	return results
		
def PlotTimingResults( *sources, **kwargs ):
	results = CollateTimingResults( *sources )
	import matplotlib.pyplot as plt
	if 'IPython' in sys.modules: plt.ion()
	else: plt.ioff()
	plt.cla()
	for v in results:
		denom = 1
		#denom = v[ 'batch_size' ]
		k = 'deltaWallTime'
		hw, = plt.plot( v[ 't' ], v[ k ] / denom, label='%s %s' % ( v[ 'type' ], k ), **kwargs )
		k = 'deltaCpuTime'
		hc, = plt.plot( v[ 't' ], v[ k ] / denom, label='%s %s' % ( v[ 'type' ], k ), color=hw.get_color(), linestyle='--', **kwargs )
	plt.grid( True )
	plt.legend()
	if 'IPython' not in sys.modules: plt.show()
	return results

if __name__ == '__main__':
	PlotTimingResults( *sys.argv[ 1: ], marker='x' )