#!/usr/bin/env python
""" "
@echo off
cls
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::

set "CUSTOMPYTHONHOME=%PYTHONHOME_DEFAULT%
if "%CUSTOMPYTHONHOME%"=="" goto :skipconfig
set "PYTHONHOME=%CUSTOMPYTHONHOME%
set "PYTHONPATH=%PYTHONHOME%;%PYTHONHOME%\Lib\site-packages
set "PATH=%CUSTOMPYTHONHOME%;%PATH%
:skipconfig

::start pythonw "%0" %*
python "%0" %*

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::pause
goto :eof
" """

__doc__ = """

Defaults::

	NumberOfRows=14 SymbolsPerRow=5 NumberOfDifferentSymbols=4 BaseStrokeWidth=1 ScaleFactor=10**0.1 
	ScreenSize=(1920,1080) ScreenHeightInMm=335 ViewingDistanceInCm=213.2 Interpolate=True SaveAs='chart.png'

Examples::

	chart.cmd ScreenSize=(1920,1080) ScreenHeightInMm=335   BaseStrokeWidth=1   ViewingDistanceInCm=213.2 Interpolate=1 # Lenovo Horizon II or Dell OptiPlex 7760 AIO at 7ft distance
	chart.cmd ScreenSize=(2560,1440) ScreenHeightInMm=335   BaseStrokeWidth=1.5 ViewingDistanceInCm=239.9 Interpolate=1 # Lenovo A7
	chart.cmd ScreenSize=(2160,1440) ScreenHeightInMm=158   BaseStrokeWidth=1   ViewingDistanceInCm=75.4  Interpolate=1 # Surface Pro 3
	chart.cmd ScreenSize=(2160,1440) ScreenHeightInMm=158   BaseStrokeWidth=1   ViewingDistanceInCm=62    Interpolate=1 # Surface Pro 3 (OptokineSys arm's-length standard)
	chart.cmd ScreenSize=(6000,4500) ScreenHeightInMm=190.5 BaseStrokeWidth=3   ViewingDistanceInCm=62    Interpolate=0 OnScreen=0 Display=0 # bedside card to be printed on US Letter at 600dpi
	
	NB: For printed charts: BaseStrokeWidth < 3 results in poor quality of smallest symbols on lab HP1022
	    HP1022 appears to perform at least as well as our previous best effort at FedEx (normal paper, 600 dpi glossy lamination) although note that HP1022 does not print in colour and significantly better than the Toshiba "Molecular 1" printer.
	    Both HP1022 and FedEx seem to shrink the image by about 0.25--0.5% (whereas the Toshiba "Molecular 1" printer does not).
	    HP1022 seems best when set (in the "Printer Features" part of the macOS print dialog) to  {1200 FastRes, REt off}
		(1) 1200 FastRes, REt ON
		(2) 1200 FastRes, REt OFF  <- probably the best setting judging by EyeChartPrinterSettingsComparison-20190719.JPG
		(3) 1200 ProRes,  REt ON
		(4) 1200 ProRes,  REt OFF
		(5) 600 dpi,      REt ON
		(6) 600 dpi,      REt OFF

In the stimulus of Bailey and Lovie (1976, Am. J Optometry & Physiol. Optics) viewed at 6m:
	line  1 (20/200 = LogMAR  1.0): stroke width is 10   arcmin 
	line 11 (20/20  = LogMAR  0  ): stroke width is  1.0 arcmin 
	line 14 (20/10  = LogMAR -0.3): stroke width is  0.5 arcmin 

	LogMAR acuity  =  (LogMAR value of the best line read) + 1.0 - 0.02 x (number of letters read)

	- or equivalently:

	LogMAR acuity  =  (LogMAR value of the best line read) + 0.02 per mistake/failure

"""

import os, sys, random, textwrap
import numpy

import Shady
from Shady.Dependencies import Image
from Shady.Text import LoadFont, RenderTextOntoImage, DeferredDraw

def DisplayFullScreen( numpyArray ):
	w = Shady.World(clearColor=0, threaded=False)
	w.Stimulus( numpyArray )
	Shady.AutoFinish( w )
	
def ResizeImage( imageNumpy, width, height ):
	imagePIL = Image.fromarray( imageNumpy )
	imagePIL = imagePIL.resize( ( width, height ), Image.ANTIALIAS )
	imageNumpy = numpy.array( imagePIL )
	return imageNumpy

def SaveRow( image, sequence ):
	Image.fromarray( row ).save( 'E%02d%s.png' % ( image.shape[ 1 ] / base.shape[ 1 ], sequence.replace( ' ', '' ) ) )
	
class Chart:
	
	def __init__( self, NumberOfRows=14, SymbolsPerRow=5, NumberOfDifferentSymbols=4, BaseStrokeWidth=1, ScaleFactor=10**0.1, ScreenSize=( 1920, 1080 ), ScreenHeightInMm=335, ViewingDistanceInCm=213.2, Interpolate=True, OnScreen=True, SaveAs=None ):

		base = 255 * numpy.array( [
			[ 0, 0, 0, 0, 0 ],
			[ 0, 1, 0, 1, 0 ],
			[ 0, 1, 0, 1, 0 ],
			[ 0, 1, 0, 1, 0 ],
			[ 0, 1, 0, 1, 0 ],
		], dtype=numpy.uint8)
		base.shape = list( base.shape ) + [ 1 ]
		base = numpy.tile( base, [ 1, 1, 3 ] )
		large = base.repeat( 100, axis=0 ).repeat( 100, axis=1 )

		def White( width, height=None ):
			if height is None: width, height = width
			if hasattr( height, 'shape' ): height = height.shape[ 0 ]
			if hasattr(  width, 'shape' ):  width =  width.shape[ 1 ]
			return numpy.zeros( [ max( 0, height ), max( 0, width ), 3 ], dtype=numpy.uint8 ) + 255

		def Pad( image, horizontal=0, vertical=0 ):
			extraLeft = int( ( horizontal - image.shape[ 1 ] ) / 2 )
			extraRight = horizontal - image.shape[ 1 ] - extraLeft
			image = numpy.concatenate( [ White( extraLeft,  image ), image, White( extraRight, image ) ], axis=1 )
			extraTop = int( ( vertical - image.shape[ 0 ] ) / 2 )
			extraBottom = vertical - image.shape[ 0 ] - extraTop
			image = numpy.concatenate( [ White( image, extraTop    ), image, White( image, extraBottom ) ], axis=0 )
			return image, max( 0, extraLeft ), max( 0, extraRight ), max( 0, extraTop ), max( 0, extraBottom )
		
		scrw, scrh = ScreenSize
		mmPerPixel = float( ScreenHeightInMm ) / scrh
		dpi = scrh / ( ScreenHeightInMm / 25.4 )
		strokeWidthsInPixels = BaseStrokeWidth * ScaleFactor ** numpy.arange( NumberOfRows, dtype=float )
		if not Interpolate: strokeWidthsInPixels = numpy.unique( numpy.round( strokeWidthsInPixels ).astype( int ) )
		strokeWidthsInPixels = strokeWidthsInPixels[ ::-1 ]
		
		rowWidth = 0
		accumulatedHeight = 0
		chart = []
		textCommands = []
		lineHeightPixels = numpy.inf
		lineHeightPixels = min( lineHeightPixels, 0.12 * dpi )
		lineHeightPixels = min( lineHeightPixels, scrh / 100.0 )
		lineHeightPixels = min( lineHeightPixels, min( strokeWidthsInPixels ) * 9.0 )
		lineHeightPixels = max( lineHeightPixels, 10.0 )
		font = LoadFont( 'monaco', lineHeightPixels )
		
		print( 'At a viewing distance of %gm:' % ( ViewingDistanceInCm / 100.0 ) )
		for i, strokeWidthInPixels in enumerate( strokeWidthsInPixels ):
			if abs( strokeWidthInPixels - round( strokeWidthInPixels ) ) / float( strokeWidthInPixels ) <= 0.1:
				strokeWidthInPixels = int( round( strokeWidthInPixels ) )
			if strokeWidthInPixels == round( strokeWidthInPixels ):
				scaled = base.repeat( strokeWidthInPixels, axis=0 ).repeat( strokeWidthInPixels, axis=1 )
				interpstring = ' '
			else:
				newWidth  = int( round ( base.shape[ 1 ] * strokeWidthInPixels ) )
				newHeight = int( round ( base.shape[ 0 ] * strokeWidthInPixels ) )
				scaled = ResizeImage( large, newWidth, newHeight )
				strokeWidthInPixels = float( scaled.shape[ 1 ] ) / base.shape[ 1 ]
				interpstring = '*'
			strokeWidthInMm = strokeWidthInPixels * mmPerPixel
			strokeWidthInDegrees = 2.0 * numpy.arctan2( strokeWidthInMm / 2.0, ViewingDistanceInCm * 10.0 ) * 180.0 / numpy.pi
			strokeWidthInMinutes = 60.0 * strokeWidthInDegrees
			
			
			logMAR = numpy.log10( strokeWidthInMinutes )
			twentyWhat = 20 * strokeWidthInMinutes
			#lineNumber = round( 11 - 10 * logMAR )
			lineNumber = i + 1
			print( '    stroke width = %5.2f pixels = %5.2f arcmin   LogMAR = % 6.3f     20/%3g    %s' % ( strokeWidthInPixels, strokeWidthInMinutes, logMAR, twentyWhat, interpstring ) )
			
			E = {}
			image = E[ 'D' ] = scaled
			image = E[ 'U' ] = image[ ::-1, : ]
			image = E[ 'L' ] = image.transpose( ( 1, 0, 2 ) )
			image = E[ 'R' ] = image[ :, ::-1, : ]
			blank = E[ ' ' ] = image * 0 + 255
			symbolHeight, symbolWidth = image.shape[ :2 ]
			sequence = []
			while len( sequence ) < SymbolsPerRow: sequence += random.sample( 'DURL'[ :NumberOfDifferentSymbols ], NumberOfDifferentSymbols )
			sequence = sequence[ :SymbolsPerRow ]
			random.shuffle( sequence )
			sequence = ' ' + ' '.join( sequence ) + ' '
			row = numpy.concatenate( [ numpy.concatenate( [ blank, E[ x ] ], axis=0 ) for x in sequence ], axis=1 )

			# Annotations
			row, left, right, top, bottom = Pad( row, horizontal=scrw )
			spillover = row.shape[ 1 ] - scrw
			rmleft = int( spillover / 2 )
			rmright = spillover - rmleft
			if rmleft  > 0: row = row[ :, rmleft:,  : ]
			if rmright > 0: row = row[ :, rmright:, : ]
			
			row[ -int( symbolHeight / 2.0 ), : int( left * 0.97 ), : ] = 0 # horizontal black line
			txt = "%9d: LogMAR % .3f (20/%d) %s" % ( lineNumber, logMAR, round( twentyWhat ), interpstring )
			
			rowWidth = max( rowWidth, row.shape[ 1 ] )
			accumulatedHeight += row.shape[ 0 ]
			textCommands += RenderTextOntoImage( None, ( 0, accumulatedHeight - bottom - symbolHeight / 2.0 ), txt, font=font, align='left', anchor='left', bg='#FFFFFF', defer=True, fill=( 0, 0, 0 ) )
			chart.append( row )
			
		chart = numpy.concatenate( chart, axis=0 )
		print( '  size = %4d x %4d pixels' % ( rowWidth, chart.shape[ 0 ] ) )
		chart = chart[ :scrh, -scrw:, : ]
		chart, left, right, top, bottom = Pad( chart, vertical=scrh )
		print( 'padded = %4d x %4d pixels' % ( chart.shape[ 1 ], chart.shape[ 0 ] ) )
		DeferredDraw( chart, textCommands, blockbg='#FFFFFF', shift=[ 0, top ] )
		
		
		txt = ''
		if OnScreen: txt += """\
Display in full-screen mode at 100% zoom (pixel-for-pixel).
Set screen resolution to match image size if necessary.
Ensure all red corners are visible.

"""
		txt += """\
Image dimensions:            %4d x %4d pixels
Designed to be rendered at:  %4d x %4d mm    
  and viewed from distance:  %4.2f m         

(Or measure height of border stripe and multiply by %g)

""" % (
			scrw, scrh,
			round( scrw * mmPerPixel ), round( scrh * mmPerPixel ),
			ViewingDistanceInCm / 100.0,
			10.0 * ViewingDistanceInCm / ScreenHeightInMm,
		)

		if Interpolate: antialiasingString = 'Anti-aliased rows are marked with *\nAll others should be perfectly sharp.'
		else:           antialiasingString = 'No anti-aliasing was used in preparing this image, so\nall symbols should be perfectly sharp.'
		if OnScreen:    antialiasingString += """
If not, verify the way your image-viewer software
renders and zooms images. Not all software will
render large or full-screen images faithfully, even
at nominal 100% zoom. For example, on Windows 8.1
as at August 2015, Microsoft "Windows Photo Viewer"
is good in fullscreen mode, but Microsoft "Photos"
(the default image viewer) is not suitable.
"""
		#txt += textwrap.fill( antialiasingString, 57 )
		txt += antialiasingString
		RenderTextOntoImage( chart, [ scrw - right - 20, scrh - 20 ], txt, font=font, anchor='bottom right', align='left', fill=( 0, 0, 0 ) )
		
		chart[  :, 1:30, 0:2 ] = 0 # border stripe
		chart[ [ 0, -1 ],  :40, 0   ] = 255 # red corner horizontal stroke (top and bottom left)
		chart[ [ 0, -1 ],  :40, 1:3 ] = 0
		chart[ [ 0, -1 ], -40:, 0   ] = 255 # red corner horizontal stroke (top and bottom right)
		chart[ [ 0, -1 ], -40:, 1:3 ] = 0
		chart[  :40, [ 0, -1 ], 0   ] = 255 # red corner vertical stroke (top left and right)
		chart[  :40, [ 0, -1 ], 1:3 ] = 0
		chart[ -40:, [ 0, -1 ], 0   ] = 255 # red corner vertical stroke (bottom left and right)
		chart[ -40:, [ 0, -1 ], 1:3 ] = 0
		
		if SaveAs:
			print( 'Saving as ' + SaveAs )
			Image.fromarray( chart ).save( SaveAs, dpi=None if dpi is None else ( dpi, dpi ) )
		self.imageNumpy = chart
	
	
if __name__ == '__main__':
	try:
		cmdline = Shady.CommandLine( dashes=[ 0, 2 ], caseSensitive=False )
		cmdline.Option( 'NumberOfRows',             default=14,             type=int, min=1 )
		cmdline.Option( 'SymbolsPerRow',            default=5,              type=int, min=1 )
		cmdline.Option( 'NumberOfDifferentSymbols', default=4,              type=int, min=1, max=4 )
		cmdline.Option( 'BaseStrokeWidth',          default=1,              type=int, min=1 )
		cmdline.Option( 'ScaleFactor',              default=10**0.1,        type=float, doc='This is the factor by which the linear size of each row should shrink, relative to the line above, as you go down the chart. The default value is 10**0.1, i.e. 0.1 LogMAR units. Note that the value you supply here will be *approximated* as closely as possible given resolution constraints.' )
		cmdline.Option( 'ScreenSize',               default=( 1920, 1080 ), type=( tuple, list ), length=2, doc='Width and height of the chart in pixels.' )
		cmdline.Option( 'ScreenHeightInMm',         default=335,            type=( int, float ), min=1, doc='Measure this with a measuring tape. For charts you intend to print, compute this by dividing the desired height in pixels (the second element of ScreenSize) by the DPI of the printer and multiplying by 25.4 millimeters-per-inch.' )
		cmdline.Option( 'ViewingDistanceInCm',      default=213.2,          type=( int, float ), min=1 )
		cmdline.Option( 'Interpolate',              default=True,           type=bool, doc='Whether or not to allow rows that have to be antialiased (so if this is False, only include rows in which the stroke width is an integer number of pixels).' )
		cmdline.Option( 'OnScreen',                 default=True,           type=bool, doc='Whether or not to render text instructions for displaying this on a computer screen. Set to False if you are intending to print this chart and use the hardcopy for testing.' )
		cmdline.Option( 'SaveAs',                   default='chart.png',    type=( str, None ), doc='Filename for saving the chart. Leave blank if you do not want to save. For best results the filename should end in .png' )
		display = cmdline.Option( 'Display',        default=True,           type=bool, container=None, doc='Whether or not to display the image on screen using Shady (if so, press Q or escape to quit).' )
		cmdline.Help().Finalize()
	except Shady.CommandLine.Error as error:
		sys.stderr.write( '\n%s\n\n' % error )
	else:
		chart = Chart( **cmdline )
		if display: DisplayFullScreen( chart.imageNumpy )
		#if sys.platform.lower().startswith( 'win' ): os.system( 'start %s' % kwargs[ 'SaveAs' ]  )
