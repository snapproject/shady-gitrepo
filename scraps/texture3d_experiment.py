# the point of this was to harness GL_TEXTURE_3D to overcome the MAX_TEXTURE_EXTENT limitation
# in loading multi-frame images.  Hasn't yet succeeded and may never be fully cross-platform (sounds
# like OSX doesn't support 3D textures in legacy OpenGL)

import Shady
Shady.BackEnd('pyglet', False)
w = Shady.World( 1000, threaded=False )

import numpy
if 1:
	a = numpy.load( 'stack.npy' )
else:
	balloon = w.Stimulus( 'scraps/balloon-toobig.gif', name='balloon' )
	a = w.stimuli.balloon.source
	if a.ndim == 2: a = a[ :, :, None ]
	a = numpy.split( a, 60, axis=1 ) # TODO: hardcoded number of frames
	a = numpy.concatenate( [ ai[ :, :, None, : ] for ai in a ], axis=2 )

height, width, nFrames, nChannels = a.shape
a[ :, :, :, 1 ] = 0 # *** this checks that the fourth dimension does indeed correspond to channels, and that the pixels are being successfully transferred, and that they're being read by the shader - all tests passed
frames = list( a.transpose( 2, 0, 1, 3 ).copy() )
try:    data = a.tobytes( order='C' )
except: data = a.tostring( order='C' ) # deprecated
internalFormatGL, formatGL, dtypeGL = Shady.PyEngine.DetermineFormats( nChannels, a.dtype )

# Shady.Rendering.Stimulus needs new ManagedProperties:
#     texture3DSlotNumber = ManagedProperty( -1, transfer='glUniform1i' )
#     numberOfFrames = ManagedProperty( -1, transfer='glUniform1i' )
#     textureFrame = ManagedProperty( 0, transfer='glUniform1i' )
# and then Shady/glsl/FragmentShader.glsl needs the corresponding uniforms:
#     uniform sampler3D uTexture3DSlotNumber;
#     uniform int uNumberOfFrames;
#     uniform int uTextureFrame;
# ...plus some logic for detecting when uNumberOfFrames > 0 and then calling
#		gl_FragColor = Texture_Pixels_XYZ(
#			uTexture3DSlotNumber,
#			vec3( uTextureSize, float( uNumberOfFrames ) ),
#			vec3( textureSourcePosition, float( uTextureFrame )
#		);
# instead of 
#		gl_FragColor = Texture_Pixels_XY( uTextureSlotNumber, uTextureSize, textureSourcePosition );
# in that case (and also skipping debug digits, which assume 2D).  Also, it needs an implementation of:
#	vec4 Texture_Pixels_XYZ( sampler3D texture, vec3 textureSize, vec3 coords ) // [0,0] is bottom left;  [1, 0] is next pixel horizontally
#	{
#		coords.y = textureSize.y - coords.y; // no -1 offset: gl_Vertex will have delivered coordinates of gridlines (0 to textureSize.* INCLUSIVE) and texture2D expects these too
#		return texture3D( texture, coords / textureSize );
#	}

# and this is where I couldn't get it right. The image content was not rendered correctly---mostly it was just a
# uniform field. The colour *did* change as a function of textureFrame, and also correctly as a function of which
# channel was zeroed in the *** manipulation above, indicating that the pixels are transferred, that 

s = w.Stimulus( [ [ 0 ] ], width=width, height=height )

import Shady.GL as GL
s.texture3DSlotNumber = GL.glGenTextures( 1 )
GL.glActiveTexture( GL.GL_TEXTURE0 + s.texture3DSlotNumber )
GL.glBindTexture(   GL.GL_TEXTURE_3D, s.texture3DSlotNumber )
GL.glEnable(        GL.GL_TEXTURE_3D )
GL.glTexImage3D(    GL.GL_TEXTURE_3D, 0, internalFormatGL, width, height, nFrames, 0, formatGL, dtypeGL, data )
GL.glTexParameterf( GL.GL_TEXTURE_3D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR )
GL.glTexParameterf( GL.GL_TEXTURE_3D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR )
#GL.glTexParameterf( GL.GL_TEXTURE_3D, GL.GL_TEXTURE_WRAP_S,     GL.GL_REPEAT )
#GL.glTexParameterf( GL.GL_TEXTURE_3D, GL.GL_TEXTURE_WRAP_T,     GL.GL_REPEAT )
#GL.glBindTexture(   GL.GL_TEXTURE_3D, 0 )

s.Set( numberOfFrames=nFrames, textureFrame=Shady.Integral(20) )
w.Run()
#balloon = w.Stimulus( 'https://i.pinimg.com/originals/fb/59/f9/fb59f953a0e9135353aa1ca3c717b2e2.gif' )
#rocket = w.Stimulus( 'http://countdown101.com/wp-content/themes/Starkers/images/rocket-blast_off.gif' )
