
import struct
def GrokNestedList( array ):
	# Returns the same args as Shady.Rendering.PrepareTextureData, but for (nested-)list data without requiring numpy.
	# However (TODO): it isn't just a drop-in replacement and would require some careful plumbing,
	# because all methods that call PrepareTextureData also do some numpy-dependent preprocessing first
	nDimensions = 0
	try: nRows = len( array )
	except: nRows = 1
	else: nDimensions = 1
	try: nColumns = len( array[ 0 ] )
	except: nColumns = 1 if nRows else 0
	else: nDimensions = 2
	try: nChannels = len( array[ 0 ][ 0 ] )
	except: nChannels = 1
	else: nDimensions = 3
		
	flattenedArray = []
	if nDimensions == 1: flattenedArray = array
	if nDimensions == 2: flattenedArray = [ pixel for row in array for pixel in row ]
	if nDimensions == 3: flattenedArray = [ channel for row in array for pixel in row for channel in pixel ]
	# TODO: if you set it up to expect multi-channel by making a[0][0] a sequence, and then subsequent pixels are *not* sequences, you get a crash here
	#       (whereas if you set it up to expect *less* dimensionality than subsequent pixels, you get the saner "invalid pixel values" error)
	
	for value in flattenedArray:
		if not isinstance( value, ( int, float ) ):
			raise ValueError( 'invalid pixel values' )
	for value in flattenedArray:
		if not isinstance( value, int ) or value < 0:
			dataType = 'float32'
			break
	else:
		dataType = 'uint8'
		if flattenedArray:
			maxValue = max( flattenedArray )
			if maxValue > 65535: dataType = 'uint32'
			elif maxValue > 255: dataType = 'uint16'
	dataTypes = dict( float32='f', uint32='I', uint16='H', uint8='B' )
	if struct.calcsize( 'L' ) == 4: dataTypes[ 'uint32' ] = 'L'
	fmt = str( len( flattenedArray ) ) + dataTypes[ dataType ]
	return ( nColumns, nRows, nChannels, dataType, struct.pack( fmt, *flattenedArray ) )
	