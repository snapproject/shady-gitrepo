
if 1:
	
	import os, sys, time
	
	vertexShaderSource = """\

//---------------------------------------------------------------------------
// Vertex
//---------------------------------------------------------------------------
#version 420 core
//---------------------------------------------------------------------------
layout(location=0) in vec4 vertex;
out vec2 pos;   // screen position <-1,+1>
void main()
    {
    pos=vertex.xy;
    gl_Position=vertex;
    }
//---------------------------------------------------------------------------

	"""
	fragmentShaderSource = """\

//---------------------------------------------------------------------------
// Fragment
//---------------------------------------------------------------------------
#version 420 core
//---------------------------------------------------------------------------
in vec2 pos;                    // screen position <-1,+1>
out vec4 gl_FragColor;          // fragment output color
uniform sampler2D txr_font;     // ASCII 32x8 characters font texture unit
uniform float fxs,fys;          // font/screen resolution ratio
//---------------------------------------------------------------------------
const int _txtsiz=256;           // text buffer size
int txt[_txtsiz],txtsiz;        // text buffer and its actual size
vec4 col;                       // color interface for txt_print()
//---------------------------------------------------------------------------
void txt_decimal( float x )       // print float x into txt
{
	int i, j, c;          // l is size of string
	float y, a;
	const float base = 10.0;
	
	// divide to int(x).fract(y) parts of number
	y = x; // TODO: y = fract( x );
	x = floor( x );
	y -= x; // TODO: remove
	// handle integer part
	i = txtsiz;                   // start of integer part
	for( ; txtsiz < _txtsiz ; )
	{
		a = x;
		x = floor( x / base );
		a -= base * x;
		txt[ txtsiz++ ] = int( a ) + 48;
		if( x <= 0.0 ) break;
	}
	j = txtsiz - 1;                 // end of integer part
	for( ; i < j; i++, j-- )      // reverse integer digits
	{
		c = txt[ i ];
		txt[ i ] = txt[ j ];
		txt[ j ] = c;
	}
	// handle fractional part
	for( txt[ txtsiz ] = 46, txtsiz++; txtsiz < _txtsiz; )
	{
		y *= base;
		a = floor( y );
		y -= a;
		txt[ txtsiz++ ] = int( a ) + 48;
		if( y <= 0.0 ) break;
	}
	txt[ txtsiz ] = 0;  // string terminator
}
//---------------------------------------------------------------------------
void txt_print(float x0,float y0)   // print txt at x0,y0 [chars]
    {
    int i;
    float x,y;
    // fragment position [chars] relative to x0,y0
    x=0.5*(1.0+pos.x)/fxs; x-=x0;
    y=0.5*(1.0-pos.y)/fys; y-=y0;
    // inside bbox?
    if ((x<0.0)||(x>float(txtsiz))||(y<0.0)||(y>1.0)) return;
    // get font texture position for target ASCII
    i=int(x);               // char index in txt
    x-=float(i);
    i=txt[i];
    x+=float(int(i&31));
    y+=float(int(i>>5));
    x/=32.0; y/=8.0;    // offset in char texture
    col=texture2D(txr_font,vec2(x,y));
    }
//---------------------------------------------------------------------------
void main()
    {
    col=vec4(0.0,1.0,0.0,1.0);  // background color
    txtsiz=0;
    txt_decimal(12.345); txt[txtsiz++]= 32; txt[txtsiz++]= 32;
    txt_decimal(1.0); txt[txtsiz++]= 32; txt[txtsiz++]= 32;
    txt_decimal(2.5); txt[txtsiz++]= 32; txt[txtsiz++]= 32;
    txt_decimal(2.6); txt[txtsiz++]= 32; txt[txtsiz++]= 32;
    txt_print(1.0,0.1);
    gl_FragColor=col;
    }
//---------------------------------------------------------------------------
	"""

	# Python (PyOpenGL) code to demonstrate the above
	# (Note: the same OpenGL calls could be made from any language)


	import OpenGL
	from OpenGL.GL import *
	from OpenGL.GLU import *

	import pygame, pygame.locals # just for getting a canvas to draw on

	try: from PIL import Image  # PIL.Image module for loading image from disk
	except ImportError: import Image  # old PIL didn't package its submodules on the path

	import numpy # for manipulating pixel values on the Python side

	def CompileShader( type, source ):
		shader = glCreateShader( type )
		glShaderSource( shader, source )
		glCompileShader( shader )
		result = glGetShaderiv( shader, GL_COMPILE_STATUS )
		if result != 1:
			raise Exception( "Shader compilation failed:\n" + glGetShaderInfoLog( shader ) )
		return shader

	class World:
		def __init__( self, width, height ):

			self.window = pygame.display.set_mode( ( width, height ), pygame.OPENGL | pygame.DOUBLEBUF )

			# compile shaders
			vertexShader = CompileShader( GL_VERTEX_SHADER, vertexShaderSource )
			fragmentShader = CompileShader( GL_FRAGMENT_SHADER, fragmentShaderSource )
			# build shader program
			self.program = glCreateProgram()
			glAttachShader( self.program, vertexShader )
			glAttachShader( self.program, fragmentShader )
			glLinkProgram( self.program )
			# try to activate/enable shader program, handling errors wisely
			try:
				glUseProgram( self.program )
			except OpenGL.error.GLError:
				print( glGetProgramInfoLog( self.program ) )
				raise

			# enable alpha blending
			glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE )
			glEnable( GL_DEPTH_TEST )
			glEnable( GL_BLEND )
			glBlendEquation( GL_FUNC_ADD )
			glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA )

			# set projection and background color
			gluOrtho2D( 0, width, 0, height )
			glClearColor( 0.0, 0.0, 0.0, 1.0 )
			
			self.uTextureSlotNumber_addr = glGetUniformLocation( self.program, 'uTextureSlotNumber' )
			self.uTextureSize_addr = glGetUniformLocation( self.program, 'uTextureSize' )

		def RenderFrame( self, *textures ):
			glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT )
			for t in textures: t.Draw( world=self )
			pygame.display.flip()
			
		def Close( self ):
			pygame.display.quit()
			
		def Capture( self ):
			w, h = self.window.get_size()
			rawRGB = glReadPixels( 0, 0, w, h, GL_RGB, GL_UNSIGNED_BYTE )
			return Image.frombuffer( 'RGB', ( w, h ), rawRGB, 'raw', 'RGB', 0, 1 ).transpose( Image.FLIP_TOP_BOTTOM )
		
	class Texture:
		def __init__( self, source, slot=0, position=(0,0,0) ):
			
			# wrangle array
			source = numpy.array( source )
			if source.dtype.type not in [ numpy.float32, numpy.float64 ]: source = source.astype( float ) / 255.0
			while source.ndim < 3: source = numpy.expand_dims( source, -1 )
			if source.shape[ 2 ] == 1: source = source[ :, :, [ 0, 0, 0 ] ]    # LUMINANCE -> RGB
			if source.shape[ 2 ] == 2: source = source[ :, :, [ 0, 0, 0, 1 ] ] # LUMINANCE_ALPHA -> RGBA
			if source.shape[ 2 ] == 3: source = source[ :, :, [ 0, 1, 2, 2 ] ]; source[ :, :, 3 ] = 1.0  # RGB -> RGBA
			# now it can be transferred as GL_RGBA and GL_FLOAT
			
			# housekeeping
			self.textureSize = [ source.shape[ 1 ], source.shape[ 0 ] ]
			self.textureSlotNumber = slot
			self.textureSlotCode = getattr( OpenGL.GL, 'GL_TEXTURE%d' % slot )
			self.listNumber = slot + 1
			self.position = list( position )
			
			# transfer texture content
			glActiveTexture( self.textureSlotCode )
			self.textureID = glGenTextures( 1 )
			glBindTexture( GL_TEXTURE_2D, self.textureID )
			glEnable( GL_TEXTURE_2D )
			glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA32F, self.textureSize[ 0 ], self.textureSize[ 1 ], 0, GL_RGBA, GL_FLOAT, source )
			glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST )
			glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST )

			# define surface
			w, h = self.textureSize
			glNewList( self.listNumber, GL_COMPILE )
			glBegin( GL_QUADS )
			glColor3f( 1, 1, 1 )
			glNormal3f( -1, -1, 1 )
			glVertex3f( -1, +1, 0 )
			glVertex3f( +1, +1, 0 )
			glVertex3f( +1, -1, 0 )
			glVertex3f( -1, -1, 0 )
			glEnd()
			glEndList()
			
		def Draw( self, world ):
			glPushMatrix()
			glTranslate( *self.position )
			#glUniform1i( world.uTextureSlotNumber_addr, self.textureSlotNumber )
			#glUniform2f( world.uTextureSize_addr, *self.textureSize )
			glUniform1i(glGetUniformLocation(world.program,"txr_font"),0);
			glUniform1f(glGetUniformLocation(world.program,"fxs"),(8.0)/float(world.window.get_width()))
			glUniform1f(glGetUniformLocation(world.program,"fys"),(8.0)/float(world.window.get_height()))
			
			glCallList( self.listNumber )
			glPopMatrix()
			

	world = World( 1000, 800 )
	digits = Texture( Image.open( 'Spektre.png' ) )
	done = False
	while not done:
		world.RenderFrame( digits )
		for event in pygame.event.get():
			# Press 'q' to quit or 's' to save a timestamped snapshot
			if event.type  == pygame.locals.QUIT: done = True
			elif event.type == pygame.locals.KEYUP and event.key in [ ord( 'q' ), 27 ]: done = True
			elif event.type == pygame.locals.KEYUP and event.key in [ ord( 's' ) ]:
				world.Capture().save( time.strftime( 'snapshot-%Y%m%d-%H%M%S.png' ) )
	world.Close()
