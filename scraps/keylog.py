import os
import sys
import ast
from collections import defaultdict

import pythoncom   # from pypiwin32 package:  python -m pip install pypiwin32
import pyHook      #  used `python -m pip install BLAH.whl` to install version-appropriate .whl file downloaded from https://www.lfd.uci.edu/~gohlke/pythonlibs/#pyhook
                   # (it may look like you can replace it with pyxhook but you cannot: pyxhook is not Windows-compatible, and crashes when it fails to find fcntl module)

import win32api, win32con  # from pywin32 package (pip installed this automatically as dependency of pypiwin32)
def CapsLockIsOn(): return win32api.GetKeyState( win32con.VK_CAPITAL )

class workaround( dict ):
	UNSPECIFIED = object
	def get( self, key, defaultValue=UNSPECIFIED ):
		key = int( str( key ) ) # this is the workaround for semi-inexplicable "TypeError: an integer is required" bug in pyHook
		if defaultValue is workaround.UNSPECIFIED: return dict.get( self, key )
		else: return dict.get( self, key, defaultValue )

PYHOOK_TO_SENDKEY = dict(
	CAPITAL='{CAPSLOCK}',
	BACK='{BACKSPACE}',
	DELETE='{DELETE}',
	RETURN='{ENTER}',
	ENTER='{ENTER}',
	LEFT='{LEFT}',
	UP='{UP}',
	DOWN='{DOWN}',
	RIGHT='{RIGHT}',
	SNAPSHOT='{PRTSC}',
	HOME='{HOME}',
	END='{END}',
	PRIOR='{PGUP}',
	NEXT='{PGDN}',
	TAB='{TAB}',
	ESCAPE='{ESCAPE}',
	INSERT='{INSERT}',
	SPACE=' ',                 # TODO: unverified that this is the correct SendKeys code
	BREAK='{BREAK}',           # TODO: unverified that this is what pyHook calls it
	NUMLOCK='{NUMLOCK}',       # TODO: unverified that this is what pyHook calls it
	SCROLLLOCK='{SCROLLLOCK}', # TODO: unverified that this is what pyHook calls it
)
for i in range( 1, 13 ): PYHOOK_TO_SENDKEY[ 'F%d' % i ] = '{F%d}' % i

SCANCODE_TO_SENDKEYS = {
# This covers the problematic cases---mostly punctuation, for which pyHook seems to deliver the
# Virtual .KeyID of the shift-modified character even when shift is not held down. So, we'll work
# from the .ScanCode instead, using codes from the "X(Set 3)" column of the table in section 10.6
# of https://www.win.tue.nl/~aeb/linux/kbd/scancodes-10.html  (this being the ScanCode "set" that 
# he Surface Pro 3 keyboard seems to use at least, but it's unclear to me how you work out the
# correct set for an arbitrary unknown keyboard)
	0x29 : '`~'[0],
	0x0c : '-_'[0],
	0x0d : '=+'[0],
	0x1a : '[{'[0],
	0x1b : ']}'[0],
	0x2b : '\\|'[0],
	0x75 : '\\|'[0],
	0x27 : ';:'[0],
	0x28 : '\'\"'[0],
	0x33 : ',<'[0],
	0x34 : '.>'[0],
	0x35 : '/?'[0],
	0x37 : '{ADD}',
	0x54 : '{SUBTRACT}',
	0x45 : '{DIVIDE}',
	0x46 : '{MULTIPLY}',
}

class KeyLogger( object ):
	def __init__( self, filename='', verbose=False ):
		try: verbose = ast.literal_eval( verbose )
		except: pass
		self.hm = pyHook.HookManager()
		self.hm.keyboard_funcs = workaround()
		self.hm.KeyDown = self.HandleEvent
		self.hm.KeyUp = self.HandleEvent
		self.hm.HookKeyboard()
		self.modifiers = defaultdict( bool )
		self.verbose = verbose
		self.t0 = None
		self.events = []
		if filename:
			self.filename = os.path.realpath( filename )
			self.fh = open( self.filename, 'wt' )
		else:
			self.filename = None
			self.fh = sys.stdout
		
		self.fh.write( """\
from SendKeys import SendKeys
import time
def WaitUntil( t ):
	while time.time() < START_TIME + t:
		time.sleep( 0.001 )
START_TIME = time.time()

""" )
	
	def __del__ ( self ):
		if self.fh not in [ sys.stdout, sys.stderr ]:
			try: self.fh.close()
			except: pass
	
	def Go( self ):
		self.t0 = None
		self.keep_going = True
		while self.keep_going: pythoncom.PumpWaitingMessages()
		return self
			
	def Stop( self ):
		self.keep_going = False
		
	def HandleEvent( self, event ):
		"""

		flags       int  (encoded info that is unpacked by the following properties:)
		
		Alt         int (property, calls .IsAlt() method, is non-zero if alt key was held down)
		Extended    int (property, calls .IsExtended() method, is non-zero if this is an "extended key" whatever that means )
		Injected    int (property, calls .IsInjected() method, is non-zero if event was programmatically generated)
		Transition  int (property, calls .IsTransition() method, is non-zero if event "is a transition from up to down or vice versa" whatever that means)
		
		Ascii       int 
		Key         str (property, calls .GetKey() method which calls an internal translation function on .KeyID attribute to obtain the key name)
		KeyID       int (key code - for letter keys, seems to correspond to ASCII code of upper-case version of letter)
		Message     int (or some funny subclass thereof, necessitating the workaround above?)
		MessageName str (property, calls .GetMessageName() method to return name of message type, e.g. 'key down' or 'key sys down')
		ScanCode    int 
		Time        int (not sure when epoch is)
		Window      int (handle)
		WindowName  str (title of window)
		"""
		#self.events.append( event )
		if self.t0 is None: self.t0 = event.Time
		if self.verbose:
			for attributeName in 'MessageName Key ScanCode Ascii KeyID'.split():
				print( 'event.%s = %r' % ( attributeName, getattr( event, attributeName ) ) )
			try: c = chr( event.Ascii )
			except: print( 'chr( event.Ascii ) failed' )
			else: print( 'chr( event.Ascii ) = %r' % c )
			try: c = chr( event.KeyID )
			except: print( 'chr( event.KeyID ) failed' )
			else: print( 'chr( event.KeyID ) = %r' % c )
			try: c = chr( event.ScanCode )
			except: print( 'chr( event.ScanCode ) failed' )
			else: print( 'chr( event.ScanCode ) = %r' % c )
			print( '' )
		
		encoded = event.Key
		upper = encoded.upper()
		msg = event.MessageName.upper()
		isModifier = upper in [ 'LMENU', 'RMENU', 'LSHIFT', 'RSHIFT', 'LCONTROL', 'RCONTROL', 'LWIN', 'RWIN' ]
		if isModifier:
			if msg in [ 'KEY DOWN', 'KEY SYS DOWN' ]: self.modifiers[ upper ] = self.modifiers[ upper[ 1: ] ] = True
			if msg in [ 'KEY UP',   'KEY SYS UP'   ]: self.modifiers[ upper ] = self.modifiers[ upper[ 1: ] ] = False
		elif msg == 'KEY DOWN':
			if upper.startswith( 'OEM_'): encoded = SCANCODE_TO_SENDKEYS.get( event.ScanCode )
			else: encoded = PYHOOK_TO_SENDKEY.get( upper, encoded )
			escaped = encoded and encoded.startswith( '{' )
			valid = escaped or ( encoded and len( encoded ) == 1 )
			if valid:
				if encoded in [ '+', '^', '%', '~', '{', '}', '(', ')' ]: encoded = '{' + encoded + '}'
				shift   = self.modifiers[ 'SHIFT' ]
				control = self.modifiers[ 'CONTROL' ]
				encoded_upper = encoded.upper()
				encoded_lower = encoded.lower()
				if encoded_upper != encoded_lower and not escaped:
					encoded = encoded_upper if CapsLockIsOn() ^ shift else encoded_lower
				elif shift:   encoded = '+' + encoded
				if control:   encoded = '^' + encoded
				if event.Alt: encoded = '%' + encoded
				self.Log( event.Time, encoded )
			else:
				print( '# Unknown( %s )' % ', '.join( '%s=%r' % ( key, getattr( event, key ) ) for key in 'MessageName Key KeyID ScanCode Ascii'.split() ) )
		if event.KeyID == 27: self.Stop()
		
	def Log( self, t, encoded ):
		if self.t0 is None: self.t0 = t
		t = ( t - self.t0 ) / 1000.0
		self.fh.write( 'WaitUntil( %7.3f ); ' % t )
		self.fh.write( 'SendKeys( %r )\n' % encoded )
if __name__ == '__main__':
	k = KeyLogger( *sys.argv[ 1: ] )
	k.Go()
