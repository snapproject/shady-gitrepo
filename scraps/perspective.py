import sys
import ast
import operator

import numpy
import sympy
from numpy import matrix as matrix, pi, sin, cos, tan; cot = lambda x: 1.0 / tan(x)


def ss( *args ):
	args = [ term for arg in args for term in ( arg.split( ',' ) if isinstance( arg, str ) else [ arg ] ) ]
	for i, arg in enumerate( args ):
		try:
			args[ i ] = ast.literal_eval( arg )
		except:
			if isinstance( arg, str ):
				args[ i ] = sympy.symbols( arg, real=True )
	return args

def mm( *terms ):
	result = None
	op = operator.mul
	for term in terms:
		if isinstance( term, str ):
			if term == '+': op = operator.add; continue
			if term == '-': op = operator.sub; continue
			if term == '*': op = operator.mul; continue
		if isinstance( term, tuple ):
			term = mm( *term )
		if isinstance( term, list ):
			term = [ float( element ) if isinstance( element, int ) else element for element in term ]
			if len( term ) == 1: term.append( term[ 0 ] )
			if len( term ) == 2: term.append( 0.0 )
			if len( term ) == 3: term.append( 1.0 )
			term = matrix( term )
			if isinstance( term, numpy.matrix ): term = term.astype( float ).T
		if result is None:
			result = term
		else:
			result = op(result, term)
			if hasattr( result, 'simplify' ): result.simplify()
		op = operator.mul
	if list( result.shape ) == [ 4, 1 ]: result = nohomo( result )
	return result
numpy.matrix.times = mm
sympy.Matrix.times = mm

def invert( m ):
	m = m.inv()
	m.simplify()
	return m
sympy.Matrix.I = property( invert )


def RotationMatrix( deg, axis='y' ):
	theta = deg * pi / 180.0
	c, s = cos( theta ), sin( theta )
	if axis == 'x': return matrix( [ [ 1, 0, 0, 0 ], [ 0, c, -s, 0 ], [ 0, s, c, 0 ], [ 0, 0, 0, 1 ] ] )
	if axis == 'y': return matrix( [ [ c, 0, s, 0 ], [ 0, 1, 0, 0 ], [ -s, 0, c, 0 ], [ 0, 0, 0, 1 ] ] )
	if axis == 'z': return matrix( [ [ c, -s, 0, 0 ], [ s, c, 0, 0 ], [ 0, 0, 1, 0 ], [ 0, 0, 0, 1 ] ] )
	
def TranslationMatrix( xshift, yshift, zshift ):
	return matrix( [ [ 1.0, 0, 0, xshift ], [ 0, 1.0, 0, yshift ], [ 0, 0, 1.0, zshift ], [ 0, 0, 0, 1.0 ] ] )

def Frustrum():
	names = 'l,r,b,t,n,f'
	return dict( zip( names.split( ',' ), ss( names ) ) )

def ProjectionToNDC( l, r, b, t, n, f ):
	n2 = 2.0 * n
	w = 1.0 * ( r - l )
	h = 1.0 * ( t - b )
	d = 1.0 * ( f - n )
	m = matrix( [
		[ n2/w,   0,  -(r+l)/w,      0   ],
		[   0,  n2/h, -(t+b)/h,      0   ],
		[   0,    0,   (f+n)/d, -f*n2/d  ],
		[   0,    0,        1,       0   ],
	] )
	return m
	
def Proj2( c, w, h, d ):
	mid = 0.5 * w * c
	f = mid + d / 2
	n = mid - d / 2
	m = matrix( [
		[ c,   0,     0,        0    ],
		[ 0, c*w/h,   0,        0    ],
		[ 0,   0,   c*w/d,  -2*f*n/d ],
		[ 0,   0,     1,        0    ],
	] )
	m.simplify()
	return m

def Normalizer( w, h=None, d=None ):
	if h is None:
		try: w, h = w
		except: h = w
	if d is None:
		d = w
	return matrix( [
		[ 2.0 / w,     0,       0,   -1.0 ],
		[     0,   2.0 / h,     0,   -1.0 ],
		[     0,       0,   2.0 / d,  0   ],
		[     0,       0,       0,    1.0 ],
	] )

def nohomo( v ):
	v = v / v[ 3 ]
	if isinstance( v, sympy.Matrix ): v.simplify()
	return v[ :3, : ]
	
if __name__ == '__main__':
	from sympy import Matrix as matrix, pi, sin, cos, tan, cot
	sympy.var( 'l,r,b,t,n,f' )
	sympy.var( 'w,h,d' )
	sympy.var( 'ax,ay' )
	sympy.var( 'alpha' )
	l = -r; b = -t # symmetric frustrum: vanishing point level with the camera
	nrm = Normalizer(w,h,d)	
	c = cot( alpha / 2 )  # alpha is the *horizontal* camera angle
	q = 0.5 * w * c  # this is the distance from the camera to the "zero" depth-plane, where (-w/2,-h/2) and (+w/2,+h/2) map to (-1,-1) and (+1,+1)
	# NB:  frustrum depth `d` must be strictly less than `2*q` !!
		
	anchorShiftX, anchorShiftY = ax, ay
	anchorShiftX, anchorShiftY = 0.5 * w * ( ax + 1.0 ), 0.5 * h * ( ay + 1.0 ) # for as-yet-unexplained reasons this produces (ax+2.0) and (ay+2.0) terms in the translation elements, whereas (ax+1.0) and (ay+1.0) produce the correct results
	anchorShiftX, anchorShiftY = 0, 0
	
	p = mm( nrm.I, Proj2( c, w, h, d ), TranslationMatrix( anchorShiftX, anchorShiftY, q ) )  # ProjectionToNDC and Proj2 assume the camera is at 0,0,0,  so we need the to translate the world away by z=q first
	# (matrixWorldNormalizer * p) is the full geometric transformation that the vertex shader applies
	# But p alone gets us to a coordinate frame of pixels-from-bottom-left and this is useful in pixel-for-pixel rounding
	# All diagonal elements of p are equal to q
	
	# mm( nrm, p, [-w/2, -h/2, 0] ) #--> [-1,-1, 0]
	# mm( nrm, p, [+w/2, +h/2, 0] ) #--> [+1,+1, 0]
	# mm( nrm, p, [0, 0, -d/2] )    #--> [ 0, 0,-1]
	# mm( nrm, p, [0, 0, +d/2] )    #--> [ 0, 0,+1]

	print(repr(p)) 

	
"""
# We assume that the World property `perspectiveCameraAngle` and the Stimulus property `matrixEnvelopeTricks`
# have been uncommented from `Rendering.py` and `ShaDyLib.cpp`,  and also that the two lines referring to
# `uMatrixEnvelopeTricks` have been uncommented in `VertexShader.glsl`:

w = Shady.World( perspectiveCameraAngle=90 )
Shady.PixelRuler( w.MakeCanvas( z=0 ) )
s = w.Stimulus( Shady.EXAMPLE_MEDIA.waves, scaling=0.2, z=-500 )
from perspective import RotationMatrix
s.matrixEnvelopeTricks = lambda t: RotationMatrix( t * 20 )
"""