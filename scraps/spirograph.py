import numpy
import Shady

class SpirographController( object ):

	def __init__( self, speed=200, steeringMean=12, deviationFactor=8, order=4.5, shiftX=0, shiftY=0 ):
		self.speed = speed # pixels per second
		self.steeringMean = steeringMean  # in degrees;  with all other parameters constant, this (inversely) governs overall radius
		self.deviationFactor = deviationFactor # tightness of loops
		self.order = order  # number of lobes
		self.shiftX, self.shiftY = shiftX, shiftY
	
		self.deltaTheta = Shady.Oscillator( lambda t: self.order * self.steeringMean / 360 ) * ( lambda t: self.deviationFactor * self.steeringMean ) + ( lambda t: self.steeringMean )
		f = Shady.Integral( self.deltaTheta  )
		tap = f.Tap()
		f *= 1j * numpy.pi / 180.0
		f.Transform( numpy.exp )
		f *= lambda t: self.speed
		f = Shady.Integral( f ).Transform( Shady.ComplexToReal2D )
		f += lambda t: [ self.shiftX, self.shiftY ]
		self.xy = f
		self.rotation = lambda t=None: tap()
		
	def __repr__( self ):
		return '{cls}({args})'.format( cls=self.__class__.__name__, args=', '.join( '%s=%r' % ( k, getattr( self, k ) ) for k in 'speed steeringMean deviationFactor order shiftX shiftY'.split() ) )
		
	def AttachToStimulus( self, stim, faceForward=True ):
		stim.xy = self.xy
		if faceForward: stim.rotation = self.rotation
		stim.controller = self
		w = stim.world()
		class FakeTrail: locations = 1j
		@w.EventHandler( slot=-10 )
		def Handle( world, event ):
			trail = getattr( stim, 'trail', FakeTrail() )
			delta = 10 if 'ctrl' in event.modifiers else 250 if 'shift' in event.modifiers else 50
			if event >> "kp[left]  ka[left] ": self.shiftX -= delta; trail.locations -= delta
			if event >> "kp[right] ka[right]": self.shiftX += delta; trail.locations += delta
			if event >> "kp[down]  ka[down] ": self.shiftY -= delta; trail.locations -= delta * 1j
			if event >> "kp[up]    ka[up]   ": self.shiftY += delta; trail.locations += delta * 1j
			if event >> "ms":
				factor = -10
				self.shiftX += factor * event.dx
				self.shiftY += factor * event.dy
				trail.locations += factor * ( event.dx + event.dy * 1j )
		return self
	
def MakeTrail( stim, brushShape=3, brushSize=5, **kwargs ):
	w = stim.world()
	trail = w.Stimulus( drawMode=Shady.DRAWMODE.POLYGON, color=1, alpha=0.5, size=0 ).Set( **kwargs )
	trail.locations = numpy.zeros( [ 20000, 1 ], dtype='complex' )
	trail.Erase = lambda: trail.locations.fill( numpy.nan )
	trail.Erase()
	trail.brush = Shady.ComplexPolygonBase( brushShape ) if brushShape else None
	trail.brushSize = brushSize
	@trail.AnimationCallback
	def MakeTracks( self, t ):
		self.locations[ 1: ] = self.locations[ :-1 ]
		self.locations[ 0 ] = stim.x + 1j * stim.y
		if self.brush is None:
			self.drawMode = Shady.DRAWMODE.LINE_STRIP
			self.points = self.locations
		else:
			self.drawMode = Shady.DRAWMODE.POLYGON
			self.points = self.brushSize * self.brush + self.locations[ :20000 // self.brush.size ]
	@w.EventHandler( slot=-20 )
	def Handle( world, event ):
		if event >> "kp[l]": trail.visible = not trail.visible
		if event >> "kp[e]": trail.Erase()
	stim.trail = trail
	return trail

def	Compute( stim ):
	import Shady.Text
	v = Shady.Derivative( lambda t: stim.x + stim.y * 1j )
	v.Transform( lambda xy: numpy.angle( xy ) * 180.0 / numpy.pi )
	v = Shady.Derivative( v )
	v %= 360
	v.Transform( lambda d: min( d, 360 - d ) )
	#v = Shady.Derivative( v ).Transform( abs )
	v = Shady.Integral( v ) / Shady.Integral().Transform( lambda x: x if x else 1.0 )
	if 'report' not in w.stimuli:
		w.Stimulus( name='report', text='', anchor=Shady.LEFT, position=lambda t: w.Place( Shady.LEFT ) )	
	w.stimuli.report.text = v
	


cmdline = Shady.WorldConstructorCommandLine( fullScreenMode=False )
w = Shady.World( **cmdline )
s = w.Stimulus( pp=-1, bgalpha=0, size=35, color=1 )
MakeTrail( s )
c = SpirographController().AttachToStimulus( s )

Shady.AutoFinish( w )
