#from Shady import Seconds as WallTime

import sys
import time

# All this stuff requires Python 3.3+
from time import perf_counter as WallTimeCore
from time import process_time as CPUTimeCore
RESOLUTIONS = tuple( [ time.get_clock_info( func.__name__ ).resolution for func in [ WallTimeCore, CPUTimeCore ] ] )
RESOLUTION = max( RESOLUTIONS )

def WallTime(): return RESOLUTION * round( WallTimeCore() / RESOLUTION )
def CPUTime():  return RESOLUTION * round( CPUTimeCore()  / RESOLUTION )
if __name__ == '__main__':
	tests = 0
	primes = []
	times = []
	batch = 1000000
	out = sys.stdout
	if len( sys.argv ) > 1: out = open( sys.argv[ 1 ], 'wt' )
	out.write( "py = d = {}\n" )
	out.write( "d[ 'batch_size' ] = %r\n" % ( batch, ) )
	out.write( "d[ 'ticks_per_sec' ] = %r\n" % ( RESOLUTIONS, ) )
	out.write( "d[ 'times' ] = t = []; t = t.append\n" )
	#t = int( time.time() )
	#while int( time.time() ) == t: time.sleep( 0.001 )
	while True:
		while True:	
			primes.clear()
			times.clear()
			candidate = 1
			previousCPUTime = CPUTime()
			previousWallTime = WallTime()
			while True:
				candidate += 1
				isPrime = True;
				divisor = 2
				for divisor in primes:
					if divisor * divisor > candidate: break
					tests += 1
					if ( tests % batch ) == 0:
						currentCPUTime = CPUTime()
						currentWallTime = WallTime()
						elapsedWallTime = currentWallTime - previousWallTime
						elapsedCPUTime = currentCPUTime - previousCPUTime
						times.append( ( elapsedWallTime, elapsedCPUTime ) )
						out.write( 't(( %.3f, %.5f, %.5f ))     #   % 11.6f ms = %g%%\n' % ( time.time(), elapsedWallTime, elapsedCPUTime, 1000.0 * ( elapsedWallTime - elapsedCPUTime ), 200.0 * ( elapsedWallTime - elapsedCPUTime ) / ( elapsedWallTime + elapsedCPUTime ) ) )
						#out.write( '%d\n' % len( primes ) )
						out.flush()
						tests = 0
						previousWallTime = WallTime()
						previousCPUTime  = CPUTime()
						#time.sleep( 0.3 )
					factor = candidate // divisor;
					if factor * divisor == candidate: isPrime = False; break
					divisor += 1
				if isPrime:
					primes.append( candidate )
					if len( primes ) > 500000: break
