# fails on Windows with pyglet.gl.ContextException: Require WGL_ARB_create_context extension to create OpenGL 3 contexts.
# However, you can still use 3.3+ features without specifically requesting a 3.3 context, if the graphics driver supports it.

# Tests on OSX (Late 2013 13" Retina MacBook, previously running 10.11 El Capitan and more recently running 10.13 High Sierra):
# Note that we need 3.3+ for floatBitsToUint and friends.
# 3.2 and 4.1 can be successfully requested, and the result is a 4.1 context in either case.
# All other version numbers before, after and in-between these two fail silently, and lead to creation of OpenGL 2.1 context.
# Even when it succeeds, we need to disable the "shadow window" otherwise we get an exception from pyglet.gl.cocoa

target = 4.1

import os; os.environ['PYGLET_SHADOW_WINDOW'] = '0'   # this is one way to disable the shadow window, before importing pyglet
import pyglet, ctypes
#pyglet.options['shadow_window'] = False   # this is the other way to do it, immediately after importing pyglet (NB: does that always work?)
config = pyglet.gl.Config( major_version=int( target ), minor_version=int( round( ( target % 1.0 ) * 10 ) ), forward_compatible=True )
print( config )
w = pyglet.window.Window( config=config, visible=False )
print( 'OpenGL version: ' + w.context.get_info().get_version() )
v = ctypes.cast( pyglet.gl.glGetString( pyglet.gl.GL_SHADING_LANGUAGE_VERSION ), ctypes.c_char_p ).value
print( '  GLSL version: %s' % v )
w.close()
