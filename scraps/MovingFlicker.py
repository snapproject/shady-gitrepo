import numpy
import Shady

Shady.Stimulus.AddCustomUniform( grain=[ 20.0, 20.0 ] )
Shady.Stimulus.AddCustomUniform( fullness=1.0 )
Shady.Stimulus.AddCustomUniform( seed=1.0 )

Shady.AddCustomSignalFunction( """
	float Blah( vec2 coords )
	{
		//vec3 seed3 = vec3( ( 1.0 + coords ) / ( 2.0 + uTextureSize ),  uSeed );
		//return ( random( seed3 ) * 0.5 + 0.5 ) * uSignalParameters[ 0 ]; // output of random() is the range [-1, +1]
	
		coords = 2.0 * mod( coords, uGrain ) / uGrain - 1.0;
		float r = max( 0.0, min( 1.0, sqrt( dot( coords, coords ) ) / uFullness ) );
		return Hann( r ) * uSignalParameters[ 0 ];
	}
""" )

w = Shady.World( canvas=True, bg=0, gamma=-1, fullScreenMode=0 )


freq = 5
amp = 0.5
midpoint = 0.5

s = w.Stimulus( signalFunction=Shady.SIGFUNC.Blah, atmosphere=w, siga=midpoint, bgalpha=0, pp=0 );
c = w.stimuli.canvas.Set( signalFunction=s, fullness=s, grain=s, atmosphere=w, siga=midpoint )
c.carrierTranslation = ( c.size - s.size ) // 2

flicker = Shady.Oscillator( lambda t: freq ) * ( lambda t: amp ) + midpoint
s.siga = flicker

s.position_function = Shady.Oscillator( 0.1, phase_deg=[ 0, 90 ] ) * 200

#@s.AnimationCallback
def Animate( self, t ):
	self.xy = self.position_function( t )
	self.carrierTranslation = -numpy.floor( self.xy )

s.xy = s.position_function
s.carrierTranslation = lambda t: -numpy.floor( s.xy )

def Noise( size=50 ):
	n = numpy.random.rand( size, size )
	s.LoadTexture( n, False )
	c.LoadTexture( n, False )
	s.Set( siga=0, bgalpha=1, pp=-1, useTexture=1 )
	c.Set( siga=0, useTexture=1 )

def Dots( siga=None ):
	if siga is None: siga = flicker
	s.Set( siga=siga, bgalpha=0, pp=0, useTexture=0 )
	c.Set( siga=midpoint, useTexture=0 )

Shady.AutoFinish( w )


