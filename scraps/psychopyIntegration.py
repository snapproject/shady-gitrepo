import random
import itertools

import Shady
import Shady.Text

def PlotResults( results, averagingFunction=None ):
	if isinstance( results, str ):
		with open( results, 'rt' ) as fh: results = fh.read().strip()
		if not results.startswith( '[' ): results = '[' + results + ']'
		import ast; results = ast.literal_eval( results )
	key = lambda result: result[ 'relativeAngle' ]
	correct   = sorted( [ result for result in results if     result[ 'responseIsCorrect' ] ], key=key )
	incorrect = sorted( [ result for result in results if not result[ 'responseIsCorrect' ] ], key=key )
	grouped = itertools.groupby( correct, key ) # GOTCHA: cannot zip(*grouped) directly: must convert each grouper to a list first
	relativeAngle, grouped = zip( *[ ( rr, list( gg ) ) for rr, gg in grouped ] )
	if averagingFunction is None: averagingFunction = lambda x: sum( x ) / float( len( x ) )
	averageLatency = [ averagingFunction( [ entry[ 'latency' ] for entry in sublist ] ) for sublist in grouped ]
	import matplotlib.pyplot as plt
	plt.ion()
	plt.cla()
	plt.plot( [ result[ 'relativeAngle' ] for result in   correct ], [ result[ 'latency' ] for result in   correct ], 'b+' )
	plt.plot( [ result[ 'relativeAngle' ] for result in incorrect ], [ result[ 'latency' ] for result in incorrect ], 'ro' )
	plt.plot( relativeAngle, averageLatency, 'b-' )
	plt.grid( True )
	plt.xlabel( 'Relative angle (degrees)' )
	plt.ylabel( 'Response latency (seconds)' )

if __name__ == '__main__':

	cmdline = Shady.WorldConstructorCommandLine( canvas=True )
	cmdline.Help().Finalize()

	numberOfTrials = 32
	leftDirection  = [ -1, +1 ]
	rightDirection = [ -1, +1 ]
	rightRotation = [ 0, 45, 90, 135, 180, 225, 270, 315 ]
	CONDITIONS = itertools.product( leftDirection, rightDirection, rightRotation )
	CONDITIONS = ( list( CONDITIONS ) * numberOfTrials )[ :numberOfTrials ]
	random.shuffle( CONDITIONS )
	CONDITIONS = iter( CONDITIONS )
	RESULTS = []

	##### Create window
	w = Shady.World( **cmdline.opts )
	
	##### Initialize stimuli
	Shady.Stimulus.SetDefault( visible=False )  # make all newly-created stimuli invisible by default
	w.Stimulus(
		text = 'F',
		text_font = 'Arial',
		text_size    = 0.20 * w.height,
		text_padto   = 0.25 * w.height,
		text_fill    = 0.0,
		text_blockbg = 1.0,
		plateauProportion = 0.98,
		backgroundAlpha = 0.0,
		position = w.Place( -0.4, 0.6 ),
		name = 'left',
	).Leave() # leave the stage (so property dynamics and texture updates are not even called)

	w.Stimulus(
		# strategy 1: copy stimulus - but this fails when not running threaded (so needs `python -m Shady` except on Windows)
		#w.stimuli.left,

		# strategy 2: Write Everything Twice
		#text = 'F',
		#text_font = 'Arial',
		#text_size    = 0.20 * w.height,
		#text_padto   = 0.25 * w.height,
		#text_fill    = 0.0,
		#text_blockbg = 1.0,
		#plateauProportion = 0.98,
		#backgroundAlpha = 0.0,
		#y = w.stimuli.left.y,
		
		# strategy 3: use ShareTexture after creation...
		size = w.stimuli.left,
		plateauProportion = w.stimuli.left,
		backgroundAlpha = w.stimuli.left,
		y = w.stimuli.left.y,

		# common to all strategies:
		x = -w.stimuli.left.x,    # but override x,
		cxscaling = -1,           # make it a mirror image,
		rotation = 60,            # and rotate it somewhat,
		visible = w.stimuli.left, # and finally link their .visible properties
		name = 'right',
	).Leave()

	w.stimuli.left.ShareTexture( w.stimuli.right )

	w.Stimulus(
		text = lambda t: ( len( RESULTS ) + 1 ),
		color = 0,
		position = w.Place( 0.9, -0.9 ),
		visible = w.stimuli.left,
		name = 'counter',
	).Leave()

	w.Stimulus(
		text = '''Welcome. You will see two letters on the screen that have been rotated. For each pair of letters, indicate if they are mirror images of each other when they two letters are in their normal upright position. (Ignore the rotations.)\n\nPress 'm' if they are mirror images of each other. Press 'n' if they are the same (not mirror images).\n\nPress the 'm' to continue.''',
		text_font = 'Arial',
		text_wrapping = -0.4 * w.width,
		text_size = 0.04 * w.height,
		position = w.Place( 0, -0.9 ),
		anchor = ( 0, -1 ),
		color = 0,
		name = 'instructions',
	).Leave()

	w.Stimulus(
		text = ''''m' for "different"\n'n' for "same"''',
		text_size = 0.03 * w.height,
		position = w.Place( 0, -0.7 ),
		color = 0,
		visible = w.stimuli.left,
		name = 'reminder',
	).Leave()
	
	##### Define the sequence of events using a state machine
	
	w.Animate = sm = Shady.StateMachine()
	@sm.AddState
	class Instructions_1( sm.State ):
		def onset( self ):
			w.stimuli.instructions.Enter( visible=True )
			@w.EventHandler( slot=-1 )
			def Advance( self, event ):
				if event >> "kp[m]": sm.ChangeState()

	@sm.AddState
	class Instructions_2( sm.State ):
		def onset( self ):
			w.stimuli.left.Enter( visible=True )
			w.stimuli.right.Enter( visible=True )
			w.stimuli.instructions.text = "Here, the letter on the right is a mirror image of the letter on the left. They would still be different after mentally rotating them to line up. So press 'm' (different). If they are the same, you would press 'n'. \n\nTry to respond as accurately as you can. Also try to be fast, but emphasize being accurate.\n\nPress 'n' to start."
			# TODO: should really use 'm' to advance rather than 'n' because 'm' is the appropriate response to the two mirror-image stimuli that are currently being presented
			@w.EventHandler( slot=-1 )
			def Advance( self, event ):
				if event >> "kp[n]": sm.ChangeState()
		def offset( self ):
			w.stimuli.instructions.Leave()
			w.stimuli.counter.Enter()
			w.stimuli.reminder.Enter()

	@sm.AddState
	class Blank( sm.State ):
		def onset( self ):
			w.stimuli.left.visible = False
		def duration( self ):
			return random.uniform( 1.0, 1.5 )
		next = 'Trial'

	@sm.AddState
	class Trial( sm.State ):
		def onset( self ):
			try:
				w.stimuli.left.cxscaling, w.stimuli.right.cxscaling, w.stimuli.right.rotation = next( CONDITIONS )
			except StopIteration:
				return self.ChangeState( 'Finish'  )
			w.stimuli.left.y = w.stimuli.right.y = 0
			w.stimuli.left.visible = True
			@w.EventHandler( slot=-1 )
			def Response( world, event ):
				if event >> "kp[m]":
					RESULTS.append( ProcessResponse( self.Elapsed( event.t ), -1 ) )
					self.ChangeState()
				if event >> "kp[n]":
					RESULTS.append( ProcessResponse( self.Elapsed( event.t ), +1 ) )
					self.ChangeState()
		def offset( self ):
			w.SetEventHandler( None, slot=-1 )
		next = 'Blank'

	@sm.AddState
	class Finish( sm.State ):
		onset = w.Close

	##### Helper function
	def ProcessResponse( latency, response ):
		correctResponse = w.stimuli.left.cxscaling * w.stimuli.right.cxscaling
		relativeAngle = w.stimuli.left.rotation - w.stimuli.right.rotation
		relativeAngle = min( relativeAngle % 360, -relativeAngle % 360 )
		result = dict(
			left_rotation   = w.stimuli.left.rotation,
			right_rotation  = w.stimuli.right.rotation,
			left_cxscaling  = w.stimuli.left.cxscaling,
			right_cxscaling = w.stimuli.right.cxscaling,
			relativeAngle = relativeAngle,
			latency = latency,
			response = response,
			correctResponse   = correctResponse, 
			responseIsCorrect = ( correctResponse == response ),
		)
		print( repr( result ) + ',' )
		return result

	##### Tidy up
	w.AfterClose( lambda: PlotResults( RESULTS ) )
	Shady.AutoFinish( w, plot=True )
