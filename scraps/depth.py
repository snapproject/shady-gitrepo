#!/usr/bin/env python
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$

#: Depth and alpha culling
"""
DOC-TODO
"""#.

import random 
if __name__ == '__main__':

	import Shady
	import Shady.Text

	import OpenGL.GL as GL

	cmdline = Shady.WorldConstructorCommandLine( width=800, height=600, fullScreenMode=0 )
	cmdline.Help().Finalize()

	w = Shady.World( **cmdline.opts )

	u = int( w.width / 20 )
	a = w.Stimulus( z=0.2, text=lambda t: 'a: z=%+.4g'%(a.z+a.oz), text_size=u*2, text_border=1.0, text_blockbg=(0.5,0.5,0.0), alpha=0.6, anchor=Shady.LEFT, x=-u*4, y=u*3 )
	b = w.Stimulus( z=0.1, text=lambda t: 'b: z=%+.4g'%(b.z+b.oz), text_size=u*2, text_border=1.0, text_blockbg=(1.0,0.0,0.0), alpha=0.6, anchor=Shady.LEFT, x=-u*7, y=0 )
	msg = w.Stimulus( pos=w.Place( -1 ), anchor=-1, text=lambda t: 'GL_DEPTH_TEST %s' % ( 'enabled' if GL.glIsEnabled( GL.GL_DEPTH_TEST ) else 'disabled' ) )
	Shady.FrameIntervalGauge( w )

	@w.EventHandler( slot=-1 )
	def ToggleCulling( self, event ):
		if event.type == 'key_press' and event.key == 'e':
			w.Culling( True )
		if event.type == 'key_press' and event.key == 'd':
			w.Culling( False )

	def StressTest( n=20, stride=-1 ):
		return [ w.MakeCanvas( z=0.99 + i * stride * 0.0001 ) for i in range( n ) ]
	
	""#>
	Shady.AutoFinish( w )

