#!/usr/bin/env python
""" "
@echo off
cls
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::

where python.exe 1>NUL 2>NUL && goto :skipconfig
:: (because any python will do, for this script)

set "CUSTOMPYTHONHOME=%PYTHONHOME_DEFAULT%
if "%CUSTOMPYTHONHOME%"=="" goto :skipconfig
set PYTHONHOME=%CUSTOMPYTHONHOME%
set PATH=%PYTHONHOME%;%PATH%
:skipconfig

python "%0" %*

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::
goto :eof
"""
LICENSE_HEADER = """
$BEGIN_SHADY_LICENSE$

This file is part of the Shady project, a Python framework for
real-time manipulation of psychophysical stimuli for vision science.

Copyright (c) {year} {author}

Shady is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses/ .

$END_SHADY_LICENSE$
"""

import os, re, sys, ast, inspect

def Canonicalize( *path_pieces ):
	return os.path.realpath( os.path.join( *path_pieces ) ).replace( '\\', '/' ).rstrip( '/' )

try:
	frame = inspect.currentframe()
	THIS = Canonicalize( inspect.getfile( frame ) )
	HERE = Canonicalize( os.path.dirname( THIS ) )
finally:
	del frame

meta = ast.literal_eval( open( Canonicalize( HERE, 'python', 'Shady', 'MASTER_META' ) ).read() )
LICENSE_HEADER = LICENSE_HEADER.format( **meta )
LICENSE_HEADER_BEGIN_TOKEN = LICENSE_HEADER.split()[  0 ]
LICENSE_HEADER_END_TOKEN   = LICENSE_HEADER.split()[ -1 ]

def Main():
	suffix = '_updated'
	for parentDir, subDirs, fileNames in os.walk( HERE ):
		if '.hg' in subDirs: subDirs.remove( '.hg' )
		if '.git' in subDirs: subDirs.remove( '.git' )
		for fileName in fileNames:
			filePath = Canonicalize( parentDir, fileName )
			if filePath == THIS: continue
			if filePath.endswith( suffix ): continue
			with open( filePath, 'rb' ) as inputFH:
				try: content = inputFH.read().decode( 'UTF-8' )
				except UnicodeError: continue
			if LICENSE_HEADER_BEGIN_TOKEN not in content: continue
			
			nCRLFs = content.count( '\r\n' )
			nCRs = content.count( '\r' ) - nCRLFs
			nLFs = content.count( '\n' ) - nCRLFs
			nLines = max( nCRLFs, nCRs, nLFs )
			lineEnding = '\n' if nLFs == nLines else '\r\n' if nCRLFs == nLines else '\r'
			licenseHeaderPattern = re.compile( r"""
				(\r\n|\r|\n)(?P<comment>[ \t]*\S+[ \t]+){beginToken}
				(
					( (\r\n|\r|\n)(?P=comment)[^\r\n]* )*?
					( (\r\n|\r|\n)(?P=comment){endToken} )
				)?
			""".format(
				beginToken = re.escape( LICENSE_HEADER_BEGIN_TOKEN ),
				endToken   = re.escape( LICENSE_HEADER_END_TOKEN ),
			), re.VERBOSE )
			def Replace( match ):
				return LICENSE_HEADER.rstrip().replace( '\n', lineEnding + match.group( 'comment' ) )
	
			newContent = licenseHeaderPattern.sub( Replace, lineEnding + content, count=1 )[ len( lineEnding ): ]
			if newContent != content:
				print( 'updating ' + filePath )
				oldMode = os.stat( filePath ).st_mode
				with open( filePath, 'wb' ) as outputFH:
					outputFH.write( newContent.encode( 'UTF-8' ) )
				os.chmod( filePath, oldMode )


if __name__ == '__main__':
	sys.exit( Main() )
