#!/bin/bash

:<<@GOTO:EOF
:: # Windows section
@echo off
:: ################################################################
:: # Requires VisualStudio.cmd from ..\accel-src\devel\build\
:: # Uses cl.exe to compile dpxmode.cpp and libdpx\libdpx.c, and to
:: # link it with the libusb-win32 libraries to build
:: # bin\Windows-64bit\dpxmode.exe or (if given input argument Win32)
:: # bin\Windows-32bit\dpxmode.exe . The corresponding libusb0.dll is
:: # also required, and is copied next to the executable.

set "TARGET=Win64"
set "IDE=2017"
if not "%~1"=="" set "TARGET=%~1"
if not "%~2"=="" set "IDE=%~2"
call %~dp0..\accel-src\devel\build\VisualStudio.cmd %TARGET% %IDE% || exit /b 1

set "STARTDIR=%CD%
set "ROOT=%~dp0
set "BITS=%TARGET:Win=%
set "DISTDIR=%ROOT%_dist
set "BINDIR=%DISTDIR%\Windows-%BITS%bit
set "USBDIR=%ROOT%libusb\Windows-%BITS%bit
set "DPXDIR=%ROOT%libdpx

mkdir "%BINDIR%" 2>NUL
cd /D "%BINDIR%"
cl.exe /MT "%ROOT%\dpxmode.cpp" "%DPXDIR%\libdpx.c" /I "%DPXDIR%" /I "%USBDIR%" "%USBDIR%\libusb.lib"
erase *.obj
cd /D "%USBDIR%"
copy *.dll                "%BINDIR%\"
copy *.exe                "%BINDIR%\"
copy *.sys                "%BINDIR%\"
copy "%ROOT%\libusb\*LICENSE*"  "%DISTDIR%\"
copy "%DPXDIR%\*LICENSE*"      "%DISTDIR%\"

cd /D "%STARTDIR%"
dir "%BINDIR%"

: ###################################################################
@GOTO:EOF
(tr -d \\r|env EXECNAME="$0" bash "$@")<<":EOF"
: ###################################################################


# posix (bash) section

ARCH=$(uname -m | perl -pe 's/^x86_64$/64bit/')

case `uname` in
	Darwin*)
		TARGET="Darwin-$ARCH"
		FRAMEWORKS="-framework Cocoa -framework IOKit"
		export MACOSX_DEPLOYMENT_TARGET="10.4"
		;;
	Linux*)
		TARGET="Linux-$ARCH"
		FRAMEWORKS="-ldl"
		;; # NB: I had to:   sudo apt-get install curl libudev-dev libtool autotools-dev automake pkg-config
esac

OLDDIR=`pwd`
ROOT=`dirname $EXECNAME`
cd "$ROOT"
ROOT=`pwd`

DISTDIR="$ROOT/_dist"
BINDIR="$DISTDIR/$TARGET"
USBDIR="$ROOT/libusb/$TARGET"
DPXDIR="$ROOT/libdpx"
ls -l "$USBDIR/libusb.a"
if [ ! -f "$USBDIR/libusb.a" ]; then

	rm -rf   "$USBDIR"/*.a "$USBDIR"/*.h
	mkdir -p "$USBDIR"
	
	cd libusb
	rm -rf _build
	mkdir _build
	cd _build

	LIBUSB_VERSION="1.0.22"
	curl -L https://github.com/libusb/libusb/releases/download/v$LIBUSB_VERSION/libusb-$LIBUSB_VERSION.tar.bz2 > src.bz2 && tar xfj src.bz2 && rm src.bz2
	LIBUSB_COMPAT_VERSION="0.1"
	curl -L https://github.com/libusb/libusb-compat-$LIBUSB_COMPAT_VERSION/archive/master.zip > src.zip && unzip src.zip && rm src.zip
	
	cd libusb-$LIBUSB_VERSION
	LIBUSB_LOCATION="$(pwd)/libusb"
	./configure CFLAGS=-fPIC CXXFLAGS=-fPIC
	make
	if [ `uname` == Linux ]; then sudo make install; fi # otherwise we don't seem to make it past ./configure (part of the autogen.sh stage) in the next section
	cd ..
	
	cd libusb-compat-$LIBUSB_COMPAT_VERSION-master
	./autogen.sh
	LIBUSB_1_0_CFLAGS="-I$LIBUSB_LOCATION"  LIBUSB_1_0_LIBS="-L$LIBUSB_LOCATION/.libs -lusb-1.0" \
	./configure CFLAGS=-fPIC CXXFLAGS=-fPIC
	make  # NB linking the actual libraries may fail here, but that's OK because we're just going to grab the .o files
	cp libusb/usb*.h "$USBDIR"/
	cd ..
		
	rm -rf obj
	mkdir obj
	find . -iname '*.o' -exec cp {} obj/ \;
	ar rcs "$USBDIR"/libusb.a obj/*.o
fi

cd "$ROOT"
mkdir -p "$BINDIR"
gcc -c "$DPXDIR"/libdpx.c -I"$DPXDIR" -I"$USBDIR"
g++ -o "$BINDIR"/dpxmode dpxmode.cpp libdpx.o -I"$DPXDIR" -L"$USBDIR" -lusb $FRAMEWORKS
cp "$ROOT"/libusb/LICENSE* "$DISTDIR"/
cp "$ROOT"/libdpx/LICENSE* "$DISTDIR"/
rm libdpx.o

cd "$OLDDIR"
ls -AFlhd "$BINDIR"/*
: ###################################################################
:EOF

