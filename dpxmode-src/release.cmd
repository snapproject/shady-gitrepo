#!/usr/bin/env python
"""
@echo off
cls
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::

where python.exe 1>NUL 2>NUL && goto :skipconfig
:: (because any python will do, for this script)

set "CUSTOMPYTHONHOME=%PYTHONHOME_DEFAULT%
if "%CUSTOMPYTHONHOME%"=="" goto :skipconfig
set PYTHONHOME=%CUSTOMPYTHONHOME%
set PATH=%PYTHONHOME%;%PATH%
:skipconfig

python "%~0" %*

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::
goto :eof
"""
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$

import os, sys, shutil, time, glob, inspect

try: frame = inspect.currentframe();  HERE = os.path.dirname( inspect.getfile( frame ) )
finally: del frame

def canonicalize( *path_pieces ):
	return os.path.realpath( os.path.join( *path_pieces ) ).replace( '\\', '/' ).rstrip( '/' )


def mkdir( *path_pieces ):
	path = canonicalize( *path_pieces )
	if not os.path.isdir( path ):
		print( "creating directory %s" % path )
		os.makedirs( path )
	return path
		
def transfer( srcRoot, dstRoot, manifest, verbose=True, fake=False ):

	if not os.path.isdir( dstRoot ):
		raise OSError( "destination directory %r does not exist" % dstRoot )
		
	srcRoot = canonicalize( srcRoot )
	dstRoot = canonicalize( dstRoot )
	
	manifest = sorted( [
		( match[ len( srcRoot ) + 1: ].replace( '\\', '/' ), verb.lower() )
		for entry in manifest
		for verb, pattern in ( [ entry.split( ' ', 1 ) ] if entry.startswith( ( 'copy ', 'move ' ) ) else [ [ 'copy', entry ] ] )
		for match in glob.glob( srcRoot + '/' + pattern )
	] )
	
	if verbose:
		print( "      SOURCE = %s" % srcRoot )
		print( " DESTINATION = %s" % dstRoot )
		if manifest: print( "\n%s\n" % '\n'.join( '    %s  %s' % ( verb, file ) for file, verb in manifest ) )
		else: print( '\n    NO MATCHING FILES FOUND\n' )
		
	for file, verb in manifest:
		source = srcRoot + '/' + file
		destination = dstRoot + '/' + file
		if verbose: print( "\n% 8sing from: %s\n              to: %s" % ( verb.rstrip( 'e' ), source, destination ) )
		if fake: continue
		mkdir( destination, '..' )
		shutil.copy2( source, destination )
		if verb == 'move':
			try: os.remove( source )
			except: print( '\nWARNING: failed to remove original %s' % source )


def main( verbose=True, fake=False ):
	return transfer(
		srcRoot = canonicalize( HERE, '_dist' ),
		dstRoot = canonicalize( HERE, '..', 'python', 'Shady', 'VPixx', 'bin' ),
		manifest = [
			'copy LICENSE*.txt',
			'copy */dpxmode',
			'copy */dpxmode.exe',
			'copy */*.dll',
		],
		verbose = verbose,
		fake = fake,
	)
						
if __name__ == '__main__':
	nargs = len( sys.argv ) - 1
	if nargs:
		sys.stderr.write( 'usage: %s\n' % ( sys.argv[ 0 ], ) )
		sys.exit( 1 )
		
	try:
		sys.exit( main() )
	except Exception as err:
		sys.stderr.write( '%s\n' % err )
		sys.exit( -1 )
