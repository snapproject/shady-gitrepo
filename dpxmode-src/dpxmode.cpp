/*
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
*/

#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <string>
extern "C" {
#include "libdpx.h"
}

/*

From libdpx_i.h:

	//		000:	C24			Straight passthrough from DVI 8-bit (or HDMI "deep" 10/12-bit) RGB to VGA 8/10/12-bit RGB
	//		001:	L48			DVI RED[7:0] is used as an index into a 256-entry 16-bit RGB colour lookup table
	//		010:	M16			DVI RED[7:0] & GREEN[7:0] concatenate into a VGA 16-bit value sent to all three RGB components
	//							Also implements a CLUT overlay which is indexed by a non-zero blue component
	//		011:	C48			Even/Odd pixel RED/GREEN/BLUE[7:0] concatenate to generate 16-bit RGB components at half the horizontal resolution
	//		100:	RSVD		Reserved for future use.  Same as VMODE_C24_c for now.
	//		101:	L48D		DVI RED[7:4] & GREEN[7:4] concatenate to form an 8-bit index into a 256-entry 16-bit RGB colour lookup table
	//		110:	M16D		DVI RED[7:3] & GREEN[7:3] & BLUE[7:2] concatenate into a VGA 16-bit value sent to all three RGB components
	//		111:	C36D		Even/Odd pixel RED/GREEN/BLUE[7:2] concatenate to generate 12-bit RGB components at half the horizontal resolution

	#define DPXREG_VID_CTRL_MODE_C24	0x0000
	#define DPXREG_VID_CTRL_MODE_L48	0x0001
	#define DPXREG_VID_CTRL_MODE_M16	0x0002
	#define DPXREG_VID_CTRL_MODE_C48	0x0003
	#define DPXREG_VID_CTRL_MODE_L48D	0x0005
	#define DPXREG_VID_CTRL_MODE_M16D	0x0006
	#define DPXREG_VID_CTRL_MODE_C36D	0x0007


*/
#define _MODESTR( m, x ) ( m == DPXREG_VID_CTRL_MODE_ ## x ) ? #x
#define _MODEVAL( s, x ) ( s == #x ) ? DPXREG_VID_CTRL_MODE_ ## x
#define MODESTR( m ) ( _MODESTR(m,C24) : _MODESTR(m,L48) : _MODESTR(m,M16) : _MODESTR(m,C48) : _MODESTR(m,L48D) : _MODESTR(m,M16D) : _MODESTR(m,C36D) : "???" )
#define MODEVAL( s ) ( _MODEVAL(s,C24) : _MODEVAL(s,L48) : _MODEVAL(s,M16) : _MODEVAL(s,C48) : _MODEVAL(s,L48D) : _MODEVAL(s,M16D) : _MODEVAL(s,C36D) : -1 )

int main( int argc, const char * argv[] )
{	
	int targetMode = -1;
	if( argc > 1 )
	{
		std::string s = argv[ 1 ];
		std::string s_upper;
		for( std::string::iterator it = s.begin(); it != s.end(); it++ )
			s_upper += ::toupper( *it );
		targetMode = MODEVAL( s_upper );
		if( targetMode < 0 )
		{
			fprintf( stderr, "unrecognized DataPixx mode \"%s\"\n", s.c_str() );
			return -2;
		}
	}
		
	DPxOpen();
	int result = DPxGetError();	
	if( result != DPX_SUCCESS )
	{
		fprintf( stderr, "DataPixx initialization failed with error %d: ensure the device is connected via USB\n", result );
		return result;
	}
	if( !DPxIsReady() )
	{
		fprintf( stderr, "DataPixx device is not ready\n" );
		DPxClose();
		return -1;
	}
	DPxStopAllScheds();
	DPxUpdateRegCache();
	int startingMode = DPxGetVidMode();
	if( targetMode >= 0 )
	{
		DPxSetVidMode( targetMode ); 
		DPxUpdateRegCache();
		fprintf( stdout, "%s -> %s\n", MODESTR( startingMode ), MODESTR( targetMode ) );
	}
	else
	{
		fprintf( stdout, "%s\n", MODESTR( startingMode ) );
	}
	DPxClose();
	return 0;
}