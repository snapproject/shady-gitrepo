These files are from

    https://sourceforge.net/projects/libusb-win32/files/libusb-win32-releases/1.2.6.0/libusb-win32-bin-1.2.6.0.zip

downloaded on 2019-03-12, specifically from the following subdirectories:

  - `include`  (the header file, renamed from `lusb0_usb.h` to `usb.h` to be
               consistent with other platforms)
  - `lib\msvc` (the `.lib` file, which is only a stub)
  - `bin\x86`  (everything else, including a DLL that had to be renamed from
               `libusb0_x86.dll` to `libusb0.dll` because the latter is what's
               hard-coded into the stub library).

The libusb-win32 project's license file (originally called `installer_license.txt`,
but seemingly referring to all the components) is reproduced here as `LICENSE-libusb-win32.txt`.
