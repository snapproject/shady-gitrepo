These files are from

    https://sourceforge.net/projects/libusb-win32/files/libusb-win32-releases/1.2.6.0/libusb-win32-bin-1.2.6.0.zip

downloaded on 2019-03-12, specifically from the following subdirectories:

  - `include`      (the header file, renamed from `lusb0_usb.h` to `usb.h` to be
                   consistent with other platforms)
  - `lib\msvc_x64` (the `.lib` file, which is only a stub)
  - `bin\amd64`    (everything else)

The libusb-win32 project's license file (originally called `installer_license.txt`,
but seemingly referring to all the components) is reproduced here as `LICENSE-libusb-win32.txt`.
