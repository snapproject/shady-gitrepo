This subdirectory contains C++ and C source files, as well as binaries of
some third-party libraries, for building `dpxmode`, a simple command-line utility
for querying and setting the display mode of a ViewPixx display (VPixx, Inc.) or
similar device that supports the DataPixx API.

`build.cmd`  works on both Windows and posixoid systems to compile from source.
The manufacturer's GPL'd code, `libdpx`, is provided as source.  It must be linked
against the third party library `libusb`---specifically, a legacy version that is
maintained as `libusb-compat`. This appears to be a compatibility *wrapper*, so
building it requires a prior build of the main `libusb` library.

On Windows, `build.cmd` assumes the command-line argument `Win64` by default, but
you can also say `build.cmd Win32`. The build process requires our Visual-Studio-
finding utility, `..\accel-src\build\devel\VisualStudio.cmd`. We have included
32-bit and 64-bit binary libraries from the `libusb-win32` project. These are
unfortunately stub libraries, so the corresponding DLL file `libusb0.dll` must
accompany `dpxmode.exe` wherever it goes.  Driver installation is also required,
the files for which are included here, but this should have been done anyway, per
the manufacturer's instructions, when you installed your ViewPixx device.

On posix-like systems, if the appropriate build of `libusb.a` is missing, `build.cmd`
will automatically download and expand source archives for, and then build, both
`libusb` and `libusb-compat`.  The build for `libusb-compat` may actually fail, but
we simply copy all the `.o` files from both projects and link them together so that
we end up with a single static library containing all the `libusb` and `libusb-compat`
code.  This is the only way we have found of creating a binary that can be released
to users, without requiring those users to grok, build and install `libusb` on their
own system---that is an unrealistic expectation and would entail a lot of
documentation and support.

Update 2022-02-07:  on macOS 12.0.1 (Monterey) on M1, the following prerequisites
had to be installed before running `build.cmd`::

      # install homebrew:
      /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
      echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> /Users/m1/.zprofile
      eval "$(/opt/homebrew/bin/brew shellenv)"
      
      # use homebrew:
      brew install automake libtool pkg-config


`release.cmd` (really a self-executing Python script, Windows- and posix-compatible)
can be used to copy the binary files from `_dist` where they are built, into the
appropriate part of `../python/Shady`, where they are version-controlled and will
also get bundled inside the `.whl` for release.

So, in summary---on Windows::

    build.cmd Win64
    build.cmd Win32
    release.cmd

and on others::

	./build.cmd
	./release.cmd
