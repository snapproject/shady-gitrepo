# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$

r"""
If you're a user and you're seeing this file, it's presumably because
you checked out a bleeding-edge copy of Shady in the form of a git
repository.  In that case, the best way to use this file is to install
the included working copy of the Python package, `./python/Shady`, as an
"editable" package::

    python -m pip install -e .

(where it is assumed that the current working directory `.` is the
directory that contains this `setup.py` file).


The other usage of this file is to package Shady up for release, which
only the Shady project maintainers will need to do. So, if that's not
you, you can stop reading now.  If it is you, then here's how it works:

Your Python distro will need the following prerequisites installed::

    python -m pip install wheel    # only necessary for older Python distros
    python -m pip install twine

As always, make sure `python` invokes the correct Python! That also applies
during the following release procedure:

1. Verify/ensure that the version number in `python/Shady/MASTER_META` is
   new, i.e. it exceeds the largest version number available on PyPI at
   https://pypi.org/project/Shady/#files

2. Has the accelerator code changed since the last release?  If so:

   a. Bump the ShaDyLib (C++ source) version number to match `MASTER_META`,
      in the #defines near the top of `./accel-src/release/ShaDyLib.h`

   b. Ensure the accelerator is built for all relevant platforms. For example,
      to build on Windows::

          .\accel-src\devel\build\go.cmd
          .\accel-src\devel\build\go.cmd Win32

   c. Ensure the new accelerator is bundled inside the Python package
      (here we're assuming that Shady is installed as "editable", i.e.
      with `python -m pip install -e .` in whichever distro the `python`
      command invokes)::

          .\accel-src\devel\build\release.cmd
    
   d. Commit, push, pull, and do the same on the Mac::
   
          ./accel-src/devel/build/go.cmd
          ./accel-src/devel/build/release.cmd
       
3. Commit everything to the `default` branch.

4. Update the content of the `release` branch::

       git checkout release
       git merge master -m "release X.Y.Z"
       git checkout master
       # do not post-bump the version numbers yet: setup.py will need them
   
5. Make and upload the release::

       python setup.py bdist_wheel --universal
       python -m twine upload dist/*
       rm -rf build dist python/Shady.egg-info
       
       # To test, using a distro that has a previous pip-installed version:
       python -m pip install --upgrade Shady --no-deps
   
6. As soon as the upload is done, maybe *already* bump the version number
   in `MASTER_META` and `ShaDyLib.h`, and commit those changes (but only
   to the `default` branch). The two advantages to that are (i) that any
   unreleased changes you now make will be associated with a version that
   is distinct from all pippable releases, and (ii) that you won't have
   to take any action in steps 1 and 2a. next time around.

7. Push everything to the server repo::

       git push --all --follow-tags

8. After a short while, check https://shady.readthedocs.io (for the release
   version) and https://shady.readthedocs.io/en/latest (for the default
   branch). Refresh both pages in the browser and verify the new version
   numbers in each.
"""
import os
import sys
import inspect
import textwrap

import setuptools


package_dir = 'python'

# import Shady itself - but make sure it's the to-be-installed version and not some legacy version hanging over
try: __file__
except: __file__ = inspect.getfile( inspect.currentframe() )
sys.path.insert( 0, os.path.join( os.path.dirname( __file__ ), package_dir ) )
import Shady
sys.path.pop( 0 )

meta = Shady.__meta__

class Indenter( object ):
	def __init__( self, *pargs, **kwargs ):
		self.things = dict( *pargs, **kwargs )
	def __getitem__( self, arg ):
		key = arg.lstrip()
		prefix = arg[ :-len( key )  ]
		txt = str( self.things[ key ] )
		return ''.join( prefix + line for line in textwrap.dedent( txt.replace( '\t', '    ' ) ).splitlines( True ) ).strip()
meta[ 'indent' ] = Indenter( meta )

# https://packaging.python.org/tutorials/distributing-packages/#setup-args
setup_args = dict(
	name = 'Shady',
	description = 'An engine for real-time manipulation of visual stimuli for neuroscience, implemented via GPU shaders',
	long_description = """\
Shady is a general-purpose visual stimulus toolbox filling a similar role to Psychtoolbox,
VisionEgg, or PsychoPy. It is for programmers who work in neuroscience, especially vision
science, and addresses their need for high timing precision, linearity, high dynamic
range, and pixel-for-pixel accuracy.

It takes its name from its heavy reliance on a *shader* program to perform parallel pixel
processing on a computer's graphics processor. It was designed with an emphasis on
performance robustness in multi-tasking applications under unforgiving conditions.
For optimal timing performance, the CPU drawing management commands are carried out by
a compiled binary engine.

See {homepage} for full documentation and installation instructions.

If you use Shady in your work, please cite:

* {indent[  citation]} ::

      {indent[      bibtex]}

""".format( **meta ),
	long_description_content_type='text/x-rst',
	license = "GPL v3+",
	url = meta[ 'homepage' ],
	author = meta[ 'author' ],
	author_email = meta[ 'email' ],
	version = meta[ 'version' ],
	python_requires = '>=2.7, !=3.0.*, !=3.1.*, !=3.2.*, <4',
	install_requires = [ 'numpy', 'pillow', 'matplotlib', 'ipython', ], # not really *required*, but you can't have much fun without them
	extras_require = { 
		'textures':  [ "numpy" ],                         # obsolete because now all included in install_requires
		'images':    [ "numpy", "pillow" ],               # obsolete because now all included in install_requires
		'fonts':     [ "numpy", "pillow", "matplotlib" ], # obsolete because now all included in install_requires
		'shell':     [ "ipython" ],                       # obsolete because now all included in install_requires
		'video':     [ "numpy", "opencv-python" ],
		'pure':      [ "pyglet" ],
	},
	package_dir = {
		'' : package_dir,  # NB: can't be absolute, can't go upwards
	},
	#packages = [],
	#package_data = {},
	#data_files = [], # NB: when pip-installed, items listed here get installed *outside* the package directory
	**Shady.Manifest( 'setup' )
)

if __name__ == '__main__':
	if len( sys.argv ) < 2: # called without any subcommand---would normally prompt print-usage-and-exit
		default_subcmd = 'bdist_wheel --universal'
		sys.argv += default_subcmd.split()
		print( 'Assuming subcommand: ' + default_subcmd )
	setuptools.setup( **setup_args )
