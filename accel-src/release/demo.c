/*

# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$

This file illustrates some of the functionality of the ShaDyLib dynamic library.
It is intended to be read as documentation, with examples. It can be compiled.


1. Getting started

To use ShaDyLib in your application:

(1) Add the file ShaDyLib_Loader.c to your project (can be compiled as C or C++)

(2) #include "ShaDyLib.h" in your own C or C++ code

(3) Call Load_ShaDyLib() in your code before calling any of the other functions.
    The argument to Load_ShaDyLib may be a string that explicitly specifies the
    name of the dynamic library file to load. Alternatively, if you pass NULL
    then the default name will be used.  This default can be manipulated at
    compile time by defining the SHADYLIB_PLATFORM symbol. For example, using gcc on
    Mac OSX on a 64-bit intel machine:

       gcc -DSHADYLIB_PLATFORM=-Darwin-x86_64  demo.c ShaDyLib_Loader.c

    will cause Load_ShaDyLib(NULL) to look for "libShady-Darwin-x86_64.dylib"
    instead of just "libShady.dylib". As another example, if you're using Visual
    C++ to compile a 32-bit binary on a Windows machine, you could say:

       cl /DSHADYLIB_PLATFORM=-Windows-i386  demo.c ShaDyLib_Loader.c

    which would cause Load_ShaDyLib(NULL) to look for "libShady-Windows-i386.dll"
    instead of just "libShady.dll" .

(4) Check the return value of Load_ShaDyLib before proceeding. It will be zero on
    success. Any non-zero value means the library failed to load and other library
	calls (function names starting with ShaDyLib_) should not be called.


This file may be compiled. In it, we will build documented example applications
for .....
                                                                              */

#include "ShaDyLib.h"

#include <stdio.h>

    int Demo( const char * arg1, const char * arg2, const char * arg3 );
    // to be defined later
    
    int main( int argc, const char * argv[] )
    {

        const char * arg1  = ( argc > 1 && *argv[ 1 ] ) ? argv[ 1 ] : NULL;
        // First command-line argument: ...
        
        const char * arg2  = ( argc > 2 && *argv[ 2 ] ) ? argv[ 2 ] : NULL;
        // Second command-line argument: ...
        
        const char * arg3  = ( argc > 3 && *argv[ 3 ] ) ? argv[ 3 ] : NULL;
        // Third command-line argument: ...


        const char * dllname = NULL;
        // NULL causes the default name for the dynamic library to be used
        // (incorporating the SHADYLIB_PLATFORM macro, if defined).
        
        int load_error = Load_ShaDyLib( dllname );
        if( load_error < 0 ) return fprintf( stderr, "failed to load dynamic library \"%s\"\n", SHADYLIB_DYLIB_NAME( dllname ) );
        if( load_error > 0 ) return fprintf( stderr, "failed to import %d functions from dynamic library \"%s\"\n", load_error, SHADYLIB_DYLIB_NAME( dllname ) );
		
        return Demo( arg1, arg2, arg3 );
    }
                                                                              /*

2.X `ShaDyLib_MessageCallback`

A `ShaDyLib_MessageCallback` is type denoting a pointer to a function implemented by
you, which has the following prototype:

    int func( const char * msg, int debugLevel );

A function of this type can be registered using
`ShaDyLib_Connection_SetMessageCallback(c, func);` This causes `func` to be called
whenever methods of a particular `ShaDyLib_Connection` deliver a warning or debugging
message.  The `debugLevel` argument will contain a number that indicates the
urgency or criticality of the message: higher numbers indicate more trivial or
routine messages. In your callback, you will probably want to process the
message only when its `debugLevel` is below a certain threshold (`debugLevel<=2`
may be a good starting point). We will use a simple example:
                                                                              */
    int Message( const char * msg, int debugLevel )
    {
        if( debugLevel > 2 ) return 0;
        return fprintf( stderr, "ShaDyLib Message (level %d): %s\n", debugLevel, msg );
    }
                                                                              /*
A `ShaDyLib_MessageCallback` function can also be registered globally using
`ShaDyLib_SetErrorCallback( func );`  This provides the opportunity to handle errors
in a more thread-safe way than with `ShaDyLib_Error()`, as described in the next
section.


3. Handling errors

There are two ways to handle errors in the ShaDyLib API. Whenever a ShaDyLib
call throws an error, it will store a single global string pointer that contains
the error message.  Both `ShaDyLib_Error()` and `ShaDyLib_ClearError()` can be used
to check for errors: they both return `NULL` if no error message has been issued
since he last call to `ShaDyLib_ClearError()`.  If there has been an error, they
return a `const char *`  to the error message. Note that all `const char *`  string
pointers returned by ShaDyLib functions like this are semi-persistent and
non-thread-safe.  If you need to keep the string, allocate memory and copy it
(the easy way is to assign it to a `std::string` in C++) before calling any more
ShaDyLib functions.

You should check for errors after any ShaDyLib call.  One way to do this is
illustrated in the following example, where a macro `CHECK` is defined to allow
easy return from `main()` or from any other function that announces failure
using a non-zero return value:
                                                                              */
    int CheckError( void )
    {
        if( ShaDyLib_Error() ) return fprintf( stderr, "%s\n", ShaDyLib_ClearError() );
        else return 0;
    }
    #define CHECK     if( CheckError() != 0 ) return -1;
                                                                              /*
We will use the `CHECK` macro to make our subsequent examples more readable, but
it is up to you to define it, or something similar, if you choose to handle
errors this way in your own code.

Reliance on a single global error pointer will be sufficient for most purposes,
but may cause problems in some multi-threaded designs.  An alternative method is
to register a `ShaDyLib_MessageCallback` function using the global function
`ShaDyLib_SetErrorCallback( func )`.   Again, this approach makes use of global
memory to store the function pointer---so separate threads should not attempt to
set *different* error callbacks---but the error string itself will not be global
when passed to the callback, and will not conflict with errors issued by other
threads. Your implementation of the callback can then implement mutexing as
necessary,  associate an error notification with the current thread id, or
whatever other measures are appropriate for your design.


4.  Application structure
                                                                              */
    int Demo( const char * arg1, const char * arg2, const char * arg3 )
    {
        ShaDyLib_SanityCheck(); CHECK
        fprintf( stderr, "ShaDyLib Version %s loaded and running on %s\n", ShaDyLib_GetVersion(), ShaDyLib_GetPlatform() );
        return 0;
    }
