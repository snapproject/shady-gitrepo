# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$


The ShaDyLib dynamic libraries in this directory have version @VERSION@.

To create a ShaDyLib application:

(1) Add the file ShaDyLib_Loader.c to your project (can be compiled as C or C++)

(2) #include "ShaDyLib.h" in your own C or C++ code

(3) Call Load_ShaDyLib() in your code before calling any of the other functions.
    The argument to Load_ShaDyLib may be a string that explicitly specifies the
    name of the dynamic library file to load. Alternatively, if you pass NULL
    then the default name will be used.  This default can be manipulated at
    compile time by defining the SHADYLIB_PLATFORM symbol. For example, using gcc on
    Mac OSX on a 64-bit intel machine:
    
       gcc -DSHADYLIB_PLATFORM=-Darwin-x86_64  demo.c ShaDyLib_Loader.c
    
    will cause Load_ShaDyLib(NULL) to look for "libShady-Darwin-x86_64.dylib"
    instead of just "libShady.dylib". As another example, if you're using Visual C++
    to compile a 32-bit binary on a Windows machine, you could say:
    
       cl /DSHADYLIB_PLATFORM=-Windows-i386  demo.c ShaDyLib_Loader.c
    
    which would cause Load_ShaDyLib(NULL) to look for "libShady-Windows-i386.dll"
    instead of just "libShady.dll" .

(4) Check the return value of Load_ShaDyLib before proceeding. It will be zero on
    success. Any non-zero value means the shared library failed to load and other
	library calls (function names starting with ShaDyLib_) should not be called.


Consult demo.c for commented example code.
