/*

# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$

Add this file to your C or C++ project.  Call Load_ShaDyLib() and check that its return
value is 0 before proceeding to call any ShaDyLib_... functions. See README.txt for more details.

*/
 
#include "ShaDyLib.h"

const char * ShaDyLib_Stub( void ) { static char msg[] = "ShaDyLib is not loaded"; return msg; }
#define SHADYLIB_FUNC( type, name, args, implementation )   type ( *name ) args = ( type ( * ) args )ShaDyLib_Stub;
#include "ShaDyLib.h"

#define DLERR 0
#ifdef _WIN32
#	include <windows.h>
#	define DynamicLibraryPtr                 HINSTANCE
#	define dlopen( LIBNAME, MODE )           LoadLibrary( LIBNAME )
#	define dlsym( DLPTR, FUNCNAME )          GetProcAddress( ( DLPTR ), ( FUNCNAME ) )
#else
#	include <dlfcn.h>
#	define DynamicLibraryPtr                 void *
#	define LoadLibrary( LIBNAME )            dlopen( LIBNAME, RTLD_NOW | RTLD_GLOBAL )
#	define GetProcAddress( DLPTR, FUNCNAME ) dlsym( ( DLPTR ), ( FUNCNAME ) )
#	if DLERR
#		include <stdio.h>
#		undef  DLERR
#		define DLERR                          if( !dll ) fprintf( stderr, "dlopen() error: %s\n", dlerror() );
#	endif
#endif /* _WIN32 */


int Load_ShaDyLib( const char *dllname )
{
	int failures = 0;
	DynamicLibraryPtr dll = LoadLibrary( SHADYLIB_DYLIB_NAME( dllname ) ); DLERR;
	if( !dll ) return -1;
#define SUPPRESS_EXTERNC
#define SHADYLIB_FUNC( type, name, args, implementation )  \
	if ( ( name = ( type ( * ) args )GetProcAddress( dll , #name ) ) == 0 ) failures++;
#include "ShaDyLib.h"
	return failures;
}
