/*
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
*/

#ifndef   INCLUDE_FileUtils_CPP
#define   INCLUDE_FileUtils_CPP

#include "FileUtils.hpp"
#include "ExceptionUtils.hpp"

#include <fstream>
#include <cerrno>

#ifdef _WIN32
#	include "windows.h"
#	ifdef    _MSC_VER // if we're using MS Visual C(++)
#		include <direct.h>
#		include "dirent_win.h"
#		define getcwd _getcwd // avoid stupid "posix name deprecated" warning
#		define mkdir _mkdir   // avoid stupid "posix name deprecated" warning
#		define chdir _chdir   // avoid stupid "posix name deprecated" warning
#	else  // _MSC_VER
#		include <dir.h>
#		include <dirent.h>
#	endif // _MSC_VER
#else  // _WIN32
#	include <sys/stat.h>
#	include <dirent.h>
#	include <unistd.h>
#	include <dlfcn.h>
#endif // _WIN32

bool
FileUtils::DirectoryExists( std::string x )
{
  DIR * p = ::opendir( x.c_str() );
  bool exists = ( p != NULL );
  if( p ) ::closedir( p );
  return exists;
}

bool
FileUtils::FileExists( std::string x )
{
	std::ifstream ifile( x.c_str() );
	return ifile.good();
}

std::string
FileUtils::GetWorkingDirectory( void )
{
	const int bufSize = 1024;
	char buf[bufSize];
	const char *cwd = ::getcwd( buf, bufSize );
	std::string result;
	if( cwd ) result = cwd;
	return StandardizePath( result );
}

std::string
FileUtils::StandardizePath( std::string x )
{
	std::string y;
	bool ignore = false;
	for( unsigned int i = 0; i < x.size(); i++)
	{
		char c = x[i];
		if( c == '\\' || c == '/' )
		{
			if( i == x.size() - 1 ) break;
			if( !ignore ) y += FILE_SEPARATOR;
#ifdef    _WIN32
			ignore = ( y.size() > 1 );
#else  // _WIN32
			ignore = true;
#endif // _WIN32
		}
		else
		{
			y += c;
			ignore = false;
		}
	}
#ifdef    _WIN32
	if( y.length() == 2 && y[ 1 ] == ':' ) y += "\\";
#else  // _WIN32
#endif // _WIN32
	return y;
}

std::string
FileUtils::JoinPath( std::string parent, std::string child )
{
	if( parent.size() == 0 ) return child;
	return StandardizePath( parent + FILE_SEPARATOR + child );
}

void
FileUtils::SplitPath( std::string fullpath, std::string & parent, std::string & stem, std::string & extension )
{
	fullpath = StandardizePath( fullpath );
	parent = ""; stem = ""; extension = "";
#ifdef    _WIN32
	if( fullpath.length() == 3 && fullpath[ 1 ] == ':' ) return;
#else  // _WIN32
#endif // _WIN32	
	size_t parentLength = fullpath.size();
	for( parentLength = fullpath.size(); parentLength > 0; parentLength-- )
		if( fullpath[parentLength-1] == FILE_SEPARATOR ) break;
	size_t dotPos=fullpath.size();
	for( size_t i = parentLength; i < fullpath.size(); i++ )
		if( fullpath[i] == '.' ) dotPos = i;

	if( parentLength ) parent = StandardizePath( fullpath.substr( 0, parentLength ) );
	if( parentLength < fullpath.size() ) stem = fullpath.substr( parentLength, dotPos - parentLength );
	if( dotPos < fullpath.size() ) extension = fullpath.substr( dotPos );
}

std::string
FileUtils::ParentDirectory( std::string x, int levels )
{
	std::string stem, extension;
	x = RealPath( x );
	for( int i = 0; i < levels && x.length(); i++ ) SplitPath( x, x, stem, extension );
	return x;
}

std::string
FileUtils::BaseName( std::string x )
{
	std::string parent, stem, extension;
	SplitPath( RealPath( x ), parent, stem, extension );
	return stem + extension;
}

std::string
FileUtils::RealPath( std::string x )
{
	if( x.size() == 0 ) return x;
	if( !DirectoryExists( x ) )
	{
		std::string parent, stem, xtn;
		SplitPath( x, parent, stem, xtn );
		return JoinPath( RealPath( parent ), stem + xtn );
	}
	x = StandardizePath( x );
	std::string oldd = GetWorkingDirectory();
	int err = ::chdir( x.c_str() );
	if( err == 0 ) x = GetWorkingDirectory();
	err = chdir( oldd.c_str() );
	return x;
}

bool
FileUtils::PathMatch( std::string a, std::string b, bool partial )
{
	a = RealPath( a );
	b = RealPath( b );
	if( partial && a.size() > b.size() ) a = a.substr( 0, b.size() );
	return a == b;
}

int
FileUtils::MakeDirectory( std::string x )
{
	int err;
#ifdef    _WIN32
	err = ::mkdir( x.c_str() );
#else  // _WIN32
	const int rwxr_xr_x = 0755;
	err = ::mkdir( x.c_str(), rwxr_xr_x );
#endif // _WIN32
	if( err ) { RAISE( "failed to create directory " << x ); }
	return err;
}

int
FileUtils::MakePath( std::string x )
{
	if( DirectoryExists( x ) ) return 0;
	std::string parent, name, extn;
	SplitPath( x, parent, name, extn);
	if( parent.size() > 0 && !DirectoryExists( parent ) )
	{
		if( MakePath( parent ) != 0 ) { RAISE( "failed to make directory " << parent ); }
	}
	return MakeDirectory( x );
}

int
FileUtils::RemoveFile( std::string x )
{
#ifdef    _WIN32
	return ( ::DeleteFileA( x.c_str() ) ? 0 : ::GetLastError() );
#else  // _WIN32
	return ( ::unlink( x.c_str() ) ? errno : 0 );
#endif // _WIN32
}

std::string
FileUtils::WhereAreYou( void * funcPtr, int up )
{
	std::string location;
	if( !funcPtr ) funcPtr = ( void * )&WhereAreYou;
#ifdef    _WIN32
	char path[ 1024 ] = "";
	HMODULE moduleHandle = NULL;
	// NB if not defined, GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS is 4 and GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT is 2
	if( GetModuleHandleExA( GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT, ( LPCSTR ) funcPtr, &moduleHandle ) )
	{
		GetModuleFileNameA( moduleHandle, path, sizeof( path ) );
		location = path;
	} // else GetLastError() can get you the error code
#else  // _WIN32
	Dl_info info;
	if( dladdr( funcPtr, &info ) ) location = info.dli_fname;
#endif // _WIN32
	if( up ) location = ParentDirectory( location, up );
	return location;
}


#endif // INCLUDE_FileUtils_CPP
