/*
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
*/

#ifndef INCLUDE_BasicTypes_HPP
#define INCLUDE_BasicTypes_HPP

#include <stddef.h> // for size_t
#include <stdint.h> // defines 8-, 16- and 32-bit signed and unsigned integer types (int8_t, uint8_t, etc)
                    // NB: if you change this to <cstdint> then you seem to get namespace problems (must include
					// this file outside of any of our own namespace definitions - but with stdint.h, it seems
					// to work OK).
typedef float  float32_t;
typedef double float64_t;

size_t as_size_t( uint64_t i );


#endif // ifndef INCLUDE_BasicTypes_HPP
