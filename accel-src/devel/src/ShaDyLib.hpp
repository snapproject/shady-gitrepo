/*
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
*/

#ifndef INCLUDED_ShaDyLib_HPP
#define INCLUDED_ShaDyLib_HPP

#include <map>
#include <vector>
#include <string>
#include <iostream>

#include "BasicTypes.hpp"
#include "ExceptionUtils.hpp"
#include "DebuggingUtils.hpp"
#include "StringUtils.hpp"
#include "TimeUtils.hpp"

#include "ShaDyLib_PackedTypes.hpp"

namespace ShaDyLib
{
	class LinkGL;
	class Renderer;
	class Stimulus;
	class RGBTable;
	class Property;
	
	typedef std::vector < Stimulus * >          StimulusVector;
	typedef std::map< std::string, Stimulus * > StimulusMap;
	typedef std::map< std::string, Property * > PropertyMap;
	
	typedef  int ( *MessageCallback )( const char * msg, int debugLevel );
	typedef  int ( *UpdateCallback  )( float64_t t, void * ptr );
	typedef  void ( *EventCallback  )( const char * msg );
	
	void         SanityCheck( void );
	std::string  GetPlatform( void );
	std::string  GetRevision( void );
	std::string  GetCompilationDatestamp( void );
	
	void         DoAnchorTranslation( int uniformAddress, float32_t * matrix, float64_t anchorX, float64_t anchorY, float64_t width, float64_t height );
	void         DoEnvelopeScaling( int uniformAddress, float32_t * matrix, float64_t scaleX, float64_t scaleY );
	void         DoEnvelopeRotation( int uniformAddress, float32_t * matrix, float64_t rotationZ );
	void         DoEnvelopeTranslation( int uniformAddress, float32_t * matrix, float64_t positionX, float64_t positionY, float64_t positionZ, float64_t originX, float64_t originY, float64_t originZ );
	void         DoWorldProjection( int uniformAddress, float32_t * matrix, float64_t anchorX, float64_t anchorY, float64_t width, float64_t height, float64_t perspectiveCameraAngle );
	void         DoWorldNormalizer( int uniformAddress, float32_t * matrix, float64_t width, float64_t height, float64_t perspectiveCameraAngle );
	void         DoCarrierTransformation( int uniformAddress, float32_t * matrix, float64_t rot, float64_t xscale, float64_t yscale, float64_t xshift, float64_t yshift, float64_t xorigin, float64_t yorigin );
	const char * GetOpenGLVersion( void );
	const char * GetGLSLVersion( void );
	uint32_t     CompileShader( uint32_t type, std::string modes, ... );
	uint32_t     InitShading( uint32_t width, uint32_t height, std::string glslDirectory="", std::string substitutions="" );
	int32_t      GetNumberOfTextureSlots( void );
	void         CaptureRawRGBA( int32_t left, int32_t bottom, int32_t width, int32_t height, char * buffer );
	void         SetSwapInterval( int32_t value, bool glfw=true );

	class LinkGL
	{
		public:
			LinkGL( Renderer * renderer, const std::string & name );
			~LinkGL();
		
			const char *   GetName( void ) { return mName.c_str(); }
			Property *     CreateCustomUniform( const std::string & propertyName, unsigned int numberOfElements, bool floatingPoint );
			bool           mRecordable;
		protected:
			Property *     CreateProperty(      const std::string & propertyName, unsigned int numberOfElements, std::string dataType, size_t elementSize, bool isUniform, bool isCustom=false );
		
			PropertyMap    mProperties;
			PropertyMap    mCustomUniformProperties;
			Renderer *     mRenderer;
			std::string    mName;
		
		private:
	};
	
	class Renderer : public LinkGL
	{
		public:
			Renderer( uint32_t program, bool legacy );
			~Renderer();
			int32_t        GetUniformAddress( const std::string & name );
			Property *     Properties( const std::string & query="", bool autoCreate=false, const char * verifyType=NULL );
			Stimulus *     CreateStimulus( const std::string & name );
			RGBTable *     CreateRGBTable( void );
			Stimulus *     GetStimulus( const std::string & name, bool raiseExceptionIfNotFound=true );
			uint32_t       RegisterStimulus( Stimulus * stimulus );
			void           RemoveStimulus( Stimulus * stimulus );
			int32_t        NextAvailableTextureDataID( void );
			int32_t        NextAvailableListNumber( void );
			float64_t      Seconds( void );
			void           WaitForNextFrame( void );
			void           Draw( void );
			void           EnableCulling( double alphaThreshold );
			void           DisableCulling( void );
			void           DisableShadyPipeline( void );
			const char *   GetStimulusOrder( void ) { return mStimulusOrder.c_str(); }
			float64_t      GetWidth( void );
			float64_t      GetHeight( void );
			int32_t        CaptureToTexture( int32_t left, int32_t bottom, int32_t width, int32_t height, int32_t destTextureID );
			
			void           SetUpdateCallback( UpdateCallback func, void *userPtr=NULL );
			void           RecordUpdate( const std::string & objectName, ShaDyLib::Property * property, const char * propertyName=NULL, void * dataPointer=NULL );
			std::string    GetUpdatedKeys( void );
			bool           IsInLegacyMode( void ) { return mLegacy; }
	        int            QueryDACMax( void );
			
	        int            mWaitForFrames;
			
		private:
			
			uint32_t       mProgram;
			bool           mLegacy;	
			uint32_t       mStimulusCounter;
			StimulusMap    mStimuli;
			float64_t      mTimeZero;
			Property *     mFramesCompletedProperty;
			Property *     mSizeProperty;
			UpdateCallback mUpdateCallback;
			void *         mUpdateCallbackArgument;
			std::string    mStimulusOrder;
			PropertyMap    mLastKnown;
			PropertyMap    mChangedThisTime;
			int32_t        mFenceNV;
	};
	
	class Stimulus : public LinkGL
	{
		public:
			Stimulus( Renderer * renderer, const std::string & name );
			~Stimulus();
			Property *     Properties( const std::string & query="", bool autoCreate=false, const char * verifyType=NULL );
			float64_t 	   FreezeZ( void );
			void           Draw( void );
			void           LoadTexture( int32_t width, int32_t height, int32_t nChannels, const std::string & dataType, void * data );
			void           LoadSubTexture( int32_t column, int32_t row, int32_t width, int32_t height, int32_t nChannels, const std::string & dataType, void * data );
			bool           SetLinearMagnification( bool setting );
			void           Enter( void );
			void           Leave( void );
			
			float64_t      mTempZ;
			uint32_t       mSerialNumber;
		
		private:
			bool           mLegacy;	
			RGBTable *     mRGBTable;
			Property *     mDepthPlaneProperty;
			Property *     mEnvelopeOriginProperty;
			Property *     mEnvelopeSizeProperty;
			Property *     mTextureSlotNumberProperty;
			Property *     mTextureChannelsProperty;
			Property *     mTextureIDProperty;
			Property *     mVisibleProperty;
			bool           mLinearMagnification;
			bool           mLeaving;

			void           AllocateModernBuffers( uint32_t nVertices, uint32_t nIndices );
			void           TransferModernBuffers( void );
			void           DrawModernQuad( float64_t width, float64_t height );
			void           DrawModernPoints( int32_t nPoints, const float64_t * points );
			void           DrawModernPolygons( int32_t nPoints, const float64_t * points );
			void           DrawModernLines( int32_t nPoints, const float64_t * points, bool strip, bool loop );
		
			void *         mModernVertexBuffer;
			size_t         mModernVertexBufferSizeAllocated;
			size_t         mModernVertexBufferSizeUsed;
			bool           mModernVertexBufferIncreased;
		
			void *         mModernIndexBuffer;
			size_t         mModernIndexBufferSizeAllocated;
			size_t         mModernIndexBufferSizeUsed;
			bool           mModernIndexBufferIncreased;
		
			int32_t        mModernVBO;
			int32_t        mModernVAO;
			int32_t        mModernEBO;
			float64_t      mModernPreviousQuadWidth;
			float64_t      mModernPreviousQuadHeight;

	};
	
	class RGBTable : public LinkGL
	{
		public:
			RGBTable( Renderer * renderer );
			~RGBTable();
			Property *     Properties( const std::string & query="", bool autoCreate=false, const char * verifyType=NULL );
			void           LoadTexture( int32_t width, int32_t height, int32_t nChannels, const std::string & dataType, void * data );
		
		private:
			Property *     mLookupTableTextureSlotNumberProperty;
			Property *     mLookupTableTextureIDProperty;
	};
	
	class Property
	{
		public:
			Property( const std::string & name, unsigned int numberOfElements, const std::string & dataType, size_t elementSize, int32_t uniformAddress, bool custom=false ) { Init( name, numberOfElements, dataType, elementSize, uniformAddress, custom ); }
			Property( const Property & other, const void * otherDataPointer=NULL );
			~Property();
			
			const char *   GetName( void ) { return mName.c_str(); }
			void *         GetDataPointer( bool deep=false ) { return deep ? mDataStorage : mDataRedirect; }
			uint32_t       GetNumberOfElements( void ) { return mNumberOfElements; }
			uint32_t       GetCurrentlyUsedElements( void ) { return mCurrentlyUsedElements; }
			void *         SetCurrentlyUsedElements( uint32_t n );
			const char *   GetDataType( void ) { return mDataType.c_str(); }
			size_t         GetElementSize( void ) { return mElementSize; }
			int32_t        GetUniformAddress( void ) { return mUniformAddress; }
			bool           IsCustom( void ) { return mCustom; }
			bool           IsEnabled( void ) { return mEnabled; }
			void           Enable( void ) { mEnabled = true; }
			void           Disable( void ) { mEnabled = false; }
			void           LinkWithMaster( Property * master );
			void           CopyValueFrom( const Property * master, const void * masterDataPointer=NULL );
			void *         Redirect( void * addr );
			void           MakeIndependent( bool revertValue=false );
			bool           DataTypeMatches( const char * t );
			void           Transfer( void ); // for "custom" uniform properties only
			bool           Differs( ShaDyLib::Property * other, const void * replaceOwnDataPointer=NULL );
			
		private:
			void Init( const std::string & name, unsigned int numberOfElements, const std::string & dataType, size_t elementSize, int32_t uniformAddress, bool custom );
		
			std::string    mName;
			uint32_t       mNumberOfElements;
			uint32_t       mCurrentlyUsedElements;
			std::string    mDataType;
			size_t         mElementSize;
			bool           mEnabled;
			void *         mDataRedirect;
			void *         mDataStorage;
			int32_t        mUniformAddress;
			bool           mCustom;
	};
	
	class PropArray
	{
		public:
			PropArray( Renderer * renderer, const std::string & propertyName, const std::string & stimulusNames );
			~PropArray() { CleanUp(); }
			void *         GetDataPointer( void ) { return mDataPointer; }
			const char *   GetDataType( void ) { return mDataType.c_str(); }
			size_t         GetNumberOfStimuli( void ) { return mStimuli.size(); }
			size_t         GetNumberOfColumns( void ) { return mNumberOfColumns; }
		private:
			void           CleanUp( void );
		
			void *         mDataPointer;
			std::string    mDataType;
			std::string    mPropertyName;
			uint32_t       mNumberOfColumns;
			size_t         mElementSize;
			StimulusVector mStimuli; 
	};
	
	
	std::string Screens( bool pythonFormat=false );
	
	class Window
	{
		public:
			Window( int width, int height, int left=0, int top=0, int screen=0, bool frame=false, double fullScreenMode=0.0, bool visible=true, int overlayMode=0, int openglContextVersion=0, bool legacy=true, const std::string & glslDirectory="", const std::string & substitutions="" );
			~Window();
			Renderer *	   GetRenderer( void ) { return mRenderer; }
			int            GetWidth( void );
			int            GetHeight( void );
			void           SetVisibility( bool visible, bool force=false );
			void           Run( void );
			void           Display( void );
			void           Close( void );
			void           SetEventCallback( EventCallback func );
			double         GetPixelScaling( void ) { return mPixelScaling; }
			void           SetPixelScaling( double ratio ) { mPixelScaling = ratio; }
		
			void *         mToolboxScreenHandle;
			void *         mToolboxWindowHandle;
			EventCallback  mEventCallback;
			
		private:
			void           Destroy( void );
		
			Renderer *     mRenderer;
			Property *     mVisibleProperty;
			int            mLastKnownWidth;
			int            mLastKnownHeight;
			void *         mNativeWindowHandle;
			bool           mVisible;
			int            mOverlayMode;
			bool           mRunning;
			double         mPixelScaling;
	};
	
} // end namespace ShaDyLib

#endif // INCLUDED_ShaDyLib_HPP
