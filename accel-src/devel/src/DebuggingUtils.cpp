/*
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
*/

#ifndef INCLUDE_DebuggingUtils_CPP
#define INCLUDE_DebuggingUtils_CPP

#include "DebuggingUtils.hpp"

#include "StringUtils.hpp"
#include "ExceptionUtils.hpp"
#include "BasicTypes.hpp"
#include <sstream>
#include <iomanip>

int
DebuggingUtils::Console( const char * msg, int debugLevel )
{
	std::cerr << msg << std::endl;
	return debugLevel;
}

void
DebuggingUtils::PrintArgv( int argc, const char * argv[] )
{
	for( int i = 0; i < argc; i++ ) std::cerr << "argv[" << i << "] = " << StringUtils::StringRepresentation( argv[ i ] ) << "\n";
}

#endif // ifndef INCLUDE_DebuggingUtils_CPP
