/*
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
*/

#ifndef INCLUDE_TimeUtils_CPP
#define INCLUDE_TimeUtils_CPP

#include "TimeUtils.hpp"


#include <cstdlib>

# if __cplusplus >= 201103L
#	include <chrono> // NB: CPLUSPLUS11 - may need -std=c++11
# else
#	include <sys/timeb.h>
# endif

#ifdef _WIN32
#	include <Windows.h>
#else
#	include <sys/select.h> // for select() in SleepSeconds()
#endif

double
TimeUtils::SecondsSinceEpoch( void )
{
# 	if __cplusplus >= 201103L
		std::chrono::time_point< std::chrono::system_clock > t = std::chrono::system_clock::now(); // CPLUSPLUS11
		std::chrono::duration< double > delta = t.time_since_epoch(); // CPLUSPLUS11
		return delta.count();
# 	else
		timeb tb;
		ftime( &tb ); // NB: deprecated, but still supported by all the compilers we use
		double secondsSinceEpoch = ( double )tb.time;
		secondsSinceEpoch += tb.millitm / 1000.0;
		return secondsSinceEpoch;
# 	endif
}

double
TimeUtils::PrecisionTime( bool relativeToFirstCall )
// The only problem with the default relativeToFirstCall=true is that then
// there's no universal time0 agreed-upon between processes (could use shared memory, perhaps.)
{
  static double origin = -1.0;
#if _WIN32 // Begin Windows implementation
  static double timebase = -1.0;
  if( timebase < 0.0 )
  {
    LARGE_INTEGER tb;
    ::QueryPerformanceFrequency( &tb );
    timebase = ( double )tb.QuadPart;
  }
  LARGE_INTEGER counter;
  ::QueryPerformanceCounter( &counter );
  double t = ( double )counter.QuadPart / timebase;
#else      // Begin Posix implementation
# 	if __cplusplus >= 201103L
  static std::chrono::steady_clock::time_point steady0 = std::chrono::steady_clock::now();
  std::chrono::steady_clock::time_point steady = std::chrono::steady_clock::now();
  if( !relativeToFirstCall ) throw "cross-process steady_clock origin not implemented";
  double t = 1e-6 * std::chrono::duration_cast< std::chrono::microseconds >( steady - steady0 ).count();
#	else // C++ not available? (e.g. macOS 10.4 to 10.8)
  double t = SecondsSinceEpoch();
#	endif
#endif     // End OS-specific implementations
  if( origin < 0.0 ) origin = t;
  if( relativeToFirstCall ) t -= origin;
  return t;
}

void
TimeUtils::SleepSeconds( double seconds )
{
	long msec = ( long )( 0.5 + seconds * 1000.0 );
#if _WIN32 // Begin Windows implementation
	::Sleep( msec );
#else      // Begin Posix implementation
	struct timeval t;
	t.tv_sec = msec / 1000L;
	t.tv_usec = ( msec % 1000L ) * 1000L;
	select( 0, 0, 0, 0, &t );
#endif     // End OS-specific implementations
}


#endif // ifndef INCLUDE_TimeUtils_CPP
