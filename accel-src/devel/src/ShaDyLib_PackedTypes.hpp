/*
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
*/

#pragma pack( push, 1 )   // for packed struct alignment

#include "BasicTypes.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////////////

typedef struct
{
	float64_t a;
	float64_t b;
	uint16_t  c;
} TestStruct;
#define PACKING_TEST_STRUCT TestStruct
#define TIGHT sizeof( float64_t ) * 2 + sizeof( uint16_t ) * 1
////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef STATIC_ASSERT   // compile with /DSTATIC_ASSERT or -DSTATIC_ASSERT to perform the following compile-time
                       // size/alignment checks

#undef STATIC_ASSERT
// Macro for "static-assert" (only useful on compile-time constant expressions)
#define STATIC_ASSERT(exp)           STATIC_ASSERT_II(exp, __LINE__)
// Macro used by STATIC_ASSERT macro (don't use directly)
#define STATIC_ASSERT_II(exp, line)  STATIC_ASSERT_III(exp, line)
// Macro used by STATIC_ASSERT macro (don't use directly)
#define STATIC_ASSERT_III(exp, line) enum static_assertion##line{static_assert_line_##line = 1/(int)(exp)}

STATIC_ASSERT( sizeof( int8_t    ) == 1 ); // if you get a compiler error on any of these
STATIC_ASSERT( sizeof( uint8_t   ) == 1 ); // lines (misleadingly, on MSVC, the error might be
STATIC_ASSERT( sizeof( int16_t   ) == 2 ); // just "expected constant expression") then the
STATIC_ASSERT( sizeof( uint16_t  ) == 2 ); // compiler settings need to be changed
STATIC_ASSERT( sizeof( int32_t   ) == 4 );
STATIC_ASSERT( sizeof( uint32_t  ) == 4 );
STATIC_ASSERT( sizeof( float32_t ) == 4 );
STATIC_ASSERT( sizeof( float64_t ) == 8 );
#ifdef PACKING_TEST_STRUCT
STATIC_ASSERT( sizeof( PACKING_TEST_STRUCT ) == TIGHT ); // If you get an error here (misleadingly, on MSVC, the
                                                         // error may be just "expected constant expression"),
                                                         // then you need to assure that the compiler aligns
                                                         // structs on 1-byte boundaries. The MSVC command-line
                                                         // flag is /Zp1 and for g++ it's -fpack-struct=1 but
                                                         // the #pragma pack( push, 1 ) at the top of this file
                                                         // should also have done it.
#endif // PACKING_TEST_STRUCT

// NB: can't think of a static assertion for little-endianity.  Make sure to use the runtime SanityCheck().

#endif // STATIC_ASSERT

////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma pack( pop )
