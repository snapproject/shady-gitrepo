/*
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
*/

#include "ShaDyLib.hpp"
#define SHADYLIB_FUNC( type, name, args, implementation ) type name args;
#include "ShaDyLib.h"

extern "C" int Load_ShaDyLib( const char * name ) { return 0; } // allows you to link statically against this file for testing (NB: must define SHADYLIB_STATIC)

const char * gShaDyLibErrorString = NULL;        // NB: non-thread-safe (!)
ShaDyLib_MessageCallback gShaDyLibErrorHandler = NULL; // NB: non-thread-safe (although less likely to be a problem - will typically be set once globally)

const char * ShaDyLib_OutputString( std::string s ) // A sanctuary for orphaned strings where they can persist in peace---for a while.
{                                             // Note that reliance on these pointers makes client code non-thread-safe.
	const size_t gNumberOfStrings = 32;
	static std::string gStrings[ gNumberOfStrings ];  // non-thread-safe (!)
	static size_t gStringBufferCursor = 0;            // non-thread-safe (!)
	
	std::string & stored = gStrings[ gStringBufferCursor++ ];
	gStringBufferCursor %= gNumberOfStrings;
	stored = s;
	return stored.c_str();
}
void ShaDyLib_HandleError( const std::string & s )
{
	gShaDyLibErrorString = ShaDyLib_OutputString( s ); // non-thread-safe but can be picked up by ShaDyLib_Error() and ShaDyLib_ClearError()
	if( gShaDyLibErrorHandler ) gShaDyLibErrorHandler( s.c_str(), 1 );  // less-unsafe alternative option for handling errors - client code should use ShaDyLib_SetErrorCallback() once globally to register a handler
}

#define TRY( STATEMENTS ) \
	{ \
		try { STATEMENTS ; }

#define CATCH( STATEMENTS ) \
		catch( std::string error ) { ShaDyLib_HandleError( error ); STATEMENTS ; } \
		catch( std::exception const & error ) { ShaDyLib_HandleError( error.what() ); STATEMENTS ; } \
	}
#define RETURN( FALLBACK, ARG ) TRY( return ARG ) CATCH( return FALLBACK )
#define RETURNSTRING( ARG )  RETURN( NULL, ShaDyLib_OutputString( ARG ) )
#define DO( STATEMENTS )  TRY( STATEMENTS ) CATCH( do{ } while( 0 ); )

#define RENDERER   ( ( ShaDyLib::Renderer  * ) r )
#define STIMULUS   ( ( ShaDyLib::Stimulus  * ) s )
#define RGBTABLE   ( ( ShaDyLib::RGBTable  * ) t )
#define PROPERTY   ( ( ShaDyLib::Property  * ) p )
#define WINDOW     ( ( ShaDyLib::Window    * ) w )
#define ARRAY      ( ( ShaDyLib::PropArray * ) a )

#define INPUTSTRING( p )  ( p ? p : "" )

#define SHADYLIB_FUNC( type, name, args, implementation )  type name args implementation
#include "ShaDyLib.h"
