/*
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
*/

#ifndef INCLUDED_ShaDyLib_CPP
#define INCLUDED_ShaDyLib_CPP

#include "FileUtils.hpp"
#include "StringUtils.hpp"
#include "DebuggingUtils.hpp"

#include "ShaDyLib.hpp"
#include "ShaDyLib_Revision.h"

using namespace ShaDyLib;

#include <cmath>
#include <cstring>
#include <cstdarg>

#include <vector>
#include <string>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <iostream>
#include <streambuf>
#include <algorithm>
#include <stdexcept>

#ifndef USE_GLEW
#	define USE_GLEW 1
#endif
#if USE_GLEW
#	include <GL/glew.h>
#	ifdef    _WIN32
#		include <GL/wglew.h>
#	endif // _WIN32
#else  // USE_GLEW
#	include <GL/gl.h>
#endif // USE_GLEW

#ifdef    _WIN32
//#	include <ShellScalingAPI.h> // required for SetProcessDpiAwareness
#endif // _WIN32

#include <GLFW/glfw3.h>


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Private stuff

#define INCREMENT_WRAPPED( X, datatype )   X = ( X + 1 ) % ( 1 << ( 8 * sizeof( datatype ) ) )
#define IS_SANE( x )   ( ( double )( x - x ) == ( double )( x - x ) )  // false for NaNs and Infs

const float64_t PI = 3.141592653589793;
const size_t MAX_POINTS = 20000;  // potentially wastes 320kB per stimulus  (160MB if there are 500 stimuli)

const int32_t DRAWMODE_QUAD       = 1;
const int32_t DRAWMODE_POINTS     = 2;
const int32_t DRAWMODE_LINES      = 3;
const int32_t DRAWMODE_LINE_STRIP = 4;
const int32_t DRAWMODE_POLYGON    = 5;
const int32_t DRAWMODE_LINE_LOOP  = 6;


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Global public functions

void
ShaDyLib::SanityCheck( void )
{
	SANITY_CHECK( PACKING_TEST_STRUCT, TIGHT );
}

std::string
ShaDyLib::GetPlatform( void )
{
	std::string s = STRINGIFY( SHADYLIB_PLATFORM );
	const char * p = s.c_str();
	if( *p == '-' ) p++;
	return p;
}

std::string
ShaDyLib::GetRevision( void )
{
	std::string s = SHADYLIB_REVISION;
	return s.length() ? s.c_str() : "unknown revision";
}

std::string
ShaDyLib::GetCompilationDatestamp( void )
{
	std::string s = SHADYLIB_DATESTAMP;
	return s.length() ? s.c_str() : "unknown date";
}

void
EnableOpenGL( void )
{
	static bool initialized = false;
	
	if( initialized ) return;
#if USE_GLEW
	int result; // NB: glewInit() will fail with the error "Missing GL Version" if an OpenGL context has not been created (creation is a platform-specific operation: OpenGL cannot be said to even exist until it is done;  glutCreateWindow/glfwCreateWindow is one way to achieve context creation)
	if( ( result = glewInit()  ) != GLEW_OK ) RAISE( "glewInit() failed: " << glewGetErrorString( result ) );
#ifdef    _WIN32 // now let's get the wgl* extensions
	if( ( result = wglewInit() ) != GLEW_OK ) RAISE( "Windows-specific wglewInit() failed: " << glewGetErrorString( result ) );
#endif // _WIN32
#endif // USE_GLEW
	
	initialized = true;
}

int32_t
ShaDyLib::GetNumberOfTextureSlots( void )
{
	GLint result;
	SanityCheck();
	EnableOpenGL();
	glGetIntegerv( GL_MAX_TEXTURE_IMAGE_UNITS, &result ); // this returns the number available for fragment-shader use although additional units may be usable by the vertex shader
	return ( int32_t )result;
}

void 
ShaDyLib::CaptureRawRGBA( int32_t left, int32_t bottom, int32_t width, int32_t height, char * buffer )
{
	if( !buffer ) RAISE( "cannot capture pixel values into null buffer" );
	if( width < 1 || height < 1 ) RAISE( "invalid capture dimensions" );
	EnableOpenGL();
	glReadPixels( left, bottom, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

ShaDyLib::LinkGL::LinkGL( ShaDyLib::Renderer * renderer, const std::string & name )
: mRenderer( renderer ), mName( name ), mRecordable( true )
{
	
}

ShaDyLib::LinkGL::~LinkGL()
{
	for( PropertyMap::iterator it = mProperties.begin(); it != mProperties.end(); )
	{
		delete it->second; // TODO: watch out - another property may be redirecting to your mDataStorage (currently not an issue because nothing is ever deleted anyway)
		mProperties.erase( it++ );
	}
}

ShaDyLib::Property *
ShaDyLib::LinkGL::CreateProperty( const std::string & propertyName, unsigned int numberOfElements, std::string dataType, size_t elementSize, bool isUniform, bool custom )
{
	int32_t uniformAddress = -2;
	if( isUniform )
	{
		std::string uniformName = "u";
		uniformName += ::toupper( propertyName[ 0 ] );
		uniformName += propertyName.substr( 1 );
		uniformAddress = mRenderer->GetUniformAddress( uniformName );
		//DEBUG( 0, uniformName << " -> " << uniformAddress );
	}
	dataType = StringUtils::ToLower( dataType );
	if( dataType.substr( dataType.length() - 2 ) == "_t" ) dataType = dataType.substr( 0, dataType.length() - 2 );
	Property * p = new Property( propertyName, numberOfElements, dataType, elementSize, uniformAddress, custom );
	mProperties[ propertyName ] = p;
	return p;
}

ShaDyLib::Property *
ShaDyLib::LinkGL::CreateCustomUniform( const std::string & propertyName, unsigned int numberOfElements, bool floatingPoint )
{
	Property * p = NULL;
	try { p = mProperties[ propertyName ]; }
	catch( std::out_of_range ) {}
	if( p && !p->IsCustom() ) RAISE( "cannot create custom property \"" << propertyName << "\" because a property of that name already exists" );
	delete p;
	p = CreateProperty( propertyName, numberOfElements, ( floatingPoint ? "float64" : "int32" ), ( floatingPoint ? sizeof( float64_t ) : sizeof( float32_t ) ), true, true );
	mCustomUniformProperties[ propertyName ] = p;
	return p;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define CFX ( GLclampf )X   // do not add parentheses- these are used with [i] immediately after
#define FX  ( GLfloat  )X   // do not add parentheses- these are used with [i] immediately after
#define XVAL X[0]

#define START_PROPERTIES \
	bool transfer = false, initialize = false, chronicle = false, special, match; \
	int32_t visible = 1; \
	special = query[ 0 ] == '*'; \
	if( special ) \
	{ \
		if(      query == "*TRANSFER*"   ) { transfer   = true; autoCreate = false; } \
		else if( query == "*INITIALIZE*" ) { initialize = true; autoCreate = true;  } \
	}

#define PROPERTY( NAME, N, TYPE, ISUNIFORM, COMMAND, ... ) \
	match = !special && query == ( NAME ); \
	if( special || match ) \
	{ \
		Property * property = NULL; \
		try { property = mProperties.at( NAME ); } \
		catch( std::out_of_range ) \
		{ \
			if( autoCreate ) \
			{ \
				property = CreateProperty( NAME, N, #TYPE, sizeof( TYPE ), ISUNIFORM ); \
				TYPE defaults[ N ] = { __VA_ARGS__ }; \
				::memcpy( property->GetDataPointer(), defaults, N * sizeof( TYPE ) ); \
			} \
		} \
		if( match ) \
		{ \
			if( verifyType && !property ) RAISE( "internal error: pointer to property \"" << NAME << "\" is NULL" ); \
			if( verifyType && !property->DataTypeMatches( verifyType ) ) RAISE( "internal error: property \"" << NAME << "\" is defined as type " << #TYPE << " but was accessed as " << verifyType ); \
			return property; \
		} \
		if( transfer && property && property->IsEnabled() ) \
		{ \
			TYPE * X = ( TYPE * )property->GetDataPointer(); \
			int32_t UADDR = property->GetUniformAddress(); \
			if( visible > 0 ) { COMMAND; } \
			if( mRecordable ) mRenderer->RecordUpdate( mName, property, NAME, X ); \
		} \
	}
// NB: the logic in FINISH_PROPERTIES, below, may look like it's repeated
//     from above, but it isn't - it's used to catch *custom* properties that
//     weren't included in the hard-coded cascade of PROPERTY() macro calls.
#define FINISH_PROPERTIES( CLASSNAME ) \
	if( !special ) \
	{  \
		Property * property = NULL; \
		try { property = mProperties.at( query ); } \
		catch( std::out_of_range ) {} \
		if( !property ) RAISE( #CLASSNAME << " object has no property \"" << query << "\"" ); \
		if( verifyType && !property->DataTypeMatches( verifyType ) ) RAISE( "internal error: " << #CLASSNAME << " property \"" << query << "\" is defined as type " << property->GetDataType() << " but was accessed as " << verifyType ); \
		return property; \
	} \
	if( transfer ) \
	{ \
		for( ShaDyLib::PropertyMap::iterator it = mCustomUniformProperties.begin(); it != mCustomUniformProperties.end(); it++ ) \
		{ \
			if( visible > 0 ) it->second->Transfer(); \
			if( mRecordable ) mRenderer->RecordUpdate( mName, it->second ); \
		} \
	}

#define ACCESS_PROPERTY_POINTER( TYPE, PROPNAME, PROP_POINTER_MEMBER ) TYPE * PROPNAME =  ( TYPE * )( PROP_POINTER_MEMBER ? PROP_POINTER_MEMBER : ( PROP_POINTER_MEMBER = Properties( #PROPNAME, true, #TYPE ) ) )->GetDataPointer()
#define ACCESS_PROPERTY_SCALAR(  TYPE, PROPNAME, PROP_POINTER_MEMBER ) TYPE & PROPNAME = *( TYPE * )( PROP_POINTER_MEMBER ? PROP_POINTER_MEMBER : ( PROP_POINTER_MEMBER = Properties( #PROPNAME, true, #TYPE ) ) )->GetDataPointer()

#define SLOW_SET_PROPERTY_VALUE( PARENTOBJ, PROPNAME, INDEX, TYPE, VALUE ) ( ( ( TYPE * )( (PARENTOBJ)->Properties( PROPNAME, true, #TYPE ) ) )[ INDEX ] = ( TYPE )( VALUE ) )

void DoNothing( void )
{
	// well, what did you expect to find here?
}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ShaDyLib::Renderer::Renderer( uint32_t program, bool legacy )
: LinkGL( this, "/" ), mProgram( program ), mStimulusCounter( 0 ),
  mFramesCompletedProperty( NULL ), mSizeProperty( NULL ), mUpdateCallback( NULL ), mUpdateCallbackArgument( NULL ), mTimeZero( -1.0 ),
  mLegacy( legacy ), mFenceNV( 0 ), mWaitForFrames( -1 )
{
	SanityCheck();
	EnableOpenGL(); // this assumes an OpenGL context has been created (a platform-specific operation, wrapped e.g. by GLUT or GLFW)
	if( !mProgram ) RAISE( "cannot create a Renderer without a shader program number" );
	Properties( "*INITIALIZE*" );
	
	ShaDyLib::Property * tmpProp = NULL;
	ACCESS_PROPERTY_SCALAR( float64_t, ditheringDenominator, tmpProp );
	ditheringDenominator = ( double )QueryDACMax();
	//SLOW_SET_PROPERTY_VALUE( this, "ditheringDenominator", 0, float64_t, QueryDACMax() );  // seems to cause a segfault - not sure why
}

ShaDyLib::Renderer::~Renderer()
{
	for( PropertyMap::iterator it = mLastKnown.begin(); it != mLastKnown.end(); )
	{
		delete it->second;
		mLastKnown.erase( it++ );
	}
	mChangedThisTime.clear();  // whereas mLastKnown contains allocated Property objects, mChangedThisTime is a *shallow* copy containing a subset of the same pointers
}

int
ShaDyLib::Renderer::QueryDACMax( void )
{
	GLint redBits;
	if( mLegacy )
	{
		// old way (compatibility profiles):
		glGetIntegerv( GL_RED_BITS, &redBits );	
	}
	else
	{
		// MODERNGLTODO here's a new way for OpenGL 3+, based on https://stackoverflow.com/a/15137195/ but may be buggy with some drivers
		glBindFramebuffer( GL_FRAMEBUFFER, 0 );
		glGetFramebufferAttachmentParameteriv( GL_FRAMEBUFFER, GL_FRONT_LEFT, GL_FRAMEBUFFER_ATTACHMENT_RED_SIZE, &redBits);
	}
	return ( 1 << redBits ) - 1;
}

int32_t
ShaDyLib::Renderer::GetUniformAddress( const std::string & name )
{
	return glGetUniformLocation( mProgram, name.c_str() );
}

ShaDyLib::Stimulus *
ShaDyLib::Renderer::CreateStimulus( const std::string & name )
{
	return new Stimulus( this, name );
}

ShaDyLib::RGBTable *
ShaDyLib::Renderer::CreateRGBTable( void )
{
	return new RGBTable( this );
}

uint32_t 
ShaDyLib::Renderer::RegisterStimulus( ShaDyLib::Stimulus * stimulus )
{
	if( !stimulus ) RAISE( "cannot add a NULL Stimulus pointer to a Renderer" );
	ShaDyLib::Stimulus * existing = NULL;
	try { existing = mStimuli.at( stimulus->GetName() ); }
	catch( std::out_of_range ) {}
	if( existing == stimulus ) return mStimulusCounter;
	else if( existing ) RAISE( "stimulus name \"" << stimulus->GetName() << "\" is already taken" );
	mStimuli[ stimulus->GetName() ] = stimulus;
	return mStimulusCounter++;
}

void
ShaDyLib::Renderer::RemoveStimulus( ShaDyLib::Stimulus * stimulus )
{
	ShaDyLib::Stimulus * existing = NULL;
	try { existing = mStimuli.at( stimulus->GetName() ); }
	catch( std::out_of_range ) {}
	if( !existing ) return;
	if( existing != stimulus ) RAISE( "internal error: cannot remove stimulus due to name collision" );
	for( ShaDyLib::StimulusMap::iterator it = mStimuli.begin(); it != mStimuli.end(); it++ )
		if( it->second == stimulus ) { mStimuli.erase( it ); break; }
}

ShaDyLib::Stimulus *
ShaDyLib::Renderer::GetStimulus( const std::string & name, bool raiseExceptionIfNotFound )
{
	try
	{
		return mStimuli.at( name );
	}
	catch( std::out_of_range )
	{
		if( raiseExceptionIfNotFound ) RAISE( "stimulus \"" << name << "\" not found" );
	}
	return NULL;
}

ShaDyLib::Property *
ShaDyLib::Renderer::Properties( const std::string & query, bool autoCreate, const char * verifyType )
{
	START_PROPERTIES
	float64_t width, height;
	float64_t anchorX, anchorY;
	float64_t cameraAngle = 0.0;
	
	if( transfer ) mChangedThisTime.clear();
	
	PROPERTY( "size",                   2,  int32_t,   false, (width=X[0],height=X[1]),  -1, -1 );
	PROPERTY( "clearColor",             3,  float64_t, false, glClearColor(CFX[0],CFX[1],CFX[2],(GLclampf)0.0),  0.8, 0.4, 0.0 );
	PROPERTY( "anchor",                 2,  float64_t, false, (anchorX=X[0], anchorY=X[1]), 0.f, 0.f);
 	PROPERTY( "perspectiveCameraAngle", 1,  float64_t, false, (cameraAngle=X[0]),  0.0 );
	PROPERTY( "matrixWorldNormalizer",  16, float32_t, true,  DoWorldNormalizer(UADDR,X,width,height,cameraAngle), 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f);
	PROPERTY( "matrixWorldProjection",  16, float32_t, true,  DoWorldProjection(UADDR,X,anchorX,anchorY,width,height,cameraAngle), 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f);
	PROPERTY( "framesCompleted",        1,  int32_t,   true,  glUniform1i(UADDR,X[0]),  0 );
 	PROPERTY( "timeInSeconds",          1,  float64_t, false, DoNothing(),  0.0 );
	PROPERTY( "visible",                1,  int32_t,   false, DoNothing(),  1 );
	
	PROPERTY( "backgroundColor",        3,  float64_t, false, DoNothing(),  0.5, 0.5, 0.5 );
	PROPERTY( "noiseAmplitude",         3,  float64_t, false, DoNothing(),  0.0, 0.0, 0.0 );
	PROPERTY( "gamma",                  3,  float64_t, false, DoNothing(),  1.0, 1.0, 1.0 );
	PROPERTY( "ditheringDenominator",   1,  float64_t, false, DoNothing(),  0.0 );
	PROPERTY( "outOfRangeColor",        3,  float64_t, false, DoNothing(),  1.0, 0.0, 1.0 );
	PROPERTY( "outOfRangeAlpha",        1,  float64_t, false, DoNothing(),  -1.0 );
	PROPERTY( "pixelReinterpretation",  1,  float64_t, true,  glUniform1f(UADDR,FX[0]),  0 );
	PROPERTY( "pixelGrouping",          2,  float64_t, true,  glUniform2f(UADDR,FX[0],FX[1]),  0.0, 0.0 );
	
	PROPERTY(          "lookupTableTextureSize", 4, int32_t,   true,  DoNothing(),  -1, -1, -1, -1 );
	PROPERTY(            "lookupTableTextureID", 1, int32_t,   true,  DoNothing(),  -1 );
	PROPERTY(    "lookupTableTextureSlotNumber", 1, int32_t,   true,  DoNothing(),  -1 );

	
	
	FINISH_PROPERTIES( Renderer );
	return NULL;
}

float64_t
ShaDyLib::Renderer::Seconds( void )
{
	return TimeUtils::PrecisionTime( true );
}

typedef std::pair< std::string, Stimulus * > StimulusMapping;
typedef std::vector< StimulusMapping >       StimulusSequence;

bool StimulusOrder( const StimulusMapping & a, const StimulusMapping & b )
{
	if( a.second->mTempZ != b.second->mTempZ )
		return ( a.second->mTempZ > b.second->mTempZ ); // render a before b if a.z is larger (further away)
	else // tie-breaker
		return ( a.second->mSerialNumber < b.second->mSerialNumber ); // render a before b if a was created earlier
}

void
ShaDyLib::Renderer::WaitForNextFrame( void )
{
	if( mWaitForFrames < 0 || mWaitForFrames == 1 )
	{
		// Attempt 1: glSetFenceNV()  supported by some NVidia drivers
		if( mFenceNV == 0 )
		{
			mFenceNV = -1;
			GLuint f;
			if( glewIsSupported( "GL_NV_fence" ) ) { glGenFencesNV( 1, &f ); mFenceNV = f; }
		}
		if( mFenceNV > 0 )
		{
			glSetFenceNV( mFenceNV, GL_ALL_COMPLETED_NV );
			glFinishFenceNV( mFenceNV );
			return;
		}
	}
	
	if( mWaitForFrames < 0 || mWaitForFrames == 2 )
	{
		GLsync sync = glFenceSync( GL_SYNC_GPU_COMMANDS_COMPLETE, 0 ); // NB: OpenGL 3.2 and up in theory, but seems to succeed even in Mac legacy contexts;  causes a crash on Ubuntu 14 VM however!
		if( sync )
		{
			//glWaitSync( sync, 0, GL_TIMEOUT_IGNORED ); // nope, fails to synchronize---use glClientWaitSync instead
			glClientWaitSync( sync, GL_SYNC_FLUSH_COMMANDS_BIT, 1000000000 ); // flags=0 in the middle causes it to wait the entire timeout
			glDeleteSync( sync );
			return;
		}
	}
	
	if( mWaitForFrames < 0 || mWaitForFrames == 3 )
	{
		// Last-resort fallback for the case that glSetFenceNV() was not supported and glFenceSync() failed
		// Worked in Mac legacy contexts before being superseded by glFenceSync().
		// Note the use of direct-mode drawing commands: will fail (hopefully silently) in strict modern contexts.
		glBegin( GL_POINTS ); 
		glEnd();
		glFlush();
		return;
	}
}

void
ShaDyLib::Renderer::Draw( void )
{
	if( mWaitForFrames ) WaitForNextFrame();
	float64_t callTime = Seconds();
	ACCESS_PROPERTY_SCALAR( int32_t, framesCompleted, mFramesCompletedProperty );
	if( framesCompleted )
	{
		glUseProgram( mProgram );
		Properties( "*TRANSFER*" );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ); // if depth test is enabled, need to | this with GL_DEPTH_BUFFER_BIT;  also figure out correct glDepthFunc and glClearDepth
	}
	else
	{
		glClearColor( 0.0, 0.0, 0.0, 0.0 );
		glClear( GL_COLOR_BUFFER_BIT );
	}
	
	StimulusSequence sequence( mStimuli.begin(), mStimuli.end() );
	for( StimulusSequence::iterator it = sequence.begin(); it != sequence.end(); it++ ) it->second->FreezeZ();
	std::sort( sequence.begin(), sequence.end(), StimulusOrder );
	mStimulusOrder = "";
	for( StimulusSequence::iterator it = sequence.begin(); it != sequence.end(); it++ )
	{
		mStimulusOrder += it->second->GetName();
		mStimulusOrder += " ";
	}
	if( framesCompleted )
	{
		// unfortunately the order needs to be different if the GL_DEPTH_TEST is running
		// this entirely reverses the order, which allows greatest efficiency (a stimulus will not be
		// drawn when there is one already in front of it) but now background stimuli do not show through
		// textures that have transparent pixels
		if( glIsEnabled( GL_DEPTH_TEST ) ) std::reverse( sequence.begin(), sequence.end() );
				
		for( StimulusSequence::iterator it = sequence.begin(); it != sequence.end(); it++ ) it->second->Draw();
		glBindTexture( GL_TEXTURE_2D, 0 ); // required to support foreign stimuli
		glFlush();
	}
	if( mUpdateCallback ) mUpdateCallback( callTime, mUpdateCallbackArgument );
	framesCompleted++;
}

void
ShaDyLib::Renderer::EnableCulling( double alphaThreshold )
{
	glEnable( GL_DEPTH_TEST );
	if( alphaThreshold >= 0.0 )
	{
		glEnable( GL_ALPHA_TEST );
		glAlphaFunc( GL_GREATER, ( GLclampf )alphaThreshold );
	}
	else
	{
		glDisable( GL_ALPHA_TEST );
	}
}

void
ShaDyLib::Renderer::DisableCulling( void )
{
	glDisable( GL_DEPTH_TEST );
	glDisable( GL_ALPHA_TEST );
}

void
ShaDyLib::Renderer::DisableShadyPipeline( void )
{
	// Disable the shader and make it possible for foreign stimuli
	// to use legacy fixed-function-pipeline OpenGL calls.
	
	glUseProgram( 0 ); // this causes an "invalid operation" error
	glGetError();      // ...which we have to clean up here before proceeding
	
	// NB: behaviour may in principle be undefined.
	// However, so far it does appear to have the desired effect of disabling the shader
}

void
ShaDyLib::Renderer::SetUpdateCallback( UpdateCallback func, void *userPtr )
{
	mUpdateCallback = func;
	mUpdateCallbackArgument = userPtr;
}

int32_t
ShaDyLib::Renderer::NextAvailableTextureDataID( void )
{
	EnableOpenGL(); // TODO: presumably also need to establish that there's a context??
	GLuint texture;
	glGenTextures( 1, &texture );
	return ( int32_t )texture;
}

int32_t
ShaDyLib::Renderer::NextAvailableListNumber( void )
{
	EnableOpenGL(); // TODO: presumably also need to establish that there's a context??
	GLuint list = glGenLists( 1 );
	return ( int32_t )list;
}

float64_t
ShaDyLib::Renderer::GetWidth( void )
{
	ACCESS_PROPERTY_POINTER( int32_t, size, mSizeProperty );
	return ( float64_t )size[ 0 ];	
}

float64_t
ShaDyLib::Renderer::GetHeight( void )
{
	ACCESS_PROPERTY_POINTER( int32_t, size, mSizeProperty );
	return ( float64_t )size[ 1 ];	
}

int32_t 
ShaDyLib::Renderer::CaptureToTexture( int32_t left, int32_t bottom, int32_t width, int32_t height, int32_t destTextureID )
{
	if( destTextureID <= 0 ) RAISE( "cannot capture: destination texture ID is invalid" );
	if( width < 1 || height < 1 ) RAISE( "invalid capture dimensions" );
	EnableOpenGL();
	//if( destTextureID <= 0 ) destTextureID = NextAvailableTextureDataID(); // NB: could do this, but remember we also have to initialize textureSlotNumber - best to do that outside
	glBindTexture( GL_TEXTURE_2D, destTextureID );
	glCopyTexImage2D( GL_TEXTURE_2D, 0,   GL_RGBA32F,   left, bottom,   width, height,   0 );
	glBindTexture( GL_TEXTURE_2D, 0 );
	return destTextureID;
}

void
ShaDyLib::Renderer::RecordUpdate( const std::string & objectName, ShaDyLib::Property * property, const char * propertyName, void * dataPointer )
{
	if( !objectName.length() || !property ) return;
	bool isRenderer = ( objectName == "/" );
	if( !propertyName ) propertyName = property->GetName();
	
	if( ::strcmp( propertyName, "carrierTransformation" ) == 0 ) return;
	if( ::strcmp( propertyName, "matrixAnchorTranslation" ) == 0 ) return;
	if( ::strcmp( propertyName, "matrixEnvelopeScaling" ) == 0 ) return;
	if( ::strcmp( propertyName, "matrixEnvelopeRotation" ) == 0 ) return;
	if( ::strcmp( propertyName, "matrixEnvelopeTranslation" ) == 0 ) return;
	
	std::string key = "/";
	if( !isRenderer ) { key += "stimuli/"; key += objectName; key += "/"; }
	key += propertyName;
	ShaDyLib::Property * storedValue = NULL;
	try { storedValue = mLastKnown.at( key ); }
	catch( std::out_of_range ) { storedValue = NULL; }
	if( !dataPointer ) dataPointer = property->GetDataPointer();
	bool hasChanged = property->Differs( storedValue, dataPointer );
	if( !storedValue ) storedValue = mLastKnown[ key ] = new Property( *property, dataPointer );
	else if( hasChanged ) storedValue->CopyValueFrom( property, dataPointer );
	if( hasChanged ) mChangedThisTime[ key ] = storedValue;
	// TODO: record LoadTexture events and their content....
}

std::string
ShaDyLib::Renderer::GetUpdatedKeys( void )
{
	std::stringstream ss;
	for( PropertyMap::iterator it = mChangedThisTime.begin(); it != mChangedThisTime.end(); it++ ) ss << it->first << " ";
	return ss.str();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ShaDyLib::Stimulus::Stimulus( ShaDyLib::Renderer * renderer, const std::string & name )
: LinkGL( renderer, name ),
  mDepthPlaneProperty( NULL ),
  mEnvelopeOriginProperty( NULL ),
  mEnvelopeSizeProperty( NULL ),
  mTextureSlotNumberProperty( NULL ),
  mTextureChannelsProperty( NULL ),
  mTextureIDProperty( NULL ),
  mVisibleProperty( NULL ),
  mLinearMagnification( true ),
  mLeaving( false ),
  mRGBTable( NULL ), mSerialNumber( 0 ),
  mLegacy( renderer->IsInLegacyMode() ),
  mModernVertexBuffer( NULL ), mModernVertexBufferSizeAllocated( 0 ), mModernVertexBufferSizeUsed( 0 ), mModernVertexBufferIncreased( false ),
  mModernIndexBuffer(  NULL ), mModernIndexBufferSizeAllocated(  0 ), mModernIndexBufferSizeUsed(  0 ), mModernIndexBufferIncreased(  false ),
  mModernVBO( 0 ), mModernVAO( 0 ), mModernEBO( 0 ),
  mModernPreviousQuadWidth( 0 ), mModernPreviousQuadHeight( 0 )
{
	//DEBUG(0, mName << ": " << ( mLegacy ? "legacy" : "modern" ) );
	if( !renderer ) RAISE( "cannot create a Stimulus based on a NULL Renderer pointer" );
	Properties( "*INITIALIZE*" );
	// TODO: uniquify name
	mSerialNumber = renderer->RegisterStimulus( this );
}

ShaDyLib::Stimulus::~Stimulus()
{
	if( mModernVertexBuffer ) free( mModernVertexBuffer );
	mModernVertexBuffer = NULL;
	mModernVertexBufferSizeAllocated = mModernVertexBufferSizeUsed = 0;
	
	if( mModernIndexBuffer ) free( mModernIndexBuffer );
	mModernIndexBuffer = NULL;
	mModernIndexBufferSizeAllocated = mModernIndexBufferSizeUsed = 0;
}

void
ShaDyLib::Stimulus::Draw( void )
{
	Properties( "*TRANSFER*" );
}

bool
SelectTexture( int32_t sampler2D_uniformAddress, int32_t textureSlotNumber, int32_t textureID )
{
	// A "texture slot number" is what I'm calling the 0-based index of a texture unit, without the GL_TEXTURE0 offset.
	// The texture slot number is the number that gets passed to the shader as a uniform sampler2D. The shader will
	// then access the texture data that is bound to that unit. Note that a shader can address any texture unit without
	// having to make it "active". However, we *will* call glActiveTexture here to activate the unit, so that the
	// subsequent call to glBindTexture binds our texture data (identified by the textureID property) to the correct
	// texture unit.  If we were to always leave each textureID bound to its original texture unit (as Shady does,
	// at least until it runs out of texture units) then we wouldn't need to do glActiveTexture *or* glBindTexture
	// on each frame for each stimulus  - however, regardless of which/how many texture units Shady uses, we cannot
	// rely on foreign (non-Shady) stimuli to leave the bindings in place.
	if( textureSlotNumber < 0 || textureID < 0 ) return false;
	glUniform1i( sampler2D_uniformAddress, textureSlotNumber ); 
	glActiveTexture( ( int32_t )GL_TEXTURE0 + textureSlotNumber );
	glBindTexture( GL_TEXTURE_2D, textureID );
	return true;
}

ShaDyLib::Property *
ShaDyLib::Stimulus::Properties( const std::string & query, bool autoCreate, const char * verifyType )
{
	START_PROPERTIES
	
	bool validTexture;
	GLfloat z;
	float64_t width, height; 
	float64_t anchorX, anchorY;
	float64_t positionX, positionY, scaleX, scaleY, rotation;
	float64_t originX, originY, originZ;
	float64_t cr;
	float64_t cxscale, cyscale;
	float64_t cxshift, cyshift;
	int32_t textureID = -1, drawMode = -1;
	int32_t nPoints = 0, smoothing = 1;
	GLfloat penThickness = 1.0;
	float64_t * points;
	int32_t visibleNextTime, *visibilityPointer = NULL;
	
	PROPERTY(                         "visible", 1,  int32_t,   false, (
		visible = X[ 0 ],
		visibleNextTime = ( visible < 0 ? 0 : visible ),
		X[ 0 ] = ( mLeaving ? 0 : visibleNextTime ),
		visibilityPointer = X
	),  1 );
	if( transfer && mLeaving )
	{
		mLeaving = false;
		*visibilityPointer = visibleNextTime;
		mRenderer->RemoveStimulus( this );
		return NULL;
	}
	if( !visible ) return NULL;
	
	PROPERTY(                       "textureID", 1,  int32_t,   false, (textureID=X[0]),  -1 );
	PROPERTY(               "textureSlotNumber", 1,  int32_t,   true,  (validTexture=SelectTexture(UADDR,X[0],textureID)),  -1 );
	PROPERTY(                    "envelopeSize", 2,  int32_t,   true,  glUniform2f(UADDR,(GLfloat)(width=X[0]),(GLfloat)(height=X[1])),  200, 200 );
	PROPERTY(                     "textureSize", 2,  int32_t,   true,  glUniform2f(UADDR,FX[0],FX[1]),  -1, -1 );
	PROPERTY(                 "textureChannels", 1,  int32_t,   true,  glUniform1i(UADDR,X[0]),  -1 );
	PROPERTY(                               "z", 1,  float64_t, false, (z=FX[0]),  0.0 );
	PROPERTY(             "envelopeTranslation", 2,  float64_t, false, (positionX=X[0],positionY=X[1]),  0, 0 );
	PROPERTY(                  "envelopeOrigin", 3,  float64_t, false, (originX=X[0],originY=X[1],originZ=X[2]),  0.0, 0.0, 0.0 );
	PROPERTY(                "envelopeRotation", 1,  float64_t, false, (rotation=X[0]),  0.0 );
	PROPERTY(                 "envelopeScaling", 2,  float64_t, false, (scaleX=X[0],scaleY=X[1]),  1.0, 1.0 );
	PROPERTY(                          "anchor", 2,  float64_t, false, (anchorX=X[0],anchorY=X[1]),  0.0, 0.0 );
	PROPERTY(         "matrixAnchorTranslation", 16, float32_t, true,  DoAnchorTranslation(UADDR,X,anchorX,anchorY,-width,-height), 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f);
	//PROPERTY(            "matrixEnvelopeTricks", 16, float32_t, true,  glUniformMatrix4fv(UADDR,1,GL_TRUE,X), 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f);
	PROPERTY(           "matrixEnvelopeScaling", 16, float32_t, true,  DoEnvelopeScaling(UADDR,X,scaleX,scaleY), 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f);
	PROPERTY(          "matrixEnvelopeRotation", 16, float32_t, true,  DoEnvelopeRotation(UADDR,X,rotation), 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f);
	PROPERTY(       "matrixEnvelopeTranslation", 16, float32_t, true,  DoEnvelopeTranslation(UADDR,X,positionX,positionY,z,originX,originY,originZ), 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f);
	PROPERTY(                      "useTexture", 1,  int32_t,   true,  glUniform1i(UADDR,validTexture?(X[0]):0),  1 );
	PROPERTY(                 "carrierRotation", 1,  float64_t, false, (cr=X[0]),  0.0 );
	PROPERTY(                  "carrierScaling", 2,  float64_t, false, (cxscale=X[0],cyscale=X[1]),  1.0, 1.0 );
	PROPERTY(              "carrierTranslation", 2,  float64_t, false, (cxshift=X[0],cyshift=X[1]),  0.0, 0.0 );
	PROPERTY(           "carrierTransformation", 9,  float32_t, true,  DoCarrierTransformation(UADDR,X,cr,cxscale,cyscale,cxshift,cyshift,width/2,height/2),  1.f,0.f,0.f, 0.f,1.f,0.f, 0.f,0.f,1.f );
	PROPERTY(                          "offset", 3,  float64_t, true,  glUniform3f(UADDR,FX[0],FX[1],FX[2]),  0.0, 0.0, 0.0 );
	PROPERTY(              "normalizedContrast", 1,  float64_t, true,  glUniform1f(UADDR,FX[0]),  1.0 );
	PROPERTY(               "plateauProportion", 2,  float64_t, true,  glUniform2f(UADDR,FX[0],FX[1]),  -1.0, -1.0 );
	PROPERTY(                  "signalFunction", 1,  int32_t,   true,  glUniform1i(UADDR,X[0]),  0 );
	PROPERTY(              "modulationFunction", 1,  int32_t,   true,  glUniform1i(UADDR,X[0]),  0 );
	PROPERTY(               "windowingFunction", 1,  int32_t,   true,  glUniform1i(UADDR,X[0]),  1 );
	PROPERTY(             "colorTransformation", 1,  int32_t,   true,  glUniform1i(UADDR,X[0]),  0 );
	PROPERTY(                "signalParameters", 4,  float64_t, true,  glUniform4f(UADDR,FX[0],FX[1],FX[2],FX[3]),  0.0, 0.050, 0.0,  0.0 );
	PROPERTY(            "modulationParameters", 4,  float64_t, true,  glUniform4f(UADDR,FX[0],FX[1],FX[2],FX[3]),  0.0, 0.005, 0.0, 90.0 );
	PROPERTY(                 "backgroundAlpha", 1,  float64_t, true,  glUniform1f(UADDR,FX[0]),  1.0 );
	PROPERTY(                 "backgroundColor", 3,  float64_t, true,  glUniform3f(UADDR,FX[0],FX[1],FX[2] ),  0.5, 0.5, 0.5 );
	PROPERTY(                  "noiseAmplitude", 3,  float64_t, true,  glUniform3f(UADDR,FX[0],FX[1],FX[2]),   0.0, 0.0, 0.0 );
	PROPERTY(                           "gamma", 3,  float64_t, true,  glUniform3f(UADDR,FX[0],FX[1],FX[2]),   1.0,  1.0,  1.0 );
	PROPERTY(            "ditheringDenominator", 1,  float64_t, true,  glUniform1f(UADDR,FX[0]),  255.0 );
	PROPERTY(                           "color", 3,  float64_t, true,  glUniform3f(UADDR,FX[0],FX[1],FX[2]),  -1.0, -1.0, -1.0 );
	PROPERTY(                           "alpha", 1,  float64_t, true,  glUniform1f(UADDR,FX[0]),  1.0 );
	PROPERTY(                 "outOfRangeColor", 3,  float64_t, true,  glUniform3f(UADDR,FX[0],FX[1],FX[2] ),  1.0, 0.0, 1.0 );
	PROPERTY(                 "outOfRangeAlpha", 1,  float64_t, true,  glUniform1f(UADDR,FX[0]),  -1.0 );
	PROPERTY(              "debugDigitsStartXY", 2,  float64_t, true,  glUniform2f(UADDR,FX[0],FX[1]),  -1.0, -1.0 );
	PROPERTY(                  "debugDigitSize", 2,  float64_t, true,  glUniform2f(UADDR,FX[0],FX[1]),  -1.0, -1.0 );
	PROPERTY(                   "debugValueIJK", 3,  float64_t, true,  glUniform3f(UADDR,FX[0],FX[1],FX[2]),  0.0, 0.0, 0.0 );
	PROPERTY(          "lookupTableTextureSize", 4,  int32_t,   true,  glUniform4f(UADDR,FX[0],FX[1],FX[2],FX[3]),  -1, -1, -1, -1 );
	PROPERTY(            "lookupTableTextureID", 1,  int32_t,   false, (textureID=X[0]),  -1 );
	PROPERTY(    "lookupTableTextureSlotNumber", 1,  int32_t,   true,  SelectTexture(UADDR,X[0],textureID), -1 );
	PROPERTY(                    "penThickness", 1,  float64_t, false, (penThickness=FX[0]), 1.0 );
	PROPERTY(                       "smoothing", 1,  int32_t,   false, (smoothing=X[0]), 1 );
	PROPERTY(                         "nPoints", 1,  int32_t,   false, (nPoints=X[0]), 0 );
	PROPERTY(                        "pointsXY", MAX_POINTS*2,float64_t,false,(points=(float64_t*)property->SetCurrentlyUsedElements((uint32_t)nPoints*2)), 0.0 );
	PROPERTY(                        "drawMode", 1,  int32_t,   false, (drawMode=X[0]), 1 ); // defer drawing until after custom uniforms have been transferred in FINISH_PROPERTIES
	
	FINISH_PROPERTIES( Stimulus );
	if( transfer )
	{
		GLenum smoothingType, drawType;
		float64_t pixelOffset = 0.0;
		if(      drawMode == DRAWMODE_POINTS     ) { smoothingType = GL_POINT_SMOOTH;   drawType = GL_POINTS;     glPointSize( penThickness ); pixelOffset = 0.5; }
		else if( drawMode == DRAWMODE_LINES      ) { smoothingType = GL_LINE_SMOOTH;    drawType = GL_LINES;      glLineWidth( penThickness ); pixelOffset = 0.5; }
		else if( drawMode == DRAWMODE_LINE_STRIP ) { smoothingType = GL_LINE_SMOOTH;    drawType = GL_LINE_STRIP; glLineWidth( penThickness ); pixelOffset = 0.5; }
		else if( drawMode == DRAWMODE_LINE_LOOP  ) { smoothingType = GL_LINE_SMOOTH;    drawType = GL_LINE_LOOP;  glLineWidth( penThickness ); pixelOffset = 0.5; }
		else if( drawMode == DRAWMODE_POLYGON    ) { smoothingType = GL_POLYGON_SMOOTH; drawType = GL_POLYGON; smoothing = ( smoothing > 1 );  pixelOffset = 0.0; }
		else if( drawMode == DRAWMODE_QUAD       ) { smoothingType = GL_POLYGON_SMOOTH; drawType = GL_QUADS;   smoothing = ( smoothing > 1 );  pixelOffset = 0.0; }
		else return NULL;
		
		if( smoothing ) { glEnable( smoothingType ); } else { glDisable( smoothingType ); }
		if( mLegacy )
		{
			if( drawMode == DRAWMODE_QUAD )
			{
				glBegin( drawType );
				glVertex3f(              0.0,                 0.0,     0.0 );
				glVertex3f( ( GLfloat )width,                 0.0,     0.0 );
				glVertex3f( ( GLfloat )width,   ( GLfloat )height,     0.0 );
				glVertex3f(              0.0,   ( GLfloat )height,     0.0 );
				glEnd();
			}
			else if( nPoints )
			{
				glBegin( drawType );
				for( int32_t iPoint = 0; iPoint < nPoints; iPoint++, points += 2 )
				{
					if( points[ 0 ] != points[ 0 ] || points[ 1 ] != points[ 1 ] )
					{
						if( iPoint < nPoints - 1 ) { glEnd(); glBegin( drawType ); }
					}
					else glVertex2f( ( GLfloat )( points[ 0 ] + pixelOffset ), ( GLfloat )( points[ 1 ] + pixelOffset ) );
				}
				glEnd();
			}
		}
		else
		{
			if( drawMode == DRAWMODE_QUAD ) DrawModernQuad( width, height );
			else if( drawMode == DRAWMODE_POINTS )  DrawModernPoints( nPoints, points );
			else if( drawMode == DRAWMODE_POLYGON ) DrawModernPolygons( nPoints, points );
			else DrawModernLines( nPoints, points, ( drawMode ==  DRAWMODE_LINE_STRIP || drawMode == DRAWMODE_LINE_LOOP ), ( drawMode == DRAWMODE_LINE_LOOP ) );
		}
	}
	return NULL;
}

void
ShaDyLib::Stimulus::AllocateModernBuffers( uint32_t nVertices, uint32_t nIndices )
{
	size_t desiredBufferSize = ( size_t )nVertices * 3 * sizeof( GLfloat );
	mModernVertexBufferIncreased = false;
	if( !mModernVertexBuffer || mModernVertexBufferSizeAllocated < desiredBufferSize )
	{
		mModernVertexBufferIncreased = true;
		mModernVertexBufferSizeAllocated = desiredBufferSize;
		mModernVertexBuffer = realloc( mModernVertexBuffer, desiredBufferSize );
	}
	mModernVertexBufferSizeUsed = desiredBufferSize;
	
	desiredBufferSize = ( size_t )nIndices * sizeof( GLuint );
	mModernIndexBufferIncreased = false;
	if( !mModernIndexBuffer || mModernIndexBufferSizeAllocated < desiredBufferSize )
	{
		mModernIndexBufferIncreased = true;
		mModernIndexBufferSizeAllocated = desiredBufferSize;
		mModernIndexBuffer = realloc( mModernIndexBuffer, desiredBufferSize );
	}
	mModernIndexBufferSizeUsed = desiredBufferSize;
}

void
ShaDyLib::Stimulus::TransferModernBuffers( void )
{
	GLuint buf;
	if( !mModernVBO ) { glGenBuffers( 1, &buf );      mModernVBO = buf; }
	if( !mModernVAO ) { glGenVertexArrays( 1, &buf ); mModernVAO = buf; }
	if( !mModernEBO ) { glGenBuffers( 1, &buf );      mModernEBO = buf; }
	
	glBindBuffer( GL_ARRAY_BUFFER, ( GLuint )mModernVBO );
	glBindVertexArray( ( GLuint )mModernVAO );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, ( GLuint )mModernEBO );
	if( mModernVertexBufferIncreased )
	{
		glBufferData( GL_ARRAY_BUFFER, mModernVertexBufferSizeUsed, mModernVertexBuffer, GL_STATIC_DRAW ); // MODERNGLTODO: is STATIC_DRAW the best?
		glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, NULL );
		glEnableVertexAttribArray( 0 );
		mModernVertexBufferIncreased = false;
	}
	else
	{
		glBufferSubData( GL_ARRAY_BUFFER, 0, mModernVertexBufferSizeUsed, mModernVertexBuffer );
		glBindVertexArray( ( GLuint )mModernVAO );
	}
	
	if( mModernIndexBufferIncreased )
	{
		glBufferData( GL_ELEMENT_ARRAY_BUFFER, mModernIndexBufferSizeUsed, mModernIndexBuffer, GL_STATIC_DRAW );
		mModernIndexBufferIncreased = false;
	}
	else
	{
		glBufferSubData( GL_ELEMENT_ARRAY_BUFFER, 0, mModernIndexBufferSizeUsed, mModernIndexBuffer );
	}
}


#define IB( i, val ) ((GLuint *)mModernIndexBuffer )[i]=(GLuint )(val)
#define VB( i, val ) ((GLfloat*)mModernVertexBuffer)[i]=(GLfloat)(val)
#define MODERN_INDEX(    i, a )        ( IB((i),   a) )
#define MODERN_LINE(     i, a, b )     ( IB((i)*2, a), IB((i)*2+1, b) )
#define MODERN_TRIANGLE( i, a, b, c )  ( IB((i)*3, a), IB((i)*3+1, b), IB((i)*3+2, c) )
#define MODERN_VERTEX(   i, x, y )     ( VB((i)*3, x), VB((i)*3+1, y), VB((i)*3+2, 0) )

void
ShaDyLib::Stimulus::DrawModernQuad( float64_t width, float64_t height )
{
	if( mModernPreviousQuadWidth != width || mModernPreviousQuadHeight != height )
	{
		mModernPreviousQuadWidth = width;
		mModernPreviousQuadHeight = height;
		AllocateModernBuffers( 4, 6 );
		MODERN_VERTEX( 0,   0.0,   0.0 );
		MODERN_VERTEX( 1,   width, 0.0 );
		MODERN_VERTEX( 2,   width, height );
		MODERN_VERTEX( 3,   0.0,   height );
		MODERN_TRIANGLE( 0,   0, 1, 2 );
		MODERN_TRIANGLE( 1,   0, 2, 3 );
		TransferModernBuffers();
	}
	else
	{
		glBindVertexArray( ( GLuint )mModernVAO );
	}
	glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL );
}

void
ShaDyLib::Stimulus::DrawModernPoints( int32_t nPoints, const float64_t * points )
{
	mModernPreviousQuadWidth = mModernPreviousQuadHeight = 0.0;
	int32_t nVertices = 0, nIndices = 0;
	for( int real = 0; real < 2; real++ )
	{
		if( real ) AllocateModernBuffers( nVertices, nIndices );
		nVertices = nIndices = 0;
		const float64_t * readPoints = points;
		for( int32_t iPoint = 0; iPoint < nPoints; iPoint++ )
		{
			float64_t x = *readPoints++;
			float64_t y = *readPoints++;
			if( x != x || y != y ) continue;
			if( real )
			{
				MODERN_VERTEX( nVertices, x + 0.5, y + 0.5 );
				MODERN_INDEX( nIndices, nVertices );
			}
			nVertices++;
			nIndices++;
		}
	}
	TransferModernBuffers();
	glDrawElements( GL_POINTS, nIndices, GL_UNSIGNED_INT, NULL );
}

void
ShaDyLib::Stimulus::DrawModernPolygons( int32_t nPoints, const float64_t * points )
{
	mModernPreviousQuadWidth = mModernPreviousQuadHeight = 0.0;
	int32_t nVertices = 0, nTriangles = 0;
	for( int real = 0; real < 2; real++ )
	{
		if( real ) AllocateModernBuffers( nVertices, nTriangles * 3 );
		nVertices = nTriangles = 0;
		const float64_t * readPoints = points;
		int32_t root = -1, prev = -1;
		for( int32_t iPoint = 0; iPoint < nPoints; iPoint++ )
		{
			float64_t x = *readPoints++;
			float64_t y = *readPoints++;
			if( x != x || y != y )
			{
				root = prev = -1;
				continue;
			}
			if( root < 0 ) root = nVertices;
			else if( prev < 0 ) prev = nVertices; // yes, DrawModernLines has an if here, whereas DrawModernPolygons has an else if
			else
			{
				if( real ) MODERN_TRIANGLE( nTriangles, root, prev, nVertices );
				prev = nVertices;
				nTriangles++;
			}
			if( real ) MODERN_VERTEX( nVertices, x, y ); // never use ++ in macro argument
			nVertices++;
		}
	}
	TransferModernBuffers();
	glDrawElements( GL_TRIANGLES, nTriangles * 3, GL_UNSIGNED_INT, NULL );
}

void
ShaDyLib::Stimulus::DrawModernLines( int32_t nPoints, const float64_t * points, bool strip, bool loop )
{
	mModernPreviousQuadWidth = mModernPreviousQuadHeight = 0.0;
	int32_t nVertices = 0, nLines = 0;
	for( int real = 0; real < 2; real++ )
	{
		if( real ) AllocateModernBuffers( nVertices, nLines * 2 );
		nVertices = nLines = 0;
		const float64_t * readPoints = points;
		int32_t root = -1, prev = -1;
		for( int32_t iPoint = 0; iPoint < nPoints; iPoint++ )
		{
			float64_t x = *readPoints++;
			float64_t y = *readPoints++;
			if( x != x || y != y )
			{
				if( loop && prev >= 0 && root >= 0 )
				{
					if( real ) MODERN_LINE( nLines, prev, root );
					nLines++;
				}
				root = prev = -1;
				continue;
			}
			if( root < 0 ) root = nVertices;
			if( prev < 0 ) prev = nVertices; // yes, DrawModernLines has an if here, whereas DrawModernPolygons has an else if
			else
			{
				if( real ) MODERN_LINE( nLines, prev, nVertices );
				nLines++;
				prev = ( strip ? nVertices : -1 );
			}
			if( real ) MODERN_VERTEX( nVertices, x + 0.5, y + 0.5 ); // never use ++ in macro argument
			nVertices++;
		}
		if( loop && prev >= 0 && root >= 0 )
		{
			if( real ) MODERN_LINE( nLines, prev, root );
			nLines++;
		}
	}
	TransferModernBuffers();
	glDrawElements( GL_LINES, nLines * 2, GL_UNSIGNED_INT, NULL );
}


void *
ShaDyLib::Property::SetCurrentlyUsedElements( uint32_t n )
{
	if( n > mNumberOfElements ) RAISE( "cannot use " << n << " elements of property \"" << mName << "\": maximum is " << mNumberOfElements );
	mCurrentlyUsedElements = n;
	return GetDataPointer();
}

float64_t
ShaDyLib::Stimulus::FreezeZ( void )
{
	ACCESS_PROPERTY_SCALAR(  float64_t, z, mDepthPlaneProperty );
	ACCESS_PROPERTY_POINTER( float64_t, envelopeOrigin, mEnvelopeOriginProperty );
	mTempZ = z + envelopeOrigin[ 2 ];
	return mTempZ;
}

void
DetermineFormats( int32_t nChannels, const std::string & dataType, GLint & internalFormatGL, GLenum & formatGL, GLenum & dtypeGL )
{
	bool floating;
	if(      StringUtils::Match( "uint8",   dataType, "i" ) ) { floating = false; dtypeGL = GL_UNSIGNED_BYTE; }
	else if( StringUtils::Match( "float32", dataType, "i" ) ) { floating = true;  dtypeGL = GL_FLOAT; }
	else RAISE( "dataType must be \"float32\" or \"uint8\"" );
	if(      nChannels == 1 ) { formatGL = GL_RED;   internalFormatGL = ( floating ? GL_R32F     : GL_R8 ); }
	else if( nChannels == 2 ) { formatGL = GL_RG;    internalFormatGL = ( floating ? GL_RG32F    : GL_RG8 ); }
	else if( nChannels == 3 ) { formatGL = GL_RGB;   internalFormatGL = ( floating ? GL_RGB32F   : GL_RGB8 ); }
	else if( nChannels == 4 ) { formatGL = GL_RGBA;  internalFormatGL = ( floating ? GL_RGBA32F  : GL_RGBA8 ); }
	else RAISE( "nChannels must be 1, 2, 3, or 4" );
}

void
ShaDyLib::Stimulus::LoadTexture( int32_t width, int32_t height, int32_t nChannels, const std::string & dataType, void * data )
{
	ACCESS_PROPERTY_SCALAR(  int32_t, textureID,         mTextureIDProperty );
	ACCESS_PROPERTY_SCALAR(  int32_t, textureSlotNumber, mTextureSlotNumberProperty );
	ACCESS_PROPERTY_SCALAR(  int32_t, textureChannels,   mTextureChannelsProperty );
	GLint internalFormatGL;
	GLenum formatGL, dtypeGL;
	DetermineFormats( nChannels, dataType, internalFormatGL, formatGL, dtypeGL );
	if( textureSlotNumber < 0 ) textureSlotNumber = 0;
	if( textureID < 0 ) textureID = mRenderer->NextAvailableTextureDataID();
	glActiveTexture( GL_TEXTURE0 + textureSlotNumber );
	glBindTexture( GL_TEXTURE_2D, textureID );
	glEnable( GL_TEXTURE_2D );
	glTexImage2D( GL_TEXTURE_2D, 0, internalFormatGL, width, height, 0, formatGL, dtypeGL, data );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mLinearMagnification ? GL_LINEAR : GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_REPEAT );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_REPEAT );
	glBindTexture( GL_TEXTURE_2D, 0 ); // required to support foreign stimuli
}

bool
ShaDyLib::Stimulus::SetLinearMagnification( bool setting )
{
	mLinearMagnification = setting;
	ACCESS_PROPERTY_SCALAR( int32_t, textureID, mTextureIDProperty );
	if( textureID >= 0 )
	{
		glBindTexture( GL_TEXTURE_2D, textureID );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mLinearMagnification ? GL_LINEAR : GL_NEAREST );
		glBindTexture( GL_TEXTURE_2D, 0 );
	}
	return setting;
}

void
ShaDyLib::Stimulus::LoadSubTexture( int32_t column, int32_t row, int32_t width, int32_t height, int32_t nChannels, const std::string & dataType, void * data )
{
	ACCESS_PROPERTY_SCALAR(  int32_t, textureID,         mTextureIDProperty );
	ACCESS_PROPERTY_SCALAR(  int32_t, textureSlotNumber, mTextureSlotNumberProperty );
	ACCESS_PROPERTY_SCALAR(  int32_t, textureChannels,   mTextureChannelsProperty );
	GLint internalFormatGL;
	GLenum formatGL, dtypeGL;
	DetermineFormats( nChannels, dataType, internalFormatGL, formatGL, dtypeGL );
	if( textureSlotNumber < 0  || textureID < 0 ) RAISE( "cannot load a subtexture into a Stimulus that has no texture" );
	if( nChannels != textureChannels ) RAISE( "sub-texture array must have the same number of channels as the original texture (" << textureChannels << ")" );
	glActiveTexture( GL_TEXTURE0 + textureSlotNumber );
	glBindTexture( GL_TEXTURE_2D, textureID );
	glEnable( GL_TEXTURE_2D );
	glTexSubImage2D( GL_TEXTURE_2D, 0, column, row, width, height, formatGL, dtypeGL, data );
	glBindTexture( GL_TEXTURE_2D, 0 ); // required to support foreign stimuli
}

void
ShaDyLib::Stimulus::Enter( void )
{
	if( !mRenderer ) RAISE( "internal error: NULL Renderer pointer" )
	mLeaving = false; // just in case we Enter() while a Leave() is still pending...
	mRenderer->RegisterStimulus( this );
}

void
ShaDyLib::Stimulus::Leave( void )
{
	if( !mRenderer ) RAISE( "internal error: NULL Renderer pointer" )
	if( mRenderer->GetStimulus( GetName(), false ) ) mLeaving = true;	
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ShaDyLib::RGBTable::RGBTable( ShaDyLib::Renderer * renderer )
: LinkGL( renderer, "" ), mLookupTableTextureSlotNumberProperty( NULL ), mLookupTableTextureIDProperty( NULL )
{
	if( !renderer ) RAISE( "cannot create an RGBTable based on a NULL Renderer pointer" );
	Properties( "*INITIALIZE*" );
}

ShaDyLib::RGBTable::~RGBTable()
{
	
}

ShaDyLib::Property *
ShaDyLib::RGBTable::Properties( const std::string & query, bool autoCreate, const char * verifyType )
{
	START_PROPERTIES
	
	PROPERTY(          "lookupTableTextureSize", 4, int32_t,   true,  DoNothing(),  -1, -1, -1, -1 );
	PROPERTY(            "lookupTableTextureID", 1, int32_t,   true,  DoNothing(),  -1 );
	PROPERTY(    "lookupTableTextureSlotNumber", 1, int32_t,   true,  DoNothing(),  -1 );
	FINISH_PROPERTIES( RGBTable );
	return NULL;
	// Currently this method is never called in "*TRANSFER*" mode, and the transfer functions do nothing anyway.
	// The power of this object comes when Stimulus properties lookupTableTextureSize, lookupTableTextureID
	// and lookupTableTextureSlotNumber are redirected to their namesakes here.
}

void
ShaDyLib::RGBTable::LoadTexture( int32_t width, int32_t height, int32_t nChannels, const std::string & dataType, void * data )
{
	ACCESS_PROPERTY_SCALAR(  int32_t, lookupTableTextureID,         mLookupTableTextureIDProperty );
	ACCESS_PROPERTY_SCALAR(  int32_t, lookupTableTextureSlotNumber, mLookupTableTextureSlotNumberProperty );
	
	if( !StringUtils::Match( "uint8", dataType, "i" ) ) RAISE( "look-up table must be in \"uint8\" format" );
	if( nChannels != 3 && nChannels != 4 ) RAISE( "look-up table must have 3 or 4 channels" );
	GLint internalFormatGL;
	GLenum formatGL, dtypeGL;
	DetermineFormats( nChannels, dataType, internalFormatGL, formatGL, dtypeGL );
	if( lookupTableTextureSlotNumber < 0 ) lookupTableTextureSlotNumber = 1;
	if( lookupTableTextureID < 0 ) lookupTableTextureID = mRenderer->NextAvailableTextureDataID();
	glActiveTexture( GL_TEXTURE0 + lookupTableTextureSlotNumber );
	glBindTexture( GL_TEXTURE_2D, lookupTableTextureID );
	glEnable( GL_TEXTURE_2D );
	glTexImage2D( GL_TEXTURE_2D, 0, internalFormatGL, width, height, 0, formatGL, dtypeGL, data );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE );
	glBindTexture( GL_TEXTURE_2D, 0 ); // required to support foreign stimuli
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ShaDyLib::Property::Property( const Property & other, const void * otherDataPointer )
{
	Init(
		other.mName, other.mNumberOfElements, other.mDataType,
		other.mElementSize, other.mUniformAddress, other.mCustom
	);
	CopyValueFrom( &other, otherDataPointer );
}

void
ShaDyLib::Property::Init(
	const std::string & name, unsigned int numberOfElements, const std::string & dataType,
	size_t elementSize, int32_t uniformAddress, bool custom
)
{
	mName = name;
	mCurrentlyUsedElements = mNumberOfElements = numberOfElements;
	mDataType = dataType;
	mElementSize = elementSize;
	mUniformAddress = uniformAddress;
	mCustom = custom;
	mEnabled = true;
	mDataRedirect = mDataStorage = NULL;
	if( mNumberOfElements && mElementSize ) mDataRedirect = mDataStorage = calloc( mNumberOfElements, mElementSize );
}

ShaDyLib::Property::~Property()
{
	if( mDataStorage ) free( mDataStorage );
}

void
ShaDyLib::Property::LinkWithMaster( ShaDyLib::Property * master )
{
	if( !master ) master = this;
	if( master == this ) mDataRedirect = mDataStorage;
	else Redirect( master->mDataRedirect );
}

void *
ShaDyLib::Property::Redirect( void * addr )
{
	void * previous = mDataRedirect;
	mDataRedirect = addr;
	return previous;
}

void
ShaDyLib::Property::CopyValueFrom( const ShaDyLib::Property * master, const void * masterDataPointer )
{
	if( !master ) RAISE( "internal error: cannot copy from NULL property" );
	if( mNumberOfElements != master->mNumberOfElements || mDataType != master->mDataType ) RAISE( "internal error: cannot copy from incompatible property" );
	SetCurrentlyUsedElements( master->mCurrentlyUsedElements );
	if( !masterDataPointer ) masterDataPointer = master->mDataRedirect;
	::memcpy( mDataRedirect, masterDataPointer, mCurrentlyUsedElements * mElementSize );
}

void
ShaDyLib::Property::MakeIndependent( bool revertValue )
{
	if( !revertValue ) ::memcpy( mDataStorage, mDataRedirect, mNumberOfElements * mElementSize );
	mDataRedirect = mDataStorage;
}

bool
ShaDyLib::Property::DataTypeMatches( const char * t )
{
	if( StringUtils::Match( mDataType, t, "i" ) ) return true;
	return ( mDataType + "_t" ) == t;
}

bool
ShaDyLib::Property::Differs( ShaDyLib::Property * other, const void * ownDataPointer )
{
	if( !other ) return true;
	if( mDataType != other->mDataType ) return true;
	if( mCurrentlyUsedElements != other->mCurrentlyUsedElements ) return true;
	if( mCurrentlyUsedElements == 0 ) return false;
	if( !ownDataPointer ) ownDataPointer = GetDataPointer();
	return ( ::memcmp( ownDataPointer, other->GetDataPointer(), mCurrentlyUsedElements * mElementSize ) != 0 );
}

void
ShaDyLib::Property::Transfer( void )
{
	if( !mCustom || mUniformAddress < -1 ) RAISE( "internal error: Transfer() is for custom uniform properties only - ." << mName << " is not a uniform" );
	if( mUniformAddress == -1 ) return; // non-active uniform
	if( mDataType == "float64" )
	{
		float64_t * X = ( float64_t * )GetDataPointer();
		switch( mNumberOfElements )
		{
			case 1: glUniform1f( mUniformAddress, FX[ 0 ] ); return;
			case 2: glUniform2f( mUniformAddress, FX[ 0 ], FX[ 1 ] ); return;
			case 3: glUniform3f( mUniformAddress, FX[ 0 ], FX[ 1 ], FX[ 2 ] ); return;
			case 4: glUniform4f( mUniformAddress, FX[ 0 ], FX[ 1 ], FX[ 2 ], FX[ 3 ] ); return;
		}
	}
	else if( mDataType == "int32" )
	{
		int32_t * X = ( int32_t * )GetDataPointer();
		switch( mNumberOfElements )
		{
			case 1: glUniform1i( mUniformAddress,  X[ 0 ] ); return;                            // NB this uses integer transfer, but...
			case 2: glUniform2f( mUniformAddress, FX[ 0 ], FX[ 1 ] ); return;                   // this uses floating-point transfer of integers, because GLSL 1.2 only has a floating-point vec2 type
			case 3: glUniform3f( mUniformAddress, FX[ 0 ], FX[ 1 ], FX[ 2 ] ); return;          // this uses floating-point transfer of integers, because GLSL 1.2 only has a floating-point vec3 type
			case 4: glUniform4f( mUniformAddress, FX[ 0 ], FX[ 1 ], FX[ 2 ], FX[ 3 ] ); return; // this uses floating-point transfer of integers, because GLSL 1.2 only has a floating-point vec4 type
		}
	}
	else RAISE( "internal error: can only use custom Transfer() for int32 and float64 types - ." << mName << " has type " << mDataType );
	RAISE( "internal error: can only support 1-, 2-, 3- or 4-D uniforms - ." << mName << " has " << mNumberOfElements << " elements" );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ShaDyLib::PropArray::PropArray( ShaDyLib::Renderer * renderer, const std::string & propertyName, const std::string & stimulusNames )
: mDataPointer( NULL ), mNumberOfColumns( 0 ), mElementSize( 0 )
{
	if( renderer == NULL ) RAISE( "cannot create PropArray with NULL Renderer pointer" );
	StringUtils::StringVector names;
	StringUtils::Split( names, stimulusNames );
	mPropertyName = propertyName;
	ShaDyLib::Property * prop = NULL;
	for( StringUtils::StringVector::iterator it = names.begin(); it != names.end(); it++ )
	{
		ShaDyLib::Stimulus * stimulus = renderer->GetStimulus( *it );
		mStimuli.push_back( stimulus );
		if( prop == NULL )
		{
			prop = stimulus->Properties( mPropertyName, true );
			mDataType = prop->GetDataType();
			mNumberOfColumns = prop->GetNumberOfElements();
			mElementSize = prop->GetElementSize();
		}
	}
	if( mNumberOfColumns == 0 || mStimuli.size() == 0 || mElementSize == 0 ) return;
	mDataPointer = calloc( mNumberOfColumns * mStimuli.size(), mElementSize );
	char * ptr = ( char * )mDataPointer;
	for( ShaDyLib::StimulusVector::iterator it = mStimuli.begin(); it != mStimuli.end(); it++ )
	{
		ShaDyLib::Stimulus * stimulus = *it;
		prop = stimulus->Properties( mPropertyName.c_str(), false, mDataType.c_str() );
		::memcpy( ptr, prop->GetDataPointer(), mElementSize * mNumberOfColumns );
		prop->Redirect( ( void * )ptr );
		ptr += mElementSize * mNumberOfColumns;
	}
}

void
ShaDyLib::PropArray::CleanUp( void )
{
	for( ShaDyLib::StimulusVector::iterator it = mStimuli.begin(); it != mStimuli.end(); it++ )
	{
		ShaDyLib::Stimulus * stimulus = *it;
		ShaDyLib::Property * prop = stimulus->Properties( mPropertyName.c_str(), false );
		if( prop ) prop->MakeIndependent( false );
	}
	if( mDataPointer ) free( mDataPointer );
	mDataPointer = NULL;	
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const char *
ShaDyLib::GetOpenGLVersion( void )
{
	EnableOpenGL();
	return ( const char * )glGetString( GL_VERSION );
}

const char *
ShaDyLib::GetGLSLVersion( void )
{
	EnableOpenGL();
	return ( const char * )glGetString( GL_SHADING_LANGUAGE_VERSION );
}

void
ShaDyLib::DoAnchorTranslation( int uniformAddress, float32_t * matrix, float64_t anchorX, float64_t anchorY, float64_t width, float64_t height )
{
	float64_t tx = ::floor( ( 0.5 + 0.5 * anchorX ) * width );
	float64_t ty = ::floor( ( 0.5 + 0.5 * anchorY ) * height);

	matrix[ 0] = (float32_t)1.0; matrix[ 1] = (float32_t)0.0; matrix[ 2] = (float32_t)0.0; matrix[ 3] = (float32_t)tx;
	matrix[ 4] = (float32_t)0.0; matrix[ 5] = (float32_t)1.0; matrix[ 6] = (float32_t)0.0; matrix[ 7] = (float32_t)ty;
	matrix[ 8] = (float32_t)0.0; matrix[ 9] = (float32_t)0.0; matrix[10] = (float32_t)1.0; matrix[11] = (float32_t)0.0;
	matrix[12] = (float32_t)0.0; matrix[13] = (float32_t)0.0; matrix[14] = (float32_t)0.0; matrix[15] = (float32_t)1.0;
	
	glUniformMatrix4fv( uniformAddress, 1, GL_TRUE, matrix );
}

void
ShaDyLib::DoEnvelopeScaling( int uniformAddress, float32_t * matrix, float64_t scaleX, float64_t scaleY )
{
	matrix[ 0] = (float32_t)scaleX; matrix[ 1] = (float32_t)0.0;    matrix[ 2] = (float32_t)0.0; matrix[ 3] = (float32_t)0.0;
	matrix[ 4] = (float32_t)0.0;    matrix[ 5] = (float32_t)scaleY; matrix[ 6] = (float32_t)0.0; matrix[ 7] = (float32_t)0.0;
	matrix[ 8] = (float32_t)0.0;    matrix[ 9] = (float32_t)0.0;    matrix[10] = (float32_t)1.0; matrix[11] = (float32_t)0.0;
	matrix[12] = (float32_t)0.0;    matrix[13] = (float32_t)0.0;    matrix[14] = (float32_t)0.0; matrix[15] = (float32_t)1.0;

	glUniformMatrix4fv( uniformAddress, 1, GL_TRUE, matrix );
}

void
ShaDyLib::DoEnvelopeRotation( int uniformAddress, float32_t * matrix, float64_t rotationZ )
{
	float64_t alpha = rotationZ * PI / 180.0;
	float64_t cosa = ::cos( alpha ), sina = ::sin( alpha );
	matrix[ 0] = (float32_t)(+cosa); matrix[ 1] = (float32_t)(-sina); matrix[ 2] = (float32_t)0.0; matrix[ 3] = (float32_t)0.0;
	matrix[ 4] = (float32_t)(+sina); matrix[ 5] = (float32_t)(+cosa); matrix[ 6] = (float32_t)0.0; matrix[ 7] = (float32_t)0.0;
	matrix[ 8] = (float32_t)0.0;     matrix[ 9] = (float32_t)0.0;     matrix[10] = (float32_t)1.0; matrix[11] = (float32_t)0.0;
	matrix[12] = (float32_t)0.0;     matrix[13] = (float32_t)0.0;     matrix[14] = (float32_t)0.0; matrix[15] = (float32_t)1.0;

	glUniformMatrix4fv( uniformAddress, 1, GL_TRUE, matrix );
}

void
ShaDyLib::DoEnvelopeTranslation( int uniformAddress, float32_t * matrix, float64_t positionX, float64_t positionY, float64_t positionZ, float64_t originX, float64_t originY, float64_t originZ )
{
	matrix[ 0] = (float32_t)1.0; matrix[ 1] = (float32_t)0.0; matrix[ 2] = (float32_t)0.0; matrix[ 3] = (float32_t)(::floor(positionX) + originX);
	matrix[ 4] = (float32_t)0.0; matrix[ 5] = (float32_t)1.0; matrix[ 6] = (float32_t)0.0; matrix[ 7] = (float32_t)(::floor(positionY) + originY);
	matrix[ 8] = (float32_t)0.0; matrix[ 9] = (float32_t)0.0; matrix[10] = (float32_t)1.0; matrix[11] = (float32_t)( positionZ + originZ );
	matrix[12] = (float32_t)0.0; matrix[13] = (float32_t)0.0; matrix[14] = (float32_t)0.0; matrix[15] = (float32_t)1.0;

	glUniformMatrix4fv( uniformAddress, 1, GL_TRUE, matrix );
}

float64_t FrustrumDepth( float64_t width, float64_t perspectiveCameraAngle, float64_t * returnCameraDistance )
{
	// If perspectiveCameraAngle <= 0 that indicates we're dealing with an orthographic projection
	if( perspectiveCameraAngle <= 0.0 )
	{
		if( returnCameraDistance ) *returnCameraDistance = 1.0;
		return 2.0;
	}
	// Otherwise, perspectiveCameraAngle is the angle in degrees subtended by (width) pixels at z==0.
	float64_t cotangent_term = ( perspectiveCameraAngle == 90.0 ) ? 1.0 : ( 1.0 / ::tan( perspectiveCameraAngle * 0.5 * PI / 180.0 ) );
	float64_t q = 0.5 * width * cotangent_term;
	// We originally wanted to establish the convention that depth = width (so we're looking at a room with a square floor plan).
	// However, if depth >= 2*q then the near plane of the frustrum crashes into the camera and things degenerate.
	// (Exactly where this happens depends on the camera angle: at 90 deg, the crash happens exactly when depth == width.)
	// So we'll set the frustrum depth to whatever is smaller: width, or 99% of the limit.
	float64_t depth = 2.0 * q * 0.99;
	if( width < depth ) depth = width;
	if( returnCameraDistance ) *returnCameraDistance = q;
	return depth;
}

void
ShaDyLib::DoWorldProjection( int uniformAddress, float32_t * matrix, float64_t anchorX, float64_t anchorY, float64_t width, float64_t height, float64_t perspectiveCameraAngle )
{
	if( perspectiveCameraAngle < 0 && matrix[ 0 ] != 0.0f ) 
	{
		DoNothing();
		// let perspectiveCameraAngle < 0 be the signal to allow the matrix to be defined directly by the user
	}
	else if( perspectiveCameraAngle > 0.0 )
	{
		float64_t q; // camera position will be at 0,0,-q
		float64_t depth = FrustrumDepth( width, perspectiveCameraAngle, &q );
		// The following perspective projection matrix was computed under one big assumption, i.e.
		// that the camera is exactly level with the middle of the screen, horizontally and vertically.
		// It also has three adjustments relative to the usual formula:  (1) it is unnormalized, so the
		// resulting coordinates are in pixel units from bottom left (the matrixWorldNormalizer separately
		// normalizes things to {-1,+1}^3);  (2) there's a z translation before applying the usual projection,
		// such that z=0 now refers to a plane half-way through the frustrum, at which one pixel will map to
		// one pixel on the screen; (3) there are also x and y translations to account for the world anchor.
		// The math comes from scraps/perspective.py
		matrix[ 0] = (float32_t)q;   matrix[ 1] = (float32_t)0.0; matrix[ 2] = (float32_t)(width/2.0);  matrix[ 3] = (float32_t)::ceil(0.5*q*(1.0+anchorX)*width);
		matrix[ 4] = (float32_t)0.0; matrix[ 5] = (float32_t)q;   matrix[ 6] = (float32_t)(height/2.0); matrix[ 7] = (float32_t)::ceil(0.5*q*(1.0+anchorY)*height);
		matrix[ 8] = (float32_t)0.0; matrix[ 9] = (float32_t)0.0; matrix[10] = (float32_t)q;            matrix[11] = (float32_t)(0.25*depth*depth);
		matrix[12] = (float32_t)0.0; matrix[13] = (float32_t)0.0; matrix[14] = (float32_t)1.0;          matrix[15] = (float32_t)q;
	}
	else // orthographic
	{	
		matrix[ 0] = (float32_t)1.0;         matrix[ 1] = (float32_t)0.0;          matrix[ 2] = (float32_t)0.0; matrix[ 3] = (float32_t)::ceil( 0.5 * width  * ( anchorX + 1.0 ) );
		matrix[ 4] = (float32_t)0.0;         matrix[ 5] = (float32_t)1.0;          matrix[ 6] = (float32_t)0.0; matrix[ 7] = (float32_t)::ceil( 0.5 * height * ( anchorY + 1.0 ) );
		matrix[ 8] = (float32_t)0.0;         matrix[ 9] = (float32_t)0.0;          matrix[10] = (float32_t)1.0; matrix[11] = (float32_t)0.0;
		matrix[12] = (float32_t)0.0;         matrix[13] = (float32_t)0.0;          matrix[14] = (float32_t)0.0; matrix[15] = (float32_t)1.0;
	}
	glUniformMatrix4fv( uniformAddress, 1, GL_TRUE, matrix );
}

void
ShaDyLib::DoWorldNormalizer( int uniformAddress, float32_t * matrix, float64_t width, float64_t height, float64_t perspectiveCameraAngle )
{
	if( perspectiveCameraAngle < 0 && matrix[ 0 ] != 0.0f ) 
	{
		DoNothing();
		// let perspectiveCameraAngle < 0 be the signal to allow the matrix to be defined directly by the user
	}
	else
	{
		float64_t depth = FrustrumDepth( width, perspectiveCameraAngle, NULL );
		matrix[ 0] = (float32_t)(2.0/width); matrix[ 1] = (float32_t)0.0;          matrix[ 2] = (float32_t)0.0;         matrix[ 3] = (float32_t)-1.0;
		matrix[ 4] = (float32_t)0.0;         matrix[ 5] = (float32_t)(2.0/height); matrix[ 6] = (float32_t)0.0;         matrix[ 7] = (float32_t)-1.0;
		matrix[ 8] = (float32_t)0.0;         matrix[ 9] = (float32_t)0.0;          matrix[10] = (float32_t)(2.0/depth); matrix[11] = (float32_t) 0.0;
		matrix[12] = (float32_t)0.0;         matrix[13] = (float32_t)0.0;          matrix[14] = (float32_t)0.0;         matrix[15] = (float32_t) 1.0;
	}	
	glUniformMatrix4fv( uniformAddress, 1, GL_TRUE, matrix );
}

void
ShaDyLib::DoCarrierTransformation( int uniformAddress, float32_t * matrix, float64_t rot, float64_t xscale, float64_t yscale, float64_t xshift, float64_t yshift, float64_t xorigin, float64_t yorigin )
{
	float64_t alpha = -rot * PI / 180.0;
	float64_t cosa = ::cos( alpha ), sina = ::sin( alpha );
	
	xorigin += xshift; yorigin += yshift; // allows carrierTranslation to move the center of carrier rotation/scaling
	xshift = -cosa * xorigin / xscale + sina * yorigin / xscale + xorigin - xshift;
    yshift = -cosa * yorigin / yscale - sina * xorigin / yscale + yorigin - yshift;

	matrix[ 0 ] =  (float32_t)(cosa / xscale); matrix[ 1 ] = (float32_t)(-sina / xscale); matrix[ 2 ] = (float32_t)( xshift );
	matrix[ 3 ] =  (float32_t)(sina / yscale); matrix[ 4 ] = (float32_t)( cosa / yscale); matrix[ 5 ] = (float32_t)( yshift );
	matrix[ 6 ] =  (float32_t)(0.0          ); matrix[ 7 ] = (float32_t)( 0.0          ); matrix[ 8 ] = (float32_t)( 1.0    );
	glUniformMatrix3fv( uniformAddress, 1, GL_TRUE, matrix );
}

GLuint
ShaDyLib::CompileShader( GLuint type, std::string modes, ... )
{
	std::string accumulatedSource;
	std::string substitutions;
	std::string lastFileName;
	va_list vl;
	va_start( vl, modes );
	const char * arg;
	while( true )
	{
		arg = va_arg( vl, const char * );
		if( !arg ) break;
		if( !*arg ) continue;
		if( StringUtils::StartsWith( arg, "//#" ) ) { substitutions += arg; substitutions += "\n"; continue; }
		lastFileName = arg;
		std::ifstream fileStream( arg );
		std::string fileContents( ( std::istreambuf_iterator< char >( fileStream ) ),
		                            std::istreambuf_iterator< char >(   ) );
		if( !fileContents.length() ) RAISE( "failed to read GLSL code from " << arg );
		fileContents += "\n";
		accumulatedSource += fileContents;
	}
	va_end( vl );
	while( substitutions.length() )
	{
		std::string searchTerm = StringUtils::ChompString( substitutions, " \t\r\n" );
		size_t startNext = substitutions.find( "//#" ); // string::npos is a perfectly acceptable outcome
		std::string replacement = substitutions.substr( 0, startNext );
		substitutions = substitutions.substr( replacement.length() );
		StringUtils::Replace( accumulatedSource, searchTerm, replacement + "\n", false );
	}
	while( modes.length() )
	{
		// If you pass modes="ONE TWO", strings "//@ONE " and "//@TWO " will be removed
		// from the source (thereby uncommenting anything that follows them on the line)
		// Possible useful mode names might be MODERN, LEGACY and MIXED.
		std::string mode = StringUtils::ChompString( modes, "+,;| " );
		if( mode.length() ) StringUtils::Replace( accumulatedSource, "//@" + mode + " ", "", true );
	}
	
	//{ std::ofstream dbout( lastFileName + ".accumulated" ); dbout << accumulatedSource; }
	GLuint shader = glCreateShader( type );
	const char * src = accumulatedSource.c_str();
	GLint stringLength = ( GLint )accumulatedSource.length();
	glShaderSource( shader, 1, &src, &stringLength );
	glCompileShader( shader );
	GLint result;
	glGetShaderiv( shader, GL_COMPILE_STATUS, &result );
	if( result != GL_TRUE )
	{
		GLsizei logLength;
		const GLsizei bufferSize = 4096;
		char log[ bufferSize ];
		glGetShaderInfoLog( shader, bufferSize, &logLength, log );
		RAISE( "Shader compilation failed:\n" << log );
	}
	return shader;
}

uint32_t
ShaDyLib::InitShading( uint32_t width, uint32_t height, std::string glslDirectory, std::string substitutions )
{
	SanityCheck();
	EnableOpenGL(); // this assumes an OpenGL context has been created (a platform-specific operation, wrapped e.g. by GLUT or GLFW)
	if( !glslDirectory.length() )
	{
		std::string d = FileUtils::WhereAreYou( NULL, 1 );
		for( int i = 0; d.length() && i < 3; i++ )
		{
			glslDirectory = FileUtils::JoinPath( d, "glsl" );
			if( FileUtils::DirectoryExists( glslDirectory ) ) break;
			glslDirectory = "";
			d = FileUtils::ParentDirectory( d );
		}
	}
	if( !glslDirectory.length() ) RAISE( "could not find glsl directory" );
	std::string glsl = GetGLSLVersion();
	std::string modes;
	if( glsl >= "3.3" ) modes += " MODERN";
	std::string versionHeaderSource  = FileUtils::JoinPath( glslDirectory, "Version.glsl" );
	std::string vertexShaderSource   = FileUtils::JoinPath( glslDirectory, "VertexShader.glsl" );
	std::string randomNumberSource   = FileUtils::JoinPath( glslDirectory, glsl >= "3.3" ? "RandomGLSL330AndUp.glsl" : "Random.glsl" );
	std::string fragmentShaderSource = FileUtils::JoinPath( glslDirectory, "FragmentShader.glsl" );
	GLuint vertexShader   = CompileShader( GL_VERTEX_SHADER,   modes, versionHeaderSource.c_str(), vertexShaderSource.c_str(), substitutions.c_str(), NULL );
	GLuint fragmentShader = CompileShader( GL_FRAGMENT_SHADER, modes, versionHeaderSource.c_str(), randomNumberSource.c_str(), fragmentShaderSource.c_str(), substitutions.c_str(), NULL );
	GLuint program = glCreateProgram();
	glAttachShader( program, vertexShader );
	glAttachShader( program, fragmentShader );
	glLinkProgram( program );
	
	while( glGetError() != GL_NO_ERROR ) {} // TODO: this may lose info about previous errors
	glUseProgram( program );
	if( glGetError() != GL_NO_ERROR )
	{
		GLsizei logLength;
		const GLsizei bufferSize = 1024;
		char log[ 1024 ];
		glGetProgramInfoLog( program, bufferSize, &logLength, log );
		RAISE( "Failed to run GPU program:\n" << log );
	}
	
	// enable alpha blending
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
	//glEnable( GL_DEPTH_TEST ); // disabled by default due to its impact on RGBA textures, but can be turned on with a method call: DepthTesting( true )
	glEnable( GL_BLEND );
	glDisable( GL_PROGRAM_POINT_SIZE );
	glBlendEquation( GL_FUNC_ADD );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	
	glPixelStorei( GL_PACK_ALIGNMENT, 1 );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );
	glPixelStorei( GL_PACK_SKIP_ROWS, 0 );
	glPixelStorei( GL_UNPACK_SKIP_ROWS, 0 );
	glPixelStorei( GL_PACK_SKIP_PIXELS, 0 );
	glPixelStorei( GL_UNPACK_SKIP_PIXELS, 0 );
	// should perhaps first save (and later restore?) these state with glPushClientAttrib(GL_CLIENT_PIXEL_STORE_BIT) and glPopClientAttrib(GL_CLIENT_PIXEL_STORE_BIT)
	// see https://www.opengl.org/archives/resources/features/KilgardTechniques/oglpitfall/
	
	return program;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define GET_SHADYLIB_WINDOW( VARNAME ) ShaDyLib::Window * VARNAME = ( ShaDyLib::Window * ) glfwGetWindowUserPointer( gw )
#define GET_GLFW_WINDOW( VARNAME ) GLFWwindow * VARNAME = ( GLFWwindow * ) mToolboxWindowHandle

#define EP( VAR )  ( _ss << #VAR << "=" << VAR << ", " )
#define EPSTR( VARNAME, VALUE ) ( _ss << #VARNAME << "=" << StringUtils::StringRepresentation( VALUE ) << ", " )
#define EPNSTR( VARNAME, N, VALUE ) ( _ss << #VARNAME << N << "=" << StringUtils::StringRepresentation( VALUE ) << ", " )
#define BEGIN_EVENT \
	GET_SHADYLIB_WINDOW( sw ); \
	if( !sw || !sw->mEventCallback ) return; \
	std::stringstream _ss; \
	float64_t t = TimeUtils::PrecisionTime( true ); EP( t );
#define END_EVENT \
	std::string _s = _ss.str(); \
	sw->mEventCallback( _s.c_str() );
#define EVENT( TYPE )   BEGIN_EVENT; EPSTR( type, TYPE ); END_EVENT;
	

void ErrorCallback( int error, const char * description )
{
	fprintf( stderr, "GLFW Error %d: %s\n ", error, description );
	// TODO  would RAISE work here?  to unwind the stack, it will have to cross the boundary with glfw3.lib twice...
	//       if not, integrate with any existing ShaDyLib message callback system
}

void WindowPosCallback( GLFWwindow * gw, int x, int y )
{
	BEGIN_EVENT;
	EPSTR( type, "window_position" ); EP( x ); EP( y );
	END_EVENT;
}
void WindowSizeCallback( GLFWwindow * gw, int width, int height )
{
	BEGIN_EVENT;
	EPSTR( type, "window_resize" ); EP( width ); EP( height );
	END_EVENT;

}
void WindowCloseCallback( GLFWwindow * gw ) { EVENT( "window_close" ); }
void WindowRefreshCallback( GLFWwindow * gw ) { EVENT( "window_refresh" ); }
void WindowFocusCallback( GLFWwindow * gw, int focused ) { EVENT( focused ? "window_focus" : "window_unfocus" ); }
void WindowIconifyCallback( GLFWwindow * gw, int iconified ) { EVENT( iconified ? "window_iconify" : "window_deiconify" ); }
void FramebufferSizeCallback( GLFWwindow * gw, int width, int height )
{
	BEGIN_EVENT;
	EPSTR( type, "framebuffer_resize" ); EP( width ); EP( height );
	END_EVENT;
}

void RescaleCoordinates( GLFWwindow *gw, double knownScalingFactor, double * x, double * y)
{
	if( knownScalingFactor > 0.0 )
	{
		if( x ) *x *= knownScalingFactor;
		if( y ) *y *= knownScalingFactor;
	}
	else
	{
		int fbWidth, fbHeight, winWidth, winHeight;
		glfwGetFramebufferSize( gw, &fbWidth, &fbHeight );
		glfwGetWindowSize( gw, &winWidth, &winHeight );
		if( x && fbWidth  && winWidth  ) *x *= fbWidth  / ( double )winWidth;
		if( y && fbHeight && winHeight ) *y *= fbHeight / ( double )winHeight;
	}
}

void MouseButtonCallback( GLFWwindow * gw, int button, int action, int modifiers )
{
	BEGIN_EVENT;
	if(      action == GLFW_PRESS ) EPSTR( type, "mouse_press" );
	else if( action == GLFW_RELEASE ) EPSTR( type, "mouse_release" );
	else EPSTR( type, "mouse_unknown" );
	double x, y;
	glfwGetCursorPos( gw, &x, &y );
	RescaleCoordinates( gw, sw->GetPixelScaling(), &x, &y );
	EP( x ); EP( y );
#define M( CODENAME )   else if( button == GLFW_MOUSE_BUTTON_ ## CODENAME ) EPSTR( button, StringUtils::ToLower( #CODENAME ) )
#define MN(  NUMBER )   else if( button == GLFW_MOUSE_BUTTON_ ## NUMBER   ) EPSTR( button, "button" #NUMBER )
	if(0){} M(LEFT);M(RIGHT);M(MIDDLE);MN(4);MN(5);MN(6);MN(7);MN(8);
#undef MN
#undef M
	std::string modstr;
#define MODS( CODENAME1, CODENAME2, STR )   if( glfwGetKey( gw, GLFW_KEY_ ## CODENAME1 ) == GLFW_PRESS || glfwGetKey( gw, GLFW_KEY_ ## CODENAME2 ) == GLFW_PRESS ) modstr += ( modstr.length() ? ( " " STR ) : STR );
	MODS(LEFT_SHIFT,RIGHT_SHIFT,"shift");MODS(LEFT_CONTROL,RIGHT_CONTROL,"ctrl");MODS(LEFT_ALT,RIGHT_ALT,"alt");MODS(LEFT_SUPER,RIGHT_SUPER,"super");
#undef MODS
	EPSTR( modifiers, modstr );
	END_EVENT;	
}
void CursorPosCallback( GLFWwindow * gw, double x, double y )
{
	BEGIN_EVENT;
	EPSTR( type, "mouse_motion" );
	RescaleCoordinates( gw, sw->GetPixelScaling(), &x, &y );
	EP( x ); EP( y ); // TODO: could also compute dx and dy and return them here - currently doing it in the Python wrapper
	std::string button;
#define M( CODENAME )   if( glfwGetMouseButton( gw, GLFW_MOUSE_BUTTON_ ## CODENAME ) == GLFW_PRESS ) button += StringUtils::ToLower( button.length() ? ( " " #CODENAME ) : #CODENAME );
#define MN(  NUMBER )   if( glfwGetMouseButton( gw, GLFW_MOUSE_BUTTON_ ## NUMBER   ) == GLFW_PRESS ) button += ( button.length() ? ( " button" #NUMBER ) : ( "button" #NUMBER ) );
	M(LEFT);M(RIGHT);M(MIDDLE);MN(4);MN(5);MN(6);MN(7);MN(8);
#undef MN
#undef M
	EPSTR( button, button );
	std::string modstr;
#define MODS( CODENAME1, CODENAME2, STR )   if( glfwGetKey( gw, GLFW_KEY_ ## CODENAME1 ) == GLFW_PRESS || glfwGetKey( gw, GLFW_KEY_ ## CODENAME2 ) == GLFW_PRESS ) modstr += ( modstr.length() ? ( " " STR ) : STR );
	MODS(LEFT_SHIFT,RIGHT_SHIFT,"shift");MODS(LEFT_CONTROL,RIGHT_CONTROL,"ctrl");MODS(LEFT_ALT,RIGHT_ALT,"alt");MODS(LEFT_SUPER,RIGHT_SUPER,"super");
#undef MODS
	EPSTR( modifiers, modstr );
	END_EVENT;
}
void CursorEnterCallback( GLFWwindow * gw, int entered ) { EVENT( entered ? "mouse_enter" : "mouse_leave" ); }
void ScrollCallback( GLFWwindow * gw, double dx, double dy )
{
	dx *= -1; dy *= -1; // TODO: it would be nice to ensure that we record physical direction of scroll wheel movement rather than intended scroll direction, but the mapping is presumably under control of system preferences...
	BEGIN_EVENT;
	EPSTR( type, "mouse_scroll" );
	EP( dx );
	EP( dy );
	END_EVENT;
}
void KeyCallback( GLFWwindow * gw, int key, int scancode, int action, int modifiers )
{
	BEGIN_EVENT;
	if(      action == GLFW_PRESS   ) EPSTR( type, "key_press" );
	else if( action == GLFW_RELEASE ) EPSTR( type, "key_release" );
	else if( action == GLFW_REPEAT  ) EPSTR( type, "key_auto" );
	else EPSTR( type, "key_unknown" );
	// In earlier revisions we would discard 'key_auto' events (the thinking being that for keys that generate
	// characters, repeated 'text' events would be made anyway via the CharCallback; and for modifier keys, who
	// wants to see a deluge of 'key_auto' events anyway). 	This is now re-enabled---sometimes it's useful to be
	// able to hold down an arrow key or return key, and that would generate a 'key_auto' but not a 'text' event.
	// 
	// Note that other back-ends (pyglet, pygame) don't seem to support 'key_auto'-like events. It would be nice
	// to rely on repeated 'text' events instead there, but there's a bug (seemingly common to pyglet and the
	// accelerator, so maybe OS- or keyboard- specific on my MacBook??) whereby only a few keys generate repeated
	// 'text' events and the rest do not, although they all seem to generate 'key_auto' events.  Out of the
	// alphanumeric keys, ['a', 'c', 'e', 'i', 'l', 'n', 'o', 's', 'u', 'y', 'z'] seem to generate repeated 'text'
	// events and the rest do not.	
	bool keypad = false;
	const char * keyName = glfwGetKeyName( key, scancode );
#define K(   CODENAME )       else if( key == GLFW_KEY_ ## CODENAME ) EPSTR( key, StringUtils::ToLower( #CODENAME ) )
#define KS(  CODENAME, STR )  else if( key == GLFW_KEY_ ## CODENAME ) EPSTR( key, STR )
#define KP(  CODENAME )       else if( key == GLFW_KEY_KP_ ## CODENAME ) ( EPSTR( key, StringUtils::ToLower( #CODENAME ) ), keypad=true )
#define KPS( CODENAME, STR )  else if( key == GLFW_KEY_KP_ ## CODENAME ) ( EPSTR( key, STR ), keypad=true )
	if(0){}
	KS(SPACE," ");KS(APOSTROPHE,"'");KS(COMMA,",");KS(MINUS,"-");KS(PERIOD,".");KS(SLASH,"/");
	KS(SEMICOLON,";");KS(EQUAL,"=");
	KS(LEFT_BRACKET,"[");KS(BACKSLASH,"\\");KS(RIGHT_BRACKET,"]");KS(GRAVE_ACCENT,"`");KS(WORLD_1,"world1");KS(WORLD_2,"world2");
	K(ESCAPE);K(ENTER);K(TAB);K(BACKSPACE);K(INSERT);K(DELETE);K(RIGHT);K(LEFT);K(DOWN);K(UP);
	KS(PAGE_UP,"pageup");KS(PAGE_DOWN,"pagedown");K(HOME);K(END);
	KS(CAPS_LOCK,"capslock");KS(SCROLL_LOCK,"scrolllock");KS(NUM_LOCK,"numlock");KS(PRINT_SCREEN,"printscreen");KS(PAUSE,"pause");
	K(F1);K(F2);K(F3);K(F4);K(F5);K(F6);K(F7);K(F8);K(F9);K(F10);K(F11);K(F12);
	K(F13);K(F14);K(F15);K(F16);K(F17);K(F18);K(F19);K(F20);K(F21);K(F22);K(F23);K(F24);K(F25);
	KP(0);KP(1);KP(2);KP(3);KP(4);KP(5);KP(6);KP(7);KP(8);KP(9);
	KPS(DECIMAL,".");KPS(DIVIDE,"/");KPS(MULTIPLY,"*");KPS(SUBTRACT,"-");KPS(ADD,"+");KPS(ENTER,"enter");KPS(EQUAL,"=");
	KS(LEFT_SHIFT,"lshift");KS(LEFT_CONTROL,"lctrl");KS(LEFT_ALT,"lalt");KS(LEFT_SUPER,"lsuper");
	KS(RIGHT_SHIFT,"rshift");KS(RIGHT_CONTROL,"rctrl");KS(RIGHT_ALT,"ralt");KS(RIGHT_SUPER,"rsuper");
	K(MENU);
	else if( keyName ) EPSTR( key, StringUtils::ToLower( keyName ) );
	else { EP( key ); EP( scancode ); }
#undef KPS
#undef KP
#undef KS
#undef K
	std::string modstr = keypad ? "num" : "";
#define MODS( CODENAME, STR )   if( ( modifiers & GLFW_MOD_ ## CODENAME ) != 0 ) modstr += ( modstr.length() ? ( " " STR ) : STR );
	MODS(SHIFT,"shift");MODS(CONTROL,"ctrl");MODS(ALT,"alt");MODS(SUPER,"super");
#undef MODS
	EPSTR( modifiers, modstr );
	END_EVENT;
}
void CharCallback( GLFWwindow * gw, unsigned int text )
{
	BEGIN_EVENT;
	EPSTR( type, "text" );
	EP( text ); // convert with unichr() (Python 2) or chr() (Python 3)
	std::string modstr;
#define MODS( CODENAME1, CODENAME2, STR )   if( glfwGetKey( gw, GLFW_KEY_ ## CODENAME1 ) == GLFW_PRESS || glfwGetKey( gw, GLFW_KEY_ ## CODENAME2 ) == GLFW_PRESS ) modstr += ( modstr.length() ? ( " " STR ) : STR );
	MODS(LEFT_SHIFT,RIGHT_SHIFT,"shift");MODS(LEFT_CONTROL,RIGHT_CONTROL,"ctrl");MODS(LEFT_ALT,RIGHT_ALT,"alt");MODS(LEFT_SUPER,RIGHT_SUPER,"super");
#undef MODS
	EPSTR( modifiers, modstr );
	END_EVENT;
}
void DropCallback( GLFWwindow * gw, int count, const char ** paths )
{
	BEGIN_EVENT;
	EPSTR( type, "file_drop" );
	_ss << "paths=[";
	for( int i = 0; i < count; i++ ) _ss << ( i ? "," : "" ) << StringUtils::StringRepresentation( paths[ i ] );
	_ss << "], ";
	END_EVENT;
}
void WindowMaximizeCallback( GLFWwindow * gw, int maximized ) { EVENT( maximized ? "window_maximize" : "window_restore" ); }

void
ShaDyLib::SetSwapInterval( int32_t value, bool glfw )
{
	// NB: Normally we just rely on glfwSwapInterval(1) during Window() construction
	//     However it might become necessary to throttle the frame rate down to 1/2 or 1/3
	//     There's also the issue of negative values...
	if( glfw ) glfwSwapInterval( value );
	else
	{
		// NB: Normally we would just rely on glfwSwapInterval.
		//     This option is an experimental way of helping *other* windowing systems (e.g. pygame) synch with VBLs
		//     It's of limited use because it's now hard to imagine using pygame non-accelerated windows with accelerated rendering,
		//     and so far, attempts at creating an equivalent non-accelerated helper via PyOpenGL have failed....
#		ifdef    _WIN32
			wglSwapIntervalEXT( value ); // for this, need to #include <GL/wglew.h> and call wglewInit();  TODO: issue warning/error if unsupported or if failed
#		else  // _WIN32
			// how to ensure VBL-locked timing on non-Windows systems?
#		endif // _WIN32
	}
}

std::string
ShaDyLib::Screens( bool pythonFormat )
{
	SanityCheck();
	glfwSetErrorCallback( ErrorCallback );
	glfwInit();
	int numberOfScreens;
	GLFWmonitor * primaryScreen = glfwGetPrimaryMonitor();
	GLFWmonitor ** screens = glfwGetMonitors( &numberOfScreens );
	std::stringstream ss;
	if( pythonFormat ) ss << "[";
	for( int i = 0; i < numberOfScreens; i++ )
	{
		GLFWmonitor * eachScreen = screens[ i ];
		const GLFWvidmode * eachMode = glfwGetVideoMode( eachScreen );
		int eachX, eachY; glfwGetMonitorPos( eachScreen, &eachX, &eachY );
		std::string eachName = StringUtils::StringRepresentation( glfwGetMonitorName( eachScreen ) );
		if( pythonFormat ) ss << ( i ? "," : "" ) << "\n    "
			//<< ( i + 1 ) << " : " // it would be nice to have numbers as keys in a Python dict but this breaks JSON compatibility of the string
			<< "{ "
			<<     "\"number\":" << ( i + 1 )
			<<     ", \"name\":" << eachName
			<<   ",  \"width\":" << eachMode->width
			<<   ", \"height\":" << eachMode->height
			<<    ",  \"left\":" << eachX
			<<      ", \"top\":" << eachY
			<<      ",  \"hz\":" << eachMode->refreshRate
			<< ",  \"primary\":" << ( eachScreen == primaryScreen )
			<< " }";
		else ss << ( i ? "\n" : "" )
			<< "    " << ( i + 1 ) << ":\t "
			<< eachName << "\t  "
			<< eachMode->width << "\t "
			<< "x\t "
			<< eachMode->height << "\t "
			<< " @\t "
			<< "(\t"
			<< eachX << ",\t"
			<< eachY << ")\t "
			<< "-\t "
			<< eachMode->refreshRate << "Hz"
		;
	}
	if( pythonFormat ) return ss.str() + "\n]";
	else return StringUtils::Tabulate( ss.str(), "rlrcrccrrcr" );
}

int GetScreenLeft( GLFWmonitor * gm ) { int left; glfwGetMonitorPos( gm, &left, NULL ); return left; }
int GetScreenTop(  GLFWmonitor * gm ) { int top;  glfwGetMonitorPos( gm,  NULL, &top ); return top;  }

ShaDyLib::Window::Window( int width, int height, int left, int top, int screen, bool frame, double fullScreenMode, bool visible, int overlayMode, int openglContextVersion, bool legacy, const std::string & glslDirectory, const std::string & substitutions )
: mRenderer( NULL ), mVisibleProperty( NULL ), mLastKnownWidth( 0 ), mLastKnownHeight( 0 ), mToolboxScreenHandle( NULL ), mToolboxWindowHandle( NULL ),
  mNativeWindowHandle( NULL ), mOverlayMode( overlayMode ), mRunning( false ), mEventCallback( NULL ), mPixelScaling( 0.0 )
{
	SanityCheck();
	
	glfwSetErrorCallback( ErrorCallback );
	glfwInit();
#ifdef    _WIN32
	//::SetProcessDpiAwareness( PROCESS_PER_MONITOR_DPI_AWARE ); // NB: Windows 8.1+ only: load dynamically instead?
	/*
		Okay, the DPI awareness issue. Where your resolution is not really your resolution, except when it is.
		To start with, consider the MacOS case, with retina displays:  these seem to be handled automatically by
		GLFW 3.2.1 provided we feed OpenGL the output of glfwGetFramebufferSize instead of glfwGetWindowSize,
		which we now do. So, while we're stuck with using fake/wacky units for telling the OS to initialize the
		window, we can use real pixel units for OpenGL initialization and everything we subsequently do, and all
		is well.  The one gotcha might be if you read off the (true-pixel) size of a Window/Renderer/Stimulus and
		then naively think that you can use those same numbers to recreate an identical Window later on.
		Unfortunately the same trick doesn't seem to work in Windows.
		
		In Windows, all is well provided we ensure DpiAwareness is set. In Windows 7 and 8.0, that means calling
		SetProcessDPIAware() which resides in user32.dll. In Windows 8.1+ we can load SetProcessDpiAwareness from
		Shcore.dll and set it to 2, i.e. call SetProcessDpiAwareness(PROCESS_PER_MONITOR_DPI_AWARE).   GLFW 3.2.1
		seems to take care of this automatically (choosing which of the two to call, appropriately) during
		initialization - hence we're able to comment out the call above. (NB: from a certain milestone in the
		evolution of Windows 10, Microsoft introduced, and started recommending, DPI awareness manipulation on a
		per-thread basis, which GLFW 3.2.1 doesn't seem to do.) The only thing we have to worry about is prior
		calls: this can happen if somebody has put a .exe.manifest next to your .exe, and globally configured the
		OS to take notice of such files; or (less insanely) if DPI scaling has been manipulated via the .exe file's
		properties dialog.
		
		If awareness happens to have been previously set to 0, and the OS is performing DPI scaling, any further
		attempt by GLFW or by us to change awareness will fail.  Then, unfortunately (and unlike the MacOS situation),
		glfwGetFramebufferSize and glfwGetWindowSize will still both return the same (larger) numbers as each other.
		You can't stop the actual scaling being performed anyway, but since the two functions return the same values
		we cannot even use them to detect the discrepancy (and, say, issue an error or warning, and/or simply commit
		ourselves to working in fake lower-resolution units). So if this happens, there's not much we can do.
	*/
#endif // _WIN32
		
	if( openglContextVersion < 330 )
	{
		// do nothing
	}
	else if( true || openglContextVersion == 330 )
	{
		// This is part of what it takes to switch away from legacy (1.2) GLSL and operate from a base of 3.3+
		int major = openglContextVersion / 100;
		int minor = ( openglContextVersion - 100 * major ) / 10;
		glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, major );
		glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, minor );
		glfwWindowHint( GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE ); // required on OSX - and means we have to *remove* all legacy shader calls
		glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE ); // required on OSX
	}
	else RAISE( "context hints for openglContextVersion=" << openglContextVersion << " have not yet been implemented" );
	
	GLFWmonitor * gm = NULL;
	const GLFWvidmode * mode = NULL;
	
	bool screenPreference = ( screen > 0 );
	if( screenPreference )
	{
		int numberOfScreens;
		GLFWmonitor ** screens = glfwGetMonitors( &numberOfScreens );
		if( screen > numberOfScreens ) RAISE( "Failed to select screen " << screen << ". Available screens are:\n" << ShaDyLib::Screens( false ) );
		gm = screens[ screen - 1 ];
	}
	else gm = glfwGetPrimaryMonitor();
	mToolboxScreenHandle = ( void * )gm;
	const GLFWgammaramp *oldRamp = glfwGetGammaRamp( gm );
	if( oldRamp )
	{
		unsigned short * rampValues = new unsigned short[ oldRamp->size ];
		double denom = ( double )( oldRamp->size - 1 );
		double gamma = 1.0; // parameterized it like this for debugging; really we want to be able to control gamma from properties, so we should set it linear here
		for( unsigned int iRampEntry = 0; iRampEntry < oldRamp->size; iRampEntry++ )
			rampValues[ iRampEntry ] = ( unsigned short )( 0.5 +  65535.0  * ::pow( ( double )iRampEntry / denom, 1.0 / gamma ) );
		GLFWgammaramp newRamp;
		newRamp.size = oldRamp->size;
		newRamp.red = newRamp.green = newRamp.blue = rampValues;
		glfwSetGammaRamp( gm, &newRamp );
		delete [] rampValues;
	}
	if( gm ) mode = glfwGetVideoMode( gm );
	if( mode )
	{
#		ifdef    _WIN32                                             // This is a dirty hack. On Windows, a window that fits its
			int modeWidth = mode->width - ( mOverlayMode ? 1 : 0 ); // screen exactly will go into pseudo-fullscreen mode when
#		else                                                        // foregrounded. This is usually good (it has timing
			int modeWidth = mode->width;                            // performance advantages due to lack of compositing) but in
#		endif                                                       // overlay mode we don't want this, so we shave 1 pixel off.
		if( width  <= 0 ) { width  = modeWidth  ;  left = screenPreference ? 0 : GetScreenLeft( gm ); }
		if( height <= 0 ) { height = mode->height; top  = screenPreference ? 0 : GetScreenTop(  gm ); }
		if( fullScreenMode ) { left = GetScreenLeft( gm ); top = GetScreenTop( gm ); }
		else if( screenPreference ) { left += GetScreenLeft( gm ); top += GetScreenTop( gm ); }
		glfwWindowHint( GLFW_RED_BITS, mode->redBits );
		glfwWindowHint( GLFW_GREEN_BITS, mode->greenBits );
		glfwWindowHint( GLFW_BLUE_BITS, mode->blueBits );
		glfwWindowHint( GLFW_REFRESH_RATE, mode->refreshRate );
		if( !fullScreenMode && ( width != mode->width || height != mode->height ) ) gm = NULL;
	}
	else fullScreenMode = 0.0;
	if( width  <= 0 ) width  = 800;
	if( height <= 0 ) height = 600;
#define WINDOW_TITLE "Shady Stimulus Display"
	if( mOverlayMode ) { fullScreenMode = 0.0; gm = NULL; }
	if( fullScreenMode ) frame = false;
	/*
	NB: fullScreenMode = false is a good default on Windows provided we're using my fork of GLFW with Camilla's "HACK"
	    sections removed. On OSX, fullScreenMode = true is a better default (even though you get the undesirable
	    minimize-on-loss-of-focus behaviour) because it's the only way to get rid of the menu bar.
	    
	    In either case, we still make fullScreenMode an option, because of the following (see https://github.com/glfw/glfw/issues/1144 ):
		- On Intel graphics cards under Windows, gm=NULL (fullScreenMode=false) causes tearing artefacts unless you
		  remove both "HACK" sections of wgl_context.c in glfw 3.2.1 sources. For now, we're adopting the approach
		  of patching GLFW to remove these HACKs, so fullScreenMode=false is a good default, but...
		- on NVidia graphics cards under Windows, the HACK sections are allegedly needed, it could be (haven't tested
		  yet) that selecting fullScreenMode=true (and hence gm!=NULL) might be the best option. However, then...
		- unfortunately on both Windows and MacOS, with gm != NULL you have to choose one of the following two
		  undesirable behaviours:
			-- glfwWindowHint( GLFW_AUTO_ICONIFY, GLFW_TRUE ); // (default) fullscreen-mode window gets iconified and hence disappears (on MacOS, slowly) whenever it loses focus
			-- glfwWindowHint( GLFW_AUTO_ICONIFY, GLFW_FALSE ); //          fullscreen-mode window refuses to go into the background - even if you also glfwWindowHint( GLFW_FLOATING, GLFW_FALSE );
	*/ 
	glfwWindowHint( GLFW_DECORATED, frame ? GLFW_TRUE : GLFW_FALSE );
	
	if( mOverlayMode )
	{
		glfwWindowHint( GLFW_FLOATING,                GLFW_TRUE );
		glfwWindowHint( GLFW_TRANSPARENT_FRAMEBUFFER, GLFW_TRUE );
		glfwWindowHint( GLFW_MOUSE_PASSTHROUGH,       GLFW_TRUE );
		glfwWindowHint( GLFW_FOCUSED,                 GLFW_FALSE );
		glfwWindowHint( GLFW_FOCUS_ON_SHOW,           GLFW_FALSE );
	}
	GLFWwindow * gw = glfwCreateWindow( width, height, WINDOW_TITLE, gm, NULL ); // this creates the OpenGL context and, if gm is non-NULL, sets us on course for full-screen mode
	if( gm && fullScreenMode <= 0.0 ) glfwSetWindowMonitor( gw, NULL, left, top, width, height, GLFW_DONT_CARE ); // this undoes full-screen mode
	if( gm && fullScreenMode >  1.0 ) glfwSetWindowMonitor( gw, gm,   left, top, width, height, ( int )( 0.5 + fullScreenMode ) ); // this specifies the refresh-rate explicitly
	// if neither condition is true (for example, we just use fullScreenMode=1) then we don't need glfwSetWindowMonitor and can just continue in full-screen mode with whatever refresh rate is already in use
	
	// Note that for full-size windows, even if we don't want fullscreen mode, we first pretend that we do, and then undo it.
	// This is because, in non-fullscreen mode, there's a bug/feature of GLFW 3.2.1 that can mis-scale the window.
	// I plugged in an external monitor to my Surface Pro 3: the OS said it's 1920x1080 with 100% DPI scaling, whereas
	// my builtin screen is 2160x1440 with 150% DPI scaling.  Then for illogical reasons, if I declare myself
	// DPI-aware and ask for a full-size non-full-screen window on the UNSCALED second screen, it applies the inverse
	// of the OTHER screen's scaling factor and, although 1920x1080 is passed to CreateWindow, I only actually get a
	// window covering the top left 1280x720.   Workarounds are to declare ourselves dpi-UNAWARE (we don't want that,
	// because it will mess things up on the builtin screen) or to create in full-screen mode.  Luckily the latter can be undone.
	// NB: doing this would mess things up if we're really asking for a NON-full-size window....,  so we set
	// gm=NULL, above, in that case.  So then the system MIGHT mis-scale some non-fullsize non-fullscreen
	// secondary-monitor windows, but hey, you can't have it all.
	glfwSetWindowPos( gw, left, top );
	mToolboxWindowHandle = ( void * )gw;
	glfwSetWindowUserPointer( gw, ( void * )this );
	glfwMakeContextCurrent( gw );
	if( !mOverlayMode ) glfwSetInputMode( gw, GLFW_CURSOR, GLFW_CURSOR_HIDDEN );
	glfwSwapInterval( 1 ); // TODO: investigate: negative values may be beneficial if WGL_EXT_swap_control_tear is supported...??
	glfwSetWindowPosCallback( gw, WindowPosCallback );
	glfwSetWindowSizeCallback( gw, WindowSizeCallback );
	glfwSetWindowCloseCallback( gw, WindowCloseCallback );
	glfwSetWindowRefreshCallback( gw, WindowRefreshCallback );
	glfwSetWindowFocusCallback( gw, WindowFocusCallback );
	glfwSetWindowIconifyCallback( gw, WindowIconifyCallback );
	glfwSetFramebufferSizeCallback( gw, FramebufferSizeCallback );
	glfwSetMouseButtonCallback( gw, MouseButtonCallback );
	glfwSetCursorPosCallback( gw, CursorPosCallback );
	glfwSetCursorEnterCallback( gw, CursorEnterCallback );
	glfwSetScrollCallback( gw, ScrollCallback );
	glfwSetKeyCallback( gw, KeyCallback );
	glfwSetCharCallback( gw, CharCallback );
	glfwSetDropCallback( gw, DropCallback );
	//glfwSetWindowMaximizeCallback( gw, WindowMaximizeCallback ); // TODO: looks like it's only implemented in GLFW 3.3+
	// TODO: there are also global (not window-bound) callbacks for joystick events, and for monitors being connected/disconnected
	
#ifdef    _WIN32
	//mNativeWindowHandle would be needed for windows-specific raise & focus code, but for now it seems GLFW has this covered
	//mNativeWindowHandle = ::FindWindow( NULL, WINDOW_TITLE );
	//mNativeWindowHandle = glfwGetWin32Window( gw ); // for this, need to #include <GLFW/glfw3native.h> 
	//
	// wglSwapIntervalEXT would be needed for windows-specific VBL synchronization, but for now it seems GLFW has this covered
	//wglSwapIntervalEXT( 1 ); // for this, need to #include <GL/wglew.h> and call wglewInit();  TODO: issue warning/error if unsupported or if failed
#else
	// ...if GLFW fails: how to ensure VBL-locked timing on non-Windows systems?
#endif // _WIN32
	//DEBUG(0, width << " x " << height << "  ->  " << GetWidth() << " x " << GetHeight() );
	EnableOpenGL(); // this assumes an OpenGL context has been created (a platform-specific operation, wrapped e.g. by GLUT or GLFW)
	mRenderer = new Renderer( InitShading( GetWidth(), GetHeight(), glslDirectory, substitutions ), legacy );
	mVisibleProperty = mRenderer->Properties( "visible", true, "int32" );
	( ( int32_t * )( mVisibleProperty->GetDataPointer() ) )[ 0 ] = visible;
	SetVisibility( visible, true );

	ShaDyLib::Property * clearColorProperty = mRenderer->Properties( "clearColor", true, "float64" );
	float64_t * clearColor = ( float64_t * )clearColorProperty->GetDataPointer();
	clearColor[ 0 ] = 0.0; clearColor[ 1 ] = 0.0; clearColor[ 2 ] = mOverlayMode ? 0.0 : 0.4; 
}

ShaDyLib::Window::~Window()
{
	Close();
}

void
ShaDyLib::Window::SetEventCallback( EventCallback func )
{
	mEventCallback = func;
}

int
ShaDyLib::Window::GetWidth( void )
{
	GET_GLFW_WINDOW( gw );
	if( !gw ) return 0;
	int width;
	
	if( mPixelScaling <= 0.0 )
	{                                                // this gets the right size for a Retina screen some of the time
		glfwGetFramebufferSize( gw, &width, NULL );  // i.e. ONLY when the ratio between real physical pixels and "Looks like"
	}                                                // resolution in System Preferences -> Displays is equal to the internal backingScaleFactor, which is 2 for Retina screens so far as of 2018
	else
	{                                                // for other cases we'll fall back on rescaling via a known (externally obtained) pixel scaling ratio
		glfwGetWindowSize( gw, &width, NULL );
		width = ( int )( width * mPixelScaling );
	}
	if( width <= 0 ) width = mLastKnownWidth; // because the GLFW API may return 0 when the window is not visible
	else mLastKnownWidth = width;
	return width;
}

int
ShaDyLib::Window::GetHeight( void )
{
	GET_GLFW_WINDOW( gw );
	if( !gw ) return 0;
	int height;
	if( mPixelScaling <= 0.0 )
	{                                                // this gets the right size for a Retina screen some of the time
		glfwGetFramebufferSize( gw, NULL, &height ); // i.e. ONLY when the ratio between real physical pixels and "Looks like"
	}                                                // resolution in System Preferences -> Displays is equal to the internal backingScaleFactor, which is 2 for Retina screens so far as of 2018
	else
	{                                                // for other cases we'll fall back on rescaling via a known (externally obtained) pixel scaling ratio
		glfwGetWindowSize( gw, NULL, &height );
		height = ( int )( height * mPixelScaling );
	}
	if( height <= 0 ) height = mLastKnownHeight; // because the GLFW API may return 0 when the window is not visible
	else mLastKnownHeight = height;
	return height;
}

void
ShaDyLib::Window::Run( void )
{
	mRunning = true;
	while( mToolboxWindowHandle && !glfwWindowShouldClose( ( GLFWwindow * )mToolboxWindowHandle ) ) Display();
	mRunning = false;
	Destroy();
}

void
ShaDyLib::Window::Display( void )
{
	GET_GLFW_WINDOW( gw );
	if( !gw ) return;
	int32_t & makeVisible = ( ( int32_t * )mVisibleProperty->GetDataPointer() )[ 0 ];
	bool forceRaise = makeVisible > 1;
	if( forceRaise ) makeVisible = 1;
	//glFinish(); // you'd think it would be logical to do this but it invites frame-skips and does not seem to improve veridicality
	glfwSwapBuffers( gw );
	SetVisibility( makeVisible != 0, forceRaise );
	mRenderer->Draw();
	glfwPollEvents();
}

void
ShaDyLib::Window::SetVisibility( bool visible, bool force )
{
	GET_GLFW_WINDOW( gw );
	if( !gw ) return;
	if( mVisible == visible && !force ) return;
	if( visible )
	{
		glfwShowWindow( gw );
		glfwRestoreWindow( gw );
		if( !mOverlayMode ) glfwFocusWindow( gw );
#ifdef    _WIN32
//		// Currently looks like native raise & focus code is not needed - GLFW looks like it has this covered...
//		// use a cheat from https://www.codeproject.com/Tips/76427/How-to-bring-window-to-top-with-SetForegroundWindo.aspx
//		// emulate the pressing of the ALT key, which allows non-foreground process to leap to the front
//		BYTE keyState[ 256 ] = { 0 };
//		if( ::GetKeyboardState( ( LPBYTE )&keyState ) && !( keyState[ VK_MENU ] & 0x80 ) )
//			::keybd_event( VK_MENU, 0, KEYEVENTF_EXTENDEDKEY, 0 );	
//		::SetForegroundWindow( ( HWND )mNativeWindowHandle ); // this raises and focuses the window - normally only if the current process is in the foreground, but the ALT keypress emulation cheats around that restriction
//		if( ::GetKeyboardState( ( LPBYTE )&keyState ) && !( keyState[ VK_MENU ] & 0x80 ) )
//			::keybd_event( VK_MENU, 0, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0 );
#else
		// ...if GLFW fails: how to raise & focus on non-Windows systems?
#endif // _WIN32
	}
	else
	{
		glfwIconifyWindow( gw );
		glfwHideWindow( gw );
	}
	mVisible = visible;
}

void
ShaDyLib::Window::Close( void )
{
	GET_GLFW_WINDOW( gw );
	if( !gw ) return;
	glfwSetWindowShouldClose( gw, GLFW_TRUE );
	if( !mRunning ) Destroy();
}

void
ShaDyLib::Window::Destroy( void )
{
	GET_GLFW_WINDOW( gw );
	if( !gw ) return;
	mToolboxWindowHandle = NULL;
	glfwDestroyWindow( gw );
	//glfwTerminate();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // INCLUDED_ShaDyLib_CPP
