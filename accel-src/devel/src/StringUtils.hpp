/*
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
*/

#ifndef INCLUDED_StringUtils_HPP
#define INCLUDED_StringUtils_HPP

#include <string>
#include <vector>

#define  STRINGIFY( s ) _STRINGIFY( s )
#define _STRINGIFY( s ) #s

namespace StringUtils
{
	typedef std::vector< std::string  > StringVector;
	typedef std::vector< StringVector > StringVectorVector;
	
	std::string  ToLower( const std::string & s );
	std::string  ToUpper( const std::string & s );
	std::string  ChompString( std::string & s, const std::string & delims, char groupOpener='\0', char groupCloser='\0', bool countEmpty=false );
	std::string  Join( const StringVector & v, const std::string & delim=" ", bool removeDelim=false );
	void         Split( StringVector & output, std::string input, const std::string & delims=" \t\r\n", char groupOpener='\0', char groupCloser='\0', bool countEmpty=false );
	             // split by ANY ONE of the characters in `delims`
	std::string  StringRepresentation( const std::string & s );
	std::string  StringRepresentation( const char * s );
	std::string  StringRepresentation( const char * s, size_t length, bool allhex=false );
	bool         Match( const std::string & pattern, const std::string & candidate, const char *options="" );
	bool         StartsWith( const std::string & fullString, const std::string & prefix );
	bool         EndsWith( const std::string & fullString, const std::string & suffix );
	int          Replace( std::string & s, const std::string & searchTerm, const std::string & replacement, bool replaceAll=false );

	std::string  Tabulate( const std::string & s, const std::string & columnAlignment="l" );
	
	const char * FilterOption( int * argc, const char * argv[], const char * targetOption );
	const char * FirstOption( int argc, const char * argv[] );
}

#endif // ifndef INCLUDED_StringUtils_HPP
