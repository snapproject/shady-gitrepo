/*
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
*/

#ifndef INCLUDE_ExceptionUtils_HPP
#define INCLUDE_ExceptionUtils_HPP

#include <string>
#include <sstream>

#define STRINGSUBCLASS( x ) class x : public std::string { public: \
  x( const std::string & s ) { std::string & r = *this; r = s; } \
  x & operator=( std::string & s ) { std::string & r = *this; r = s; return *this; } }
// Example:  STRINGSUBCLASS( DataStreamError );

#define RAISE( x ) { std::stringstream _ss; _ss << x; throw  _ss.str(); }
// example:   RAISE( "could not interpret \"" << argument << "\" as a valid argument"  )

#define RAISE_SUBCLASS( class, x ) { std::stringstream _ss; _ss << x; throw  class( _ss.str() ); }
// example:   RAISE( DataStreamError, "Data stream overflowed by " << overflow << " bytes" );


#endif // ifndef INCLUDE_ExceptionUtils_HPP
