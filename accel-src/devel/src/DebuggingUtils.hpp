/*
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
*/

#ifndef INCLUDE_DebuggingUtils_HPP
#define INCLUDE_DebuggingUtils_HPP

#include "BasicTypes.hpp"
#include "StringUtils.hpp"
#include <iostream>
#include <sstream>

#define DB std::cerr << __FILE__ << ":" << __LINE__ << std::endl
// simple "I was here" marker

#ifndef DEBUG_LEVEL
#	define DEBUG_LEVEL 0
#endif

#define DEBUG( level, x ) { if( ( level ) <= DEBUG_LEVEL ) std::cerr << x << std::endl; }
#define DBREPORT( level, x ) DEBUG( level, #x << " = " << x )
#define DBREPORTQ( level, x ) DEBUG( level, #x << " = " << StringUtils::StringRepresentation( x ) )

namespace DebuggingUtils
{
	int Console( const char * msg, int debugLevel );
	void PrintArgv( int argc, const char * argv[] );
}

#define DBASSERT( x, msg )  if( ! ( x ) ) _errors << ( _errors.str().length() ? "\n" : "" ) << "failed assertion " #x msg;
#define SANITY_CHECK( PACKING_TEST_STRUCT, TIGHT ) \
{ \
	std::stringstream _errors; \
	DBASSERT( sizeof( int8_t    ) == 1, "" ); \
	DBASSERT( sizeof( uint8_t   ) == 1, "" ); \
	DBASSERT( sizeof( int16_t   ) == 2, "" ); \
	DBASSERT( sizeof( uint16_t  ) == 2, "" ); \
	DBASSERT( sizeof( int32_t   ) == 4, "" ); \
	DBASSERT( sizeof( uint32_t  ) == 4, "" ); \
	DBASSERT( sizeof( int64_t   ) == 8, "" ); \
	DBASSERT( sizeof( uint64_t  ) == 8, "" ); \
	DBASSERT( sizeof( float32_t ) == 4, "" ); \
	DBASSERT( sizeof( float64_t ) == 8, "" ); \
	DBASSERT( sizeof( PACKING_TEST_STRUCT ) == TIGHT, "  (not compiled with packed struct alignment)" ); \
	int x = 1; uint8_t * one = ( uint8_t * )&x; \
	DBASSERT( one[ 0 ] == 1, "  (big-endian CPUs are not supported - sorry)" ); \
	if( _errors.str().length() ) RAISE( _errors.str() ); \
}

#endif // ifndef INCLUDE_DebuggingUtils_HPP
