/*
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
*/

#ifndef    INCLUDED_BasicTypes_CPP
#define    INCLUDED_BasicTypes_CPP
//////////////////////////////////////////////////////////////////////

#include "BasicTypes.hpp"
#include "ExceptionUtils.hpp"

size_t
as_size_t( uint64_t i )
{
	if( i > ( ( size_t )( -1 ) ) ) RAISE( "index overflow" );
	return ( size_t )i;
}

//////////////////////////////////////////////////////////////////////
#endif  // INCLUDED_BasicTypes_CPP
