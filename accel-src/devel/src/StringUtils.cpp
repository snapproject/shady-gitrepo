/*
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
*/

#ifndef INCLUDED_StringUtils_CPP
#define INCLUDED_StringUtils_CPP

#include "StringUtils.hpp"
#include <cstring>
#include <map>
#include <algorithm>
#include <sstream>
#include <iomanip>
#define HEX( width )  std::setfill( '0' ) << std::setw( width ) << std::hex << std::uppercase

std::string
StringUtils::ToLower( const std::string & s )
{
	std::string out = s;
	for( std::string::iterator it = out.begin(); it != out.end(); it++ ) *it = tolower( *it );
	return out;
}

std::string
StringUtils::ToUpper( const std::string & s )
{
	std::string out = s;
	for( std::string::iterator it = out.begin(); it != out.end(); it++ ) *it = toupper( *it );
	return out;
}

std::string
StringUtils::ChompString( std::string & s, const std::string & delims, char groupOpener, char groupCloser, bool countEmpty )
{ // split by any one of the characters in `delims`
#	define ISDELIM( CHAR )  ( delims.find( CHAR ) != std::string::npos )
	std::string out;
	size_t pos = 0, length = s.length();
	if( countEmpty && length && ISDELIM( s[ 0 ] ) ) { s = s.substr( 1 ); return out; }
	while( pos < length && !countEmpty && ISDELIM( s[ pos ] ) ) pos++;
	size_t start = pos, tokenLength = 0, nested=0;
	while( pos < length && ( nested || !ISDELIM( s[ pos ] ) ) )
	{
		if( nested && groupCloser && groupCloser == s[ pos ] ) nested--;
		else if( groupOpener && groupOpener == s[ pos ] ) nested++;
		pos++;
		tokenLength++;
	}
	while( pos < length && ISDELIM( s[ pos ] ) ) 
	{
		pos++;
		if( countEmpty ) break;
	}
	out = s.substr( start, tokenLength );
	s = s.substr( pos );
	return out;
#	undef ISDELIM
}

std::string
StringUtils::Join( const StringVector & v, const std::string & delim, bool removeDelim )
{
	std::string out;
	bool started = false;
	size_t delim_length = delim.length();
	for( StringVector::const_iterator it = v.begin(); it != v.end(); it++ )
	{
		if( started ) out += delim;
		if( removeDelim )
		{
			for( size_t i = 0; i < it->length(); )
			{
				if( it->substr( i, delim_length ) == delim ) i += delim_length; 
				else out += ( *it )[ i++ ];
			}
		}
		else out += *it;
		started = true;
	}
	return out;
}

void
StringUtils::Split( StringVector & output, std::string input, const std::string & delims, char groupOpener, char groupCloser, bool countEmpty )
{ // split by ANY ONE of the characters in `delims`
	output.clear();
	while( input.length() ) output.push_back( ChompString( input, delims, groupOpener, groupCloser, countEmpty ) );
}

std::string
StringUtils::StringRepresentation( const std::string & s )
{
	return StringRepresentation( s.c_str(), s.length() );
}

std::string
StringUtils::StringRepresentation( const char * s )
{
	return StringRepresentation( s, s ? strlen( s ) : 0 );
}

std::string
StringUtils::StringRepresentation( const char * s, size_t length, bool allhex )
{
	if( s == NULL ) return "NULL";
	std::stringstream ss;
	char quote = '"';
	ss << quote;
	for( size_t i = 0; i < length; i++ )
	{
		char c = s[ i ];
		if(      !allhex && c == '\0' ) ss << "\\0";
		else if( !allhex && c == '\n' ) ss << "\\n";
		else if( !allhex && c == '\r' ) ss << "\\r";
		else if( !allhex && c == '\t' ) ss << "\\t";
		else if( !allhex && c == '\\' ) ss << "\\\\";
		else if( !allhex && c == quote ) ss << "\\" << c;
		else if( allhex || c < 32 || c > 127 ) ss << "\\x" << std::setfill( '0' ) << std::setw( 2 ) << std::hex << std::uppercase << ( unsigned int )c;
		else ss << c;
	}
	ss << quote;
	return ss.str();
}

bool
StringUtils::Match( const std::string & pattern, const std::string & candidate, const char *options )
{
	// In `options`, you can supply zero or more of the following characters:
	//   i or I  to make the match case-insensitive
	//   *       to allow a '*' character in `pattern` to match zero or more characters of any kind in `candidate`
	//   ?       to allow a '?' character in `pattern` to match exactly one character of any kind in `candidate`
	// 
	// Adapted from wildcmp() by Jack Handy <jakkhandy@hotmail.com>, which did '*' and '?' globbing
	// https://www.codeproject.com/Articles/1088/Wildcard-string-compare-globbing
	bool starWildcards = false;
	bool queryWildcards = false;
	bool caseInsensitive = false;
	for( ; options && *options; options++ )
	{
		if( *options == 'i' || *options == 'I' ) caseInsensitive = true;
		if( *options == '*' ) starWildcards = true;
		if( *options == '?' ) queryWildcards = true;
	}
	const char *wild = pattern.c_str();
	const char *string = candidate.c_str();
	
	char wc, sc;	
	const char *cp = NULL, *mp = NULL;		
	while( *string && !( starWildcards && *wild == '*' ) )
	{
		wc = caseInsensitive ? toupper( *wild   ) : *wild;
		sc = caseInsensitive ? toupper( *string ) : *string;
		if( wc != sc && !( queryWildcards && wc == '?' ) ) return 0;
		wild++;
		string++;
	}
	if( !starWildcards ) return !*wild;
	
	while( *string )
	{
		wc = caseInsensitive ? toupper( *wild   ) : *wild;
		sc = caseInsensitive ? toupper( *string ) : *string;
		if( wc == '*' )
		{
			if( !*++wild ) return true;
			mp = wild;
			cp = string + 1;
		}
		else if( wc == sc || ( queryWildcards && wc == '?' ) )
		{
			wild++;
			string++;
		}
		else
		{
			wild = mp;
			string = cp++;
		}
	}
	while( *wild == '*' ) wild++;
	return !*wild;
}

bool
StringUtils::StartsWith( const std::string & fullString, const std::string & prefix )
{
    return ( 0 == fullString.compare( 0, prefix.length(), prefix ) );
}

bool
StringUtils::EndsWith( const std::string & fullString, const std::string & suffix )
{
    if( fullString.length() < suffix.length() ) return false;
    return ( 0 == fullString.compare( fullString.length() - suffix.length(), suffix.length(), suffix ) );
}

int
StringUtils::Replace( std::string & s, const std::string & searchTerm, const std::string & replacement, bool replaceAll )
{
	int nMatches = 0;
	size_t matchPos = 0;
	while( ( matchPos = s.find( searchTerm, matchPos ) ) != std::string::npos )
	{
		s.replace( matchPos, searchTerm.length(), replacement );
		matchPos += replacement.length();
		nMatches++;
		if( !replaceAll ) break;
    }
    return nMatches;
}

std::string
StringUtils::Tabulate( const std::string & s, const std::string & columnAlignment )
{
	const char * in;
	std::map< size_t, size_t > columnWidths;
	size_t iRow, iColumn, elementLength;
	for( iRow = iColumn = elementLength = 0, in = s.c_str(); ; in++ )
	{
		switch( *in )
		{
			case '\t':           columnWidths[ iColumn ] = std::max( columnWidths[ iColumn ], elementLength ); elementLength = 0; iColumn++; break;
			case '\n': case'\0': columnWidths[ iColumn ] = std::max( columnWidths[ iColumn ], elementLength ); elementLength = 0; iColumn = 0; iRow++; break;
			default:             elementLength++; break;
		}
		if( !*in ) break;
	}
	const char * alignmentCharacters = columnAlignment.c_str();
	size_t nAlign = columnAlignment.length();
	std::stringstream out;
	for( iRow = iColumn = elementLength = 0, in = s.c_str(); ; in++ )
	{
		char align = nAlign ? tolower( alignmentCharacters[ iColumn % nAlign ] ) : 'l';
		size_t leading = 0, width = columnWidths[ iColumn ];
		switch( *in )
		{
			case '\t': case '\n': case '\0':
				if( align == 'c' ) width -= ( leading = ( width - elementLength ) / 2 );
				if( leading ) out << std::setw( leading ) << " ";
				out << std::setw( width ) << ( align == 'r' ? std::right : std::left ) << std::string( in - elementLength, elementLength );
				if( *in == '\t' ) { elementLength = 0; iColumn++; /*out << " ";*/ }
				if( *in == '\n' ) { elementLength = 0; iColumn = 0; iRow++; out << *in; }
				if( !*in ) return out.str();
				break;
			default:
				elementLength++;
				break;
		}
	}
}

const char *
StringUtils::FilterOption( int * argc, const char * argv[], const char * targetOption  )
{
	// If `targetOption` (e.g. "--verbose") is not found in `argv` from position 1 onwards, return NULL.
	// If it is found, change `argc` and the contents of `argv` such that the option will be skipped in
	// future, and return a pointer to the option's parameter value. (If the option is present but there
	// is no parameter value, the pointer returned will be a non-NULL empty-string pointer, i.e. it will
	// simply point to character '\0'.)
	
	int i, shouldHaveEqualsSign;
	size_t targetOptionLength;
	const char * result = NULL;
	
	targetOptionLength = strlen( targetOption );
	shouldHaveEqualsSign = ( targetOption[ targetOptionLength - 1 ] == '=' );
	if( shouldHaveEqualsSign ) targetOptionLength--;
	// shouldHaveEqualsSign is now unused beyond this - enforcement can be left to the caller given that the return value lets them see the parameter value if any
	if( targetOptionLength && targetOption[ 0 ] == '-' ) { targetOption++; targetOptionLength--; }
	if( targetOptionLength && targetOption[ 0 ] == '-' ) { targetOption++; targetOptionLength--; }
	
	for( i = 1; i < *argc; i++ )
	{
		const char * candidate = argv[ i ];
		if( *candidate == '-' ) candidate++;
		if( *candidate == '-' ) candidate++;
		if( strncmp( candidate, targetOption, targetOptionLength ) == 0 )
		{
			if( candidate[ targetOptionLength ] == '=' ) result = candidate + targetOptionLength + 1;
			else if( candidate[ targetOptionLength ] == '\0' ) result = candidate + targetOptionLength;
		}
		if( result ) for( --*argc; i < *argc; i++ ) argv[ i ] = argv[ i + 1 ];
	}
	return result;
}

const char *
StringUtils::FirstOption( int argc, const char * argv[] )
{
	// Return a pointer to the first item in `argv` that starts with "--", or NULL if there is no such string.
	// It is useful to run this after running FilterOption() several times to capture options you actually
	// support.  Then FirstOption() will return the first unsupported/unrecognized option, if any.
	int i;
	for( i = 1; i < argc; i++ )
	{
		const char * arg = argv[ i ];
		if( arg[ 0 ] == '-' && arg[ 1 ] == '-' ) return arg;
	}
	return NULL;
}

#endif // ifndef INCLUDED_StringUtils_CPP
