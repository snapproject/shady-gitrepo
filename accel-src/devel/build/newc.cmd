#!/usr/bin/env python
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$

""" " 2>NUL
@echo off
cls
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::

set "CUSTOMPYTHONHOME=%PYTHONHOME_DEFAULT%
if "%CUSTOMPYTHONHOME%"=="" goto :skipconfig
set "PYTHONHOME=%CUSTOMPYTHONHOME%
set "PATH=%PYTHONHOME%;%PATH%
:skipconfig

python "%~f0" %*

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::
goto :eof
" """

__doc__ = """\
$0 NAME
"""

import os
import sys
import subprocess

def Which( program ):
	def Executable( fpath ): return fpath if os.path.isfile( fpath ) and os.access( fpath, os.X_OK ) else None
	if sys.platform.lower().startswith( 'win' ):
		Executable_ = Executable
		def Executable( fpath ): return Executable_( fpath + '.exe' ) or Executable_( fpath + '.cmd' ) or Executable_( fpath + '.bat' )
	fpath, fname = os.path.split( program )
	if fpath: return Executable( program )
	for path in os.environ[ 'PATH' ].split( os.pathsep ):
		path = path.strip( '"' )
		exe_file = os.path.join( path, program )
		resolved = Executable( exe_file )
		if resolved: return resolved

def CreatePair( name, traditional=False ):
	parentdir, name = os.path.split( name )
	name, xtn = os.path.splitext( name )
	if xtn.lower() in [ '.cpp', '.hpp', '.cxx', '.hxx', '.c++', '.h++', '.cc', '.hh' ]: mode = 'cpp'
	elif xtn.lower() in [ '.c', '.h' ]: mode = 'c'
	else: name += xtn; xtn = '.cpp'; mode = 'cpp'
	cxtn = xtn.replace( 'h', 'c' ).replace( 'H', 'C' )
	hxtn = xtn.replace( 'c', 'h' ).replace( 'C', 'H' )
	cfilename = os.path.join( parentdir, name + cxtn )
	hfilename = os.path.join( parentdir, name + hxtn )
	if traditional:	
		cdef = '__%s__' % ( name + cxtn ).upper().replace( '.', '_' )
		hdef = '__%s__' % ( name + hxtn ).upper().replace( '.', '_' )
	else:
		cdef = 'INCLUDED_%s' % ( name + cxtn.upper() ).replace( '.', '_' )
		hdef = 'INCLUDED_%s' % ( name + hxtn.upper() ).replace( '.', '_' )
	opencomment  = dict( cpp='// ', c='/* ' )
	closecomment = dict( cpp='',    c=' */' )
	hrule = dict( cpp='/' * 70, c='/* %s */' % ( '*' * 64 ) )
	args = dict( hrule=hrule[ mode ], oc=opencomment[ mode ], cc=closecomment[ mode ], cdef=cdef, hdef=hdef, header=name + hxtn, classdef='' )
	args[ 'spc' ] = ' ' * len( args[ 'oc' ] )
	if mode == 'cpp': args[ 'classdef' ] = """\
class {name}
{{
	public:
		{name}();
		~{name}();
		
	private:
		
		
}};
""".format( name=name ).strip()
	
	if not os.path.isfile( cfilename ) or not open( cfilename ).read().strip():
		with open( cfilename, 'wt' ) as f: f.write( """\
#ifndef {spc}{cdef}
#define {spc}{cdef}
{hrule}

#include "{header}"



{hrule}
#endif  {oc}{cdef}{cc}
""".format( **args ) )
	if not os.path.isfile( hfilename ) or not open( hfilename ).read().strip():
		with open( hfilename, 'wt' ) as f: f.write( """\
#ifndef {spc}{hdef}
#define {spc}{hdef}
{hrule}

{classdef}

{hrule}
#endif  {oc}{hdef}{cc}
""".format( **args ) )
	return cfilename, hfilename

if __name__ == '__main__':
	argv = list( getattr( sys, 'argv', [] ) )
	execdir, execname = os.path.split( argv.pop( 0 ) )
	__doc__ = __doc__.replace( '$0', execname )
	if len( argv ) != 1: sys.stderr.write( '\nusage:%s\n' % ( '\n' + __doc__.strip() ).replace( '\n', '\n   ' ) ); sys.exit( -1 )
	cfilename, hfilename = CreatePair( argv[ 0 ] )
	editor = Which( 'edit' ) or Which( 'cygstart' ) or Which( 'open' ) or Which( 'start' )
	if editor: subprocess.call( [ editor, cfilename, hfilename ] )
