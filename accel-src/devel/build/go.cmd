#!/bin/bash

:<<@GOTO:EOF
:: Windows section
@echo off
: ################################################################
:: NOTE TO SELF: DO NOT USE BACKTICKS IN THE COMMENTS

:: go.cmd
::     calls cmake followed by either msbuild (on Windows) or
::     make all (on other platforms) to perform an out-of-source
::     build resulting in a dynamic library in ../../release/
:: 
::     (Along the way, this script also auto-generates a header
::     called ShaDyLib_Revision.h which allows the library to
::     introspect on its own build date and version-control id.)
::
:: On macOS/Linux:
::
::     ./go.cmd  [optional CMake arguments...]
::
::         This script will always include the CMake arguments -H.
::         and -B./junk to ensure an out-of-source build with all
::         the auto-generated files tidied away into a directory
::         called "junk".
:: 
::         If you supply no arguments, -G "Unix Makefiles" is
::         also assumed. Your own arguments override this, and
::         cancel the subsequent call to make.
:: 
:: On Windows:
::
::     go.cmd [ [ PLATFORM ]  "MSVC_VERSIONS_TO_TRY" ]
::
::     The default PLATFORM argument is Win64. The other alternative
::     is Win32.  In common with the macOS/Linux instantiation of
::     this script, the -H and -B flags will be passed to CMake to
::     ensure an out-of-source build. The auto-generated files
::     will be tidied away into a directory whose name reflects both
::     the PLATFORM and the MSVC version.
:: 
::     Examples::
::
::         go.cmd
::         go.cmd Win64
::             Builds 64-bit Windows DLL using whichever version of 
::             Visual Studio can be found.
::        
::         go.cmd Win32
::             Builds 32-bit Windows DLL using whichever version of 
::             Visual Studio can be found.
::        
::         go.cmd Win64 12
::             Builds 64-bit Windows DLL using Visual Studio 12 (2013).
::        
::         go.cmd Win64 "auto 2017 2015 2013 2012"
::             Builds 64-bit Windows DLL, trying to find Visual Studio
::             versions in the specified order of preference. The
::             MSVC_VERSIONS_TO_TRY argument can contain VS version
::             numbers (11, 12, 14, 15) or, equivalently, the
::             corresponding years (2012, 2013, 2015, 2017). 'auto'
::             tries to auto- detect using the vswhere.exe utility
::             which comes with recent versions of the Microsoft Visual
::             Studio Installer.
::         
::     Requirements:
::          CMake  <http://cmake.org>
::              version 3.7+ must be installed for all users and added
::              added to the system path (this is a check-box option in
::              the CMake installer for Windows).
::          (Windows): Visual Studio <http://visualstudio.com>
::              version 11 (i.e. 2012) or later must be installed (the
::              free "Express" or "Community" versions are fine)
::          (macOS): XCode Command-Line Tools <https://developer.apple.com/download/more/>
::              Are required. Note that this script sets the environment
::              variable MACOSX_DEPLOYMENT_TARGET=10.9 if possible, which
::              means the dynamic library it makes should run on systems
::              as far back as Mavericks (2013). If the current system
::              is earlier than 10.9, it will fall back to supporting
::              everything back as far as 10.4 (Tiger, 2005) but that
::              comes at the expense of possible occasional clock glitches
::              (in the absence of C++11 support, the central clock will
::              be vulnerable to network time updates). 
:: 
::          If you want to use a different compiler and know what you're
::          doing, then instead of using this script you can equally well
::          call CMake yourself (CMakeLists.txt is here in this directory)
::          with the -G generator argument of your choosing, and build by
::          hand.
cls

setlocal
setlocal EnableDelayedExpansion

set "TARGET=Win64"
set "IDE="
if not "%~1"=="" set "TARGET=%~1"
if not "%~2"=="" set "IDE=%~2"

call "%~dp0VisualStudio" %TARGET% %IDE% || exit /b 1
set "GENERATOR=Visual Studio %VSVERSION%"
if "%VS_USE_CMAKE_ARCH_FLAG%"=="" (
	if not "%TARGET%"=="Win32" set "GENERATOR=%GENERATOR% %TARGET%"
) else (
	if "%TARGET%"=="Win32" set "VS_USE_CMAKE_ARCH_FLAG=%VS_USE_CMAKE_ARCH_FLAG% %TARGET%"
	if "%TARGET%"=="Win64" set "VS_USE_CMAKE_ARCH_FLAG=%VS_USE_CMAKE_ARCH_FLAG% x64"
)
set "JUNK=junk-VS%VSVERSION%-%TARGET%"
set "STARTDIR=%CD%
cd "%~dp0"
::echo."%GENERATOR%" && echo."%VSVERSION%"

mkdir "%JUNK%" >NUL 2>NUL

call "%~dp0MakeRevisionHeader" SHADYLIB > "%JUNK%\ShaDyLib_Revision.h"

cmake "-H." "-B%JUNK%" -G "%GENERATOR%" %VS_USE_CMAKE_ARCH_FLAG%
set PLATFORM=
msbuild /p:Configuration=Release "%JUNK%\ShaDyLib.sln"
cd "%STARTDIR%
endlocal

pause

: ###################################################################
@GOTO:EOF
(tr -d \\r|env EXECNAME="$0" bash "$@")<<":EOF"
: ###################################################################

# posix (bash) section

export MACOSX_DEPLOYMENT_TARGET=10.9
which python3 >/dev/null 2>&1 && PYTHON=python3 || PYTHON=python
$PYTHON <<-ENDPYTHON || export MACOSX_DEPLOYMENT_TARGET=10.4
import sys, platform
if sys.platform.lower().startswith( 'darwin' ):
    version = [ int( x ) for x in platform.mac_ver()[ 0 ].split( '.' ) ]
    if version < [ 10, 9 ]: sys.exit( 1 )
ENDPYTHON
#echo MACOSX_DEPLOYMENT_TARGET=$MACOSX_DEPLOYMENT_TARGET
# Under 10.13 (High Sierra, 2017) with gcc --version = "Apple LLVM 9.1.0":
# - you get a libstd++ deprecation warning if you target anything earlier than 10.9 (Mavericks, 2013)
# - the absolute farthest back you can go is 10.4 (Tiger, 2005)
# Update: now we only go back to 10.9 by default, not 10.4, because we need the newer libc++,
#         not the deprecated libstdc++, for c++11 features like steady_clock

STARTDIR=$(pwd)
TARGETDIR=$(dirname "$EXECNAME")
JUNK=junk
cd "$TARGETDIR"
mkdir -p "$JUNK"

. MakeRevisionHeader.cmd SHADYLIB > "$JUNK/ShaDyLib_Revision.h"

if [ "$1" = "" ]; then
  cmake -H. -B$JUNK -G "Unix Makefiles"
  cd "$JUNK"
  make all
  cd "$STARTDIR"
else
  cmake -H. -B$JUNK "$@"
fi

: ###################################################################
:EOF
