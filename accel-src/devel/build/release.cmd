#!/usr/bin/env python
"""
@echo off
cls
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::

where python.exe 1>NUL 2>NUL && goto :skipconfig
:: (because any python will do, for this script)

set "CUSTOMPYTHONHOME=%PYTHONHOME_DEFAULT%
if "%CUSTOMPYTHONHOME%"=="" goto :skipconfig
set PYTHONHOME=%CUSTOMPYTHONHOME%
set PATH=%PYTHONHOME%;%PATH%
:skipconfig

python "%~0" %*

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::
goto :eof
"""
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$

import os, sys, shutil, time, glob, inspect

def canonicalize( *path_pieces ):
	return os.path.realpath( os.path.join( *path_pieces ) ).replace( '\\', '/' ).rstrip( '/' )

try: frame = inspect.currentframe();  HERE = os.path.dirname( inspect.getfile( frame ) )
finally: del frame

def main():

	srcdir = canonicalize( HERE, '..', '..', 'release' )
	nargs = len( sys.argv ) - 1
	if nargs == 0:
		dstdir = canonicalize( HERE, '..', '..', '..', 'python', 'Shady', 'accel' )
		mode = 'minimal'
		removeBinary = True
	elif nargs == 1:
		dstdir = canonicalize( sys.argv[ 1 ] )
		mode = 'full'
		removeBinary = False
	else:
		sys.stderr.write( 'usage: %s\n       %s DSTDIR\n' % ( sys.argv[ 0 ], sys.argv[ 0 ] ) )
		return 1
		
	print( '      SOURCE = %r' % srcdir )
	print( ' DESTINATION = %r' % dstdir )

	if not os.path.isdir( dstdir ):
		sys.stderr.write( "destination directory does not exist\n" )
		return 1

	sys.path.insert( 0, srcdir )
	translation = None
	if mode == 'full':
		import ShaDyLib as SH
		version = SH.GetVersion()
		revision = SH.GetRevision()
		print( " DLL version = %r" % version )
		print( "DLL revision = %r" % revision )
		translation = {
			'VERSION'         : version,
			'REVISION'        : revision,
		}

	def mkdir( *path_pieces ):
		path = canonicalize( *path_pieces )
		if not os.path.isdir( path ):
			print( "creating directory %s" % path )
			os.makedirs( path )
		return path
		
	def copyfile( pattern, binary=False, removeBinary=False ):
		for source in sorted( glob.glob( srcdir + '/' + pattern ) ):
			source = canonicalize( source )
			destination = canonicalize( dstdir, source[ len( srcdir ) + 1 : ] )
			verb = "copying"
			if binary and removeBinary: verb = " moving"
			print( "\n%s from: %s\n          to: %s" % ( verb, source, destination ) )
			mkdir( destination, '..' )
			if translation and not binary:
				txt = open( source, 'rb' ).read().decode( 'utf-8' )
				for match, replace in translation.items(): txt = txt.replace( '@' + match + '@', replace )
				open( destination, 'wb' ).write( txt.encode( 'utf-8' ) )
			else:
				shutil.copy2( source, destination )
				if binary and removeBinary:
					try: os.remove( source )
					except: print( '\n    NB: failed to remove original %s' % source )
				
	copyfile( '*.dll',   binary=True, removeBinary=removeBinary )
	copyfile( '*.so',    binary=True, removeBinary=removeBinary )
	copyfile( '*.dylib', binary=True, removeBinary=removeBinary )
	copyfile( 'ShaDyLib.py' )
	copyfile( 'ShaDyLib.h' )
	if mode == 'full':
		copyfile( 'ShaDyLib_Loader.c' )
		copyfile( 'demo.c' )
		copyfile( 'changelog.txt' )
		copyfile( 'README.txt' )
	print( '' )

if __name__ == '__main__':
	sys.exit( main() )
