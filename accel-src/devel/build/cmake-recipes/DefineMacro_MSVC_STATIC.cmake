MACRO( MSVC_STATIC )
  IF( MSVC )
    # Set compiler options.
    SET( variables
      CMAKE_C_FLAGS_DEBUG
      CMAKE_C_FLAGS_MINSIZEREL
      CMAKE_C_FLAGS_RELEASE
      CMAKE_C_FLAGS_RELWITHDEBINFO
      CMAKE_CXX_FLAGS_DEBUG
      CMAKE_CXX_FLAGS_MINSIZEREL
      CMAKE_CXX_FLAGS_RELEASE
      CMAKE_CXX_FLAGS_RELWITHDEBINFO
    )
    MESSAGE( "MSVC -> forcing use of statically-linked runtime." )
    FOREACH( variable ${variables} )
      IF( ${variable} MATCHES "/MD" )
        STRING( REGEX REPLACE "/MD" "/MT" ${variable} "${${variable}}" )
      ENDIF()
    ENDFOREACH()
  ENDIF( MSVC )
ENDMACRO( MSVC_STATIC )
