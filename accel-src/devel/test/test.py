# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$

import sys, os
location = os.path.realpath( os.path.join( __file__, '..', '..', '..', 'release' ) )
modulePath = os.path.join( location, 'ShaDyLib.py' )
if not os.path.isfile( modulePath ): raise SystemExit( "failed to find " + modulePath )
print( 'importing ' + modulePath )

sys.path.insert( 0, location )
import ShaDyLib as sh
sys.path.pop( 0 )

print( sh.dll )
print( sh.GetPlatform() )
print( sh.GetVersion() )
print( sh.GetRevision() )
print( sh.GetCompilationDatestamp() )
