/*
# $BEGIN_SHADY_LICENSE$
# 
# This file is part of the Shady project, a Python framework for
# real-time manipulation of psychophysical stimuli for vision science.
# 
# Copyright (c) 2017-2025 Jeremy Hill, Scott Mooney
# 
# Shady is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# $END_SHADY_LICENSE$
*/

#include "ShaDyLib.hpp"

#include "DebuggingUtils.hpp"
#include "ExceptionUtils.hpp"
#include "FileUtils.hpp"
#include "StringUtils.hpp"

#include <iostream>
#include <string>

using namespace std;

int Message( const char * msg, int debugLevel=3 )
/*
	Debug levels 1 or less:  described as an "error"
	      level 2: described as a "warning"
	      higher: just a "message"
*/
{
	if( debugLevel > 10 ) return 0;
	cerr << "ShaDyLib " << ( debugLevel < 2 ? "Error" : debugLevel < 3 ? "Warning" : "Message" ) << " (level " << debugLevel << "): " << msg << endl;
	return 1;
}

int Main( int argc, const char * argv[] )
{	
	Message( "Hello world!" );
	for( std::string s = FileUtils::WhereAreYou( NULL ); s.length(); s = FileUtils::ParentDirectory( s ) ) DBREPORTQ( 0, s );
	return 0;
}


int main( int argc, const char * argv[] )
{	
	try
	{
		return Main( argc, argv );
	}
	catch( std::string const & error )
	{
		return Message( error.c_str(), 1 );
	}
	catch( std::exception const & ex )
	{
		std::string s = "std::exception caught: "; s += ex.what();
		return Message( s.c_str(), 1 );
	}
	catch( ... )
	{
		return Message( "uncaught exception", 1 );
	}
}
