This subdirectory contains C++ and C source files, as well as binaries of
some third-party libraries, for building the `ShaDyLib` dynamic library, a.k.a. the
Shady "accelerator". You probably do not need this directory in order to run Shady,
since pre-built binary versions of the accelerator are kept under version-control
(everything you need is inside the Python package, `python/Shady`) and are also
bundled inside the `pypi.org` download that you install with `pip`.  This directory
is provided (a) for Shady's developers to rebuild, extend or debug the accelerator;
(b) for people who are attempting to use Shady on platforms for which we have not
provided pre-built binaries, and (c) to comply with the GNU General Public License,
which requires all source code to be made available.

`accel-src/devel/build/go.cmd`  works on both Windows and posixoid systems, to compile
the accelerator from source. It requires CMake 3.7+ and a C/C++ compiler that supports
C++11 or later.  It links against third-party libraries GLEW (provided as C source) and
GLFW (provided as static libraries built from slightly-modified sources---see
`accel-src/devel/glfw-3.2.1/build-notes.txt`).

On Windows, you can of course use any compiler supported by CMake, but for full automation
`go.cmd` needs Visual Studio (version from 2012 to 2017---free "Express" or "Community"
editions are fine). By default, the argument `Win64` is assumed and a 64-bit binary is
built. You can also say `accel-src\devel\build\go.cmd Win32` to build a 32-bit binary,
and this is also part of our standard release process. Each resulting DLL is named
differently according to the platform/architecture for which it is built.

Binaries are built in `accel-src/release/` and are not version-controlled in this
location.  If you are using `Shady` from this git working-copy (perhaps installed
with `pip install -e`) then Shady will detect and use any freshly-built ("development")
version of the DLL found in the `accel-src/release/` directory in preference to the
version-controlled "bundled" copy inside `../python/Shady`.   Once you have determined
that the binary is stable, you can invoke `accel-src/devel/build/release.cmd` to move
the binary dynamic library, and copy the supporting header file and Python wrapper
that must both accompany it, to the appropriate places within `../python/Shady`.
`release.cmd` is really a self-running Python script so again it will work on both
Windows and posix.

