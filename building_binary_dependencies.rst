.. default-role:: code

Accelerator
===========

* On Windows, install Visual Studio
* On macOS, install gcc, g++, git, etc with `xcode-select --install`
* On macOS or Windows, install CMake using the installer from https://cmake.org
* On Linux, `sudo apt install cmake git`

A new platform will require a new appropriately-named subdirectory of
`accel-src/devel/glfw/lib`::

    git clone git@bitbucket.org:snapproject/glfw-fork
	cd glfw-fork
	./go.cmd

Then, back in `shady-gitrepo`::

    ./accel-src/devel/build/go.cmd

Note the platform/architecture identifiers that the resulting dynamic library
name uses (obtained by `DefineFunction_TARGET_ARCHITECTURE.cmake`)
For a new platform like the M1, ensure that Python retrieves the same
identifier from `platform.machine().lower()` when it runs
`Shady.accel.ShaDyLib.LoadSharedLibrary()`. If all is in order::

    ./accel-src/devel/build/release.cmd

Is it worth trying to make an M1+Intel universal binary? Maybe not, but whether
in one file or two, it would certainly be an advantage to cross-compile for
both platforms on either. Here is a cross-compiling breadcrumb from a comment
under https://stackoverflow.com/a/65385996 ::

    clang++ main.cpp -target arm64-apple-macos11 -isysroot /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX11.1.sdk


dpxmode
=======

A new platform will require a new appropriately-inflected subdirectory of
`dpxmode-src/libusb/`, containing `usb.h`, 'usbi.h` and a build of `libusb.a`.
The process is not straightforward but theoretically it is all described and wrapped
up in `dpxmode-src/build.cmd`.

`dpxmode-src/build.cmd` may need to be updated for new platforms, in the
`uname` section.
        
Once `build.cmd` is working, the `release.cmd` script should dump it into an
appropriately-named subdirectory of `python/Shady/VPixx/bin`.


M1 Mac Update 2022-02-07
========================

The M1 appears to be able to emulate x86_64: when I installed Anaconda, I found
that I had actually installed the x86_64 version, `platform.machine()` returned
`x86_64`, and all the existing Intel binaries got loaded without a hitch.

If you use the natively installed `/usr/bin/python` or `/usr/bin/python3` it's a
different story: `platform.machine()` gives you `arm64`, which matches `uname -m`.
But `DefineFunction_TARGET_ARCHITECTURE.cmake` is more precise and returns `armv8`,
so this needs to be dumbed down post-hoc in `CMakeLists.txt`, for Python's benefit.
  
With that fixed, the glfw library and ShaDyLib accelerator dylib could be built
without any trouble. The only glitch appears to be the following warning at run time,
whose exact implications for Shady have not yet been analysed::

    UNSUPPORTED (log once): POSSIBLE ISSUE: unit 0 GLD_TEXTURE_INDEX_2D is unloadable
    and bound to sampler type (Float) - using zero texture because texture unloadable

See https://stackoverflow.com/a/70339902 for a possible lead.

`dpxmode` was a tougher climb, but an (untested) binary could eventually be built.
